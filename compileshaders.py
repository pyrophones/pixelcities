import os
import sys

def iterate(dir, outdir):
	for filename in os.listdir(dir):
		f = os.path.join(dir, filename)

		outname_spv = os.path.join(outdir, filename + ".spv")
		if os.path.isfile(f):
			if os.path.splitext(filename)[1] != ".glsl":
				cmd = "glslang {file_name} -Isource/vivarium/graphics/shaders/include -V100 -o {out_name}"
				os.system(cmd.format(file_name = f, out_name = outname_spv))
				cmd = "spirv-cross {file_name} --output {out_name} -V --reflect"
				os.system(cmd.format(file_name = outname_spv, out_name = outname_spv + ".json"))

		else:
			iterate(f, outdir)

if __name__ == "__main__":
	iterate(sys.argv[1], sys.argv[2])