module vivarium.graphics.tiledlightingcomputepipeline;

import std.math;

import erupted,
	   dlib.math;

import vivarium.core.camera,
	   vivarium.core.console,
	   vivarium.core.engine,
	   vivarium.graphics.computepipeline,
	   vivarium.graphics.graphicscontext,
	   vivarium.graphics.lights,
	   vivarium.graphics.memory;

struct SceneInfo
{
	vec4 camPos;
	mat4 view;
	mat4 invViewProj;
	DirectionalLight dirLight;
	float dummy;
	PointLight[1024] pointLights;
}

final class TiledLightingComputePipeline : ComputePipeline
{
	public:
		this()
		{
			
		}

		override void createCustomData(GraphicsContext gc, VkCommandBuffer current)
		{
			super.createCustomData(gc, current);
		}

	protected:
		
		override void predraw(GraphicsContext gc, ref VkCommandBuffer cmdBuffer)
		{

		}

		override void draw(GraphicsContext gc, ref VkCommandBuffer cmdBuffer, int pipelineNum)
		{
			vec2 dimensions = vec2(cast(vec2)(Engine.get().getWindowSize()) / 32.0);
			ivec2 dispatches = ivec2(cast(int)ceil(dimensions.x), cast(int)ceil(dimensions.y));
			
			foreach(i, ref pipeline; this.pipelines)
			{
				vkCmdBindPipeline(cmdBuffer, VK_PIPELINE_BIND_POINT_COMPUTE, pipeline);
				vkCmdDispatch(cmdBuffer, dispatches.x, dispatches.y, 1);
			}

			MemoryManager.get().transitionImageLayout(cmdBuffer,
													  gc.getCurrentPingPongTexture(),
													  VK_IMAGE_LAYOUT_GENERAL,
													  VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
													  VK_ACCESS_SHADER_WRITE_BIT,
													  VK_ACCESS_SHADER_READ_BIT,
													  VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT,
													  VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT);
		}

	private:
		//CameraMovedSignal camMoveSig;
}