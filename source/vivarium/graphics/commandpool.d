module vivarium.graphics.commandpool;

import std.stdio;

import erupted;

import vivarium.core.console,
	   vivarium.graphics.graphicscontext;

class CommandPool
{
	public:
		VkResult create(GraphicsContext gc, uint queueFamilyIndex, VkCommandPoolCreateFlags flags)
		{
			this.free(gc);
			VkCommandPoolCreateInfo createInfo = {};
			createInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
			createInfo.queueFamilyIndex = queueFamilyIndex;
			createInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;

			VkResult res = vkCreateCommandPool(gc.getDevice(), &createInfo, null, &this.cmdPool);
			if(res != VK_SUCCESS)
				Console.get().err("Failed to create usuable command pool");
			
			return res;
		}

		void free(GraphicsContext gc)
		{
			if(this.cmdPool != VK_NULL_HANDLE)
			{
				vkDestroyCommandPool(gc.getDevice(), this.cmdPool, null);
				this.cmdPool = VK_NULL_HANDLE;
			}
		}

		ref VkCommandPool getCommandPool()
		{
			return cmdPool;
		}

	private:
		VkCommandPool cmdPool;
}