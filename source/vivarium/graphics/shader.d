module vivarium.graphics.shader;

import std.stdio,
	   std.file,
	   std.path,
	   std.string,
	   std.json;

import erupted,
	   dlib.math;
	   //dlib.math;

import vivarium.core.console,
	   vivarium.graphics.graphicscontext,
	   vivarium.core.assetmanager;
//import vivarium.glslang_d;

struct ShaderInfo
{
	public:
		VkPipelineShaderStageCreateInfo pipelineStageInfo = {};
		VkPipelineVertexInputStateCreateInfo vertexInputInfo = {};
		VkVertexInputAttributeDescription[] inputAttributeDescriptions;
		VkVertexInputBindingDescription[] bindingDescriptions;
		string[] pushConstantNames;
}

class Shader
{
	public:
		this(string file)
		{
			this.fileName = thisExePath().dirName ~ "/" ~ file;
		}

		VkResult create(GraphicsContext gc, int defaultOffset = 0)
		{
			if(defaultOffset > -1)
				this.pushConstOffset = defaultOffset;
			this.free(gc);
			this.info.inputAttributeDescriptions.length = 0;
			this.info.bindingDescriptions.length = 0;
			this.info.pushConstantNames.length = 0;

			ubyte[] src = AssetManager.loadAsBytes(this.fileName);

			if(src.length == 0)
			{
				Console.get().err("Error loading shader, aborting module creation");
				return VK_ERROR_UNKNOWN;
			}

			VkShaderModuleCreateInfo createInfo = {};
			createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
			createInfo.codeSize = src.length;
			createInfo.pCode = cast(const(uint*))src.ptr;
			
			VkShaderModule shaderModule;
			VkResult res = vkCreateShaderModule(gc.getDevice(),
												&createInfo,
												null,
												&shaderModule);

			if(res != VK_SUCCESS)
			{
				Console.get().err("Failed to load shaders!");
				return res;
			}

			this.shaderModule = shaderModule;

			string entrypointName;

			this.info.pipelineStageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
			this.info.pipelineStageInfo.stage = this.parseReflectionData(gc, this.getFileName() ~ ".json", entrypointName);
			this.info.pipelineStageInfo.module_ = this.shaderModule;
			this.info.pipelineStageInfo.pName = toStringz(entrypointName);

			return res;
		}

		void free(GraphicsContext gc)
		{
			this.pushConstSize = 0;
			if(this.shaderModule != VK_NULL_HANDLE)
			{
				vkDestroyShaderModule(gc.getDevice(), this.shaderModule, null);
				this.shaderModule = VK_NULL_HANDLE;
			}
		}

		string getFileName()
		{
			return fileName;
		}

		@nogc int getPushConstantSize()
		{
			return this.pushConstSize;
		}

		@nogc VkShaderModule getShaderModule()
		{
			return this.shaderModule;
		}

		@nogc ref ShaderInfo getInfo()
		{
			return info;
		}

	private:
		string fileName;
		VkShaderModule shaderModule = VK_NULL_HANDLE;
		ShaderInfo info;
		int pushConstOffset = 0;
		int pushConstSize = 0;

		VkShaderStageFlagBits parseReflectionData(GraphicsContext gc, string jsonFile, out string entrypointName)
		{
			char[] jsonTxt = cast(char[])AssetManager.loadAsBytes(jsonFile);
			JSONValue json = parseJSON(jsonTxt);	
			VkShaderStageFlagBits shaderStage;

			VkDescriptorSetLayoutBinding[string][3] setbindings;

			if ("entryPoints" in json)
			{
				entrypointName = json["entryPoints"].array[0]["name"].str;
				switch(json["entryPoints"].array[0]["mode"].str)
				{
					case "vert":
						shaderStage = VK_SHADER_STAGE_VERTEX_BIT;
						int totalPerVertInputSize = 0;
						int totalPerInstInputSize = 0;
						foreach(input; json["inputs"].array)
						{
							auto splitName = input["name"].str.split("_");
							ulong curInputSize = this.getSizeFromStringType(input["type"].str);
							VkFormat format = VK_FORMAT_UNDEFINED;
							switch(curInputSize)
							{
								case float.sizeof:
									format = VK_FORMAT_R32_SFLOAT;
									break;
								case vec2.sizeof:
									format = VK_FORMAT_R32G32_SFLOAT;
									break;
								case vec3.sizeof:
									format = VK_FORMAT_R32G32B32_SFLOAT;
									break;
								case vec4.sizeof:
									if(splitName[$ - 2] != "unorm")
										format = VK_FORMAT_R32G32B32A32_SFLOAT;
									else
									{
										format = VK_FORMAT_R8G8B8A8_UNORM;
										curInputSize = 4;
									}
									break;
								default:
									break;
							}

							int total = 0;
							int bindingId = 0;
							switch(splitName[$ - 1])
							{
								case "vert":
									total = totalPerVertInputSize;
									totalPerVertInputSize += curInputSize;
									bindingId = 0;
									break;
								case "inst":
									total = totalPerInstInputSize;
									totalPerInstInputSize += curInputSize;
									bindingId = 1;
									break;
								default:
									break;
							}

							VkVertexInputAttributeDescription attributeDescription = {};
							attributeDescription.binding = bindingId;
							attributeDescription.location = cast(uint)input["location"].integer;
							attributeDescription.format = format;
							attributeDescription.offset = total;

							this.info.inputAttributeDescriptions ~= attributeDescription;
						}

						if(totalPerVertInputSize > 0)
						{
							VkVertexInputBindingDescription bindingDescription = {};
							bindingDescription.binding = 0;
							bindingDescription.stride = totalPerVertInputSize;
							bindingDescription.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;
							this.info.bindingDescriptions ~= bindingDescription;
						}

						if(totalPerInstInputSize > 0)
						{
							VkVertexInputBindingDescription bindingDescription = {};
							bindingDescription.binding = 1;
							bindingDescription.stride = totalPerInstInputSize;
							bindingDescription.inputRate = VK_VERTEX_INPUT_RATE_INSTANCE;
							this.info.bindingDescriptions ~= bindingDescription;
						}

						break;
					case "frag":
						shaderStage = VK_SHADER_STAGE_FRAGMENT_BIT;
						break;
					case "comp":
						shaderStage = VK_SHADER_STAGE_COMPUTE_BIT;
						break;
					case "tesc":
						shaderStage = VK_SHADER_STAGE_TESSELLATION_CONTROL_BIT;
						break;
					case "tese":
						shaderStage = VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT;
						break;
					case "geom":
						shaderStage = VK_SHADER_STAGE_GEOMETRY_BIT;
						break;
					default:
						break;
				}

				if (const(JSONValue)* ubos = "ubos" in json)
				{
					// TODO: Add logic to setup UBO data pointers from this
					foreach(uboInfo; ubos.array)
					{
						VkDescriptorSetLayoutBinding layoutBinding = {};
						layoutBinding.binding = cast(uint)uboInfo["binding"].integer;
						layoutBinding.descriptorCount = 1;
						layoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
						layoutBinding.pImmutableSamplers = null;
						layoutBinding.stageFlags = shaderStage;

						//gc.addDescriptorLayoutBinding(uboInfo["name"].str, layoutBinding);
						//this.info.descriptorInfo ~= DescriptorInfo(uboInfo["name"].str, cast(int)uboInfo["set"].integer);
						setbindings[cast(int)uboInfo["set"].integer][uboInfo["name"].str] = layoutBinding;
						//if(names[uboInfo["set"].integer] == "")
						//	names[uboInfo["set"].integer] =;
					}
				}

				if (const(JSONValue)* ssbos = "ssbos" in json)
				{
					// TODO: Add logic to setup UBO data pointers from this
					foreach(ssboInfo; ssbos.array)
					{
						VkDescriptorSetLayoutBinding layoutBinding = {};
						layoutBinding.binding = cast(uint)ssboInfo["binding"].integer;
						layoutBinding.descriptorCount = 1;
						if("readonly" in ssboInfo)
							layoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
						else
						{
							layoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC;
							gc.addDynamicOffset(cast(int)ssboInfo["set"].integer, ssboInfo["name"].str, 0);
						}

						layoutBinding.pImmutableSamplers = null;
						layoutBinding.stageFlags = shaderStage;

						//gc.addDescriptorLayoutBinding(ssboInfo["name"].str, layoutBinding);
						//this.info.descriptorInfo ~= DescriptorInfo(ssboInfo["name"].str, cast(int)ssboInfo["set"].integer);
						setbindings[cast(int)ssboInfo["set"].integer][ssboInfo["name"].str] = layoutBinding;
						//setbindings[ssboInfo["set"].integer] ~= layoutBinding;
						//if(names[ssboInfo["set"].integer] == "")
						//	names[ssboInfo["set"].integer] = ssboInfo["name"].str;
					}
				}

				if(const(JSONValue)* textures = "textures" in json)
				{
					foreach(texInfo; textures.array)
					{
						VkDescriptorSetLayoutBinding layoutBinding = {};
						layoutBinding.binding = cast(uint)texInfo["binding"].integer;
						layoutBinding.descriptorCount = 1;
						layoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
						layoutBinding.pImmutableSamplers = null;
						layoutBinding.stageFlags = shaderStage;

						//gc.addDescriptorLayoutBinding(texInfo["name"].str, layoutBinding);
						//this.info.descriptorInfo ~= DescriptorInfo(texInfo["name"].str, cast(int)texInfo["set"].integer);
						setbindings[cast(int)texInfo["set"].integer][texInfo["name"].str] = layoutBinding;
						//setbindings[texInfo["set"].integer] ~= layoutBinding;
						//if(names[texInfo["set"].integer] == "")
						//	names[texInfo["set"].integer] = texInfo["name"].str;
					}
				}

				if(const(JSONValue)* images = "images" in json)
				{
					foreach(imgInfo; images.array)
					{
						VkDescriptorSetLayoutBinding layoutBinding = {};
						layoutBinding.binding = cast(uint)imgInfo["binding"].integer;
						layoutBinding.descriptorCount = 1;
						layoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE;
						layoutBinding.pImmutableSamplers = null;
						layoutBinding.stageFlags = shaderStage;

						//gc.addDescriptorLayoutBinding(imgInfo["name"].str, layoutBinding);
						//this.info.descriptorInfo ~= DescriptorInfo(imgInfo["name"].str, cast(int)imgInfo["set"].integer);
						setbindings[cast(int)imgInfo["set"].integer][imgInfo["name"].str] = layoutBinding;
						//setbindings[imgInfo["set"].integer] ~= layoutBinding;
						//if(names[imgInfo["set"].integer] == "")
						//	names[imgInfo["set"].integer] = imgInfo["name"].str;
					}
				}
				
				
				if(const(JSONValue)* pcs = "push_constants" in json)
				{
					foreach(pushConst; pcs.array)
					{				
						foreach(constInfo; json["types"][pushConst["type"].str]["members"].array)
						{
							VkPushConstantRange pushConstant = {};
							pushConstant.stageFlags = shaderStage;
							pushConstant.offset = pushConstOffset + this.pushConstSize;
							pushConstant.size = cast(uint)this.getSizeFromStringType(constInfo["type"].str);
							this.pushConstSize += pushConstant.size;

							gc.addPushConstantRange(constInfo["name"].str, pushConstant);
							this.info.pushConstantNames ~= constInfo["name"].str;
						}
					}
				}
			}

			for(int i = 0; i < 3; i++)
			{
				foreach(key, val; setbindings[i])
					gc.addDescriptorLayout(i, key, val);
			}

			this.info.vertexInputInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
			this.info.vertexInputInfo.vertexBindingDescriptionCount = cast(uint)this.info.bindingDescriptions.length;
			this.info.vertexInputInfo.pVertexBindingDescriptions = this.info.bindingDescriptions.ptr;
			this.info.vertexInputInfo.vertexAttributeDescriptionCount = cast(uint)this.info.inputAttributeDescriptions.length;
			this.info.vertexInputInfo.pVertexAttributeDescriptions = this.info.inputAttributeDescriptions.ptr;

			return shaderStage;
		}

		size_t getSizeFromStringType(string name)
		{
			switch(name)
			{
				case "float":
					return float.sizeof;
				case "vec2":
					return vec2.sizeof;
				case "vec3":
					return vec3.sizeof;
				case "vec4":
					return vec4.sizeof;
				case "mat2":
					return mat2.sizeof;
				case "mat3":
					return mat3.sizeof;
				case "mat4":
					return mat4.sizeof;
				default:
					return 0;
			}
		}
}