module vivarium.graphics.mesh;

import std.stdio;

import erupted,
	   dlib.math;

import vivarium.core.transform,
	   vivarium.core.console,
	   vivarium.graphics.graphicscontext,
	   vivarium.graphics.memory;

struct Vertex
{
	vec3 pos = vec3(0.0);
	vec2 uv = vec2(0.0);
	vec3 norm = vec3(0.0);
	vec3 tan = vec3(0.0);
}

class Mesh
{
	public:
		this()
		{

		}

		void create(Vertex[] vertices, uint[] indices, uint[] offsets, uint[] amounts)
		{				
			this.verts = vertices.dup;
			this.inds = indices.dup;
			this.offs = offsets.dup;
			this.amnts = amounts.dup;

			if(this.verts.length == 0)
			{
				Console.get().warn("No mesh data present, skipping buffer creation");
			}

			this.vertexBlock = MemoryManager.get().allocDeviceLocalBuffer(Vertex.sizeof * this.verts.length);
			this.indexBlock  = MemoryManager.get().allocDeviceLocalBuffer(uint.sizeof * this.inds.length);
		}

		void free(GraphicsContext gc)
		{
			MemoryManager.get().freeDeviceLocalBuffer(gc, this.vertexBlock);
			MemoryManager.get().freeDeviceLocalBuffer(gc, this.indexBlock);
		}

		int transferData(GraphicsContext gc, ref VkCommandBuffer cmdBuffer)
		{
			BufferBlock vTransferBlock = MemoryManager.get().allocTransfer(Vertex.sizeof * this.verts.length);
			VkResult res = MemoryManager.get().copyIntoTransferBuffer(gc, this.verts.ptr, vTransferBlock);

			if(res != VK_SUCCESS)
				return res;

			BufferBlock iTransferBlock = MemoryManager.get().allocTransfer(uint.sizeof * this.inds.length);
			res = MemoryManager.get().copyIntoTransferBuffer(gc, this.inds.ptr, iTransferBlock);

			if(res != VK_SUCCESS)
				return res;

			MemoryManager.get().copyToDeviceLocalBuffer(cmdBuffer, vTransferBlock, this.vertexBlock);
			MemoryManager.get().copyToDeviceLocalBuffer(cmdBuffer, iTransferBlock, this.indexBlock);

			return res;
		}

		@nogc uint getOffsetAt(uint loc)
		{
			return this.offs[loc];
		}

		@nogc uint getAmountAt(uint loc)
		{
			return this.amnts[loc];
		}

		@nogc int getInstanceCount()
		{
			return this.instances;
		}

		@nogc void addInstance()
		{
			this.instances++;
		}

		@nogc @property ref Vertex[] vertices()
		{
			return this.verts;
		}

		@nogc @property ref uint[] indices()
		{
			return this.inds;
		}

		@nogc @property uint[] offsets()
		{
			return this.offs;
		}

		@nogc @property uint[] amounts()
		{
			return this.amnts;
		}

		@nogc @property BufferBlock vertexBufferBlock()
		{
			return this.vertexBlock;
		}

		@nogc @property BufferBlock indexBufferBlock()
		{
			return this.indexBlock;
		}

		@nogc @property ref Transform transform()
		{
			return this.trans;
		}

		@nogc @property ref Transform transform(Transform value)
		{
			this.trans = value;
			return trans;
		}

	private:
		Vertex[] verts;
		uint[] inds;
		uint[] offs;
		uint[] amnts;

		Transform trans;

		int instances;

		//Transform array for instances

		BufferBlock vertexBlock;
		BufferBlock indexBlock;
}