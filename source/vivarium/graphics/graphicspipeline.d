module vivarium.graphics.graphicspipeline;

// External
import erupted;

import vivarium.core,
	   vivarium.graphics.graphicscontext,
	   vivarium.graphics.mesh,
	   vivarium.graphics.shader,
	   vivarium.graphics.framebuffer,
	   vivarium.graphics.pipeline,
	   vivarium.graphics.renderpass,
	   vivarium.graphics.vkhelpers;

class GraphicsPipeline : Pipeline
{
	public:
		this(GraphicsContext gc, int renderpass = 0)
		{
			super();
			this.renderPassNum = renderpass;
			this.createInfo.renderPass = gc.getRenderPass(this.renderPassNum).getPass();
		}

		final override VkResult create(GraphicsContext gc, uint pipelineCount)
		{
			this.freePipelines(gc);
			this.createPipelineLayout(gc);

			VkPipelineShaderStageCreateInfo[] shaderStages;

			foreach(shaderName; this.shaderNames)
			{
				VkPipelineShaderStageCreateInfo info = gc.getShaderInfo(shaderName).pipelineStageInfo;
				shaderStages ~= info;

				if(info.stage == VK_SHADER_STAGE_VERTEX_BIT)
					this.vertexInfo = gc.getShaderInfo(shaderName).vertexInputInfo;
			}

			VkPipelineViewportStateCreateInfo viewportInfo = {};
			viewportInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
			viewportInfo.viewportCount = cast(uint)this.viewports.length;
			viewportInfo.pViewports = this.viewports.ptr;
			viewportInfo.scissorCount = cast(uint)this.scissors.length;
			viewportInfo.pScissors = this.scissors.ptr;

			this.createInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
			this.createInfo.stageCount = cast(uint)shaderStages.length;
			this.createInfo.pStages = shaderStages.ptr;
			this.createInfo.pVertexInputState = &this.vertexInfo;
			this.createInfo.pViewportState = &viewportInfo;
			this.createInfo.basePipelineHandle = VK_NULL_HANDLE;
			this.createInfo.basePipelineIndex = -1;
			this.createInfo.layout = this.pipelineLayout;
			
			this.pipelines.length = pipelineCount;
			VkResult res = vkCreateGraphicsPipelines(gc.getDevice(),
													 VK_NULL_HANDLE,
													 cast(uint)this.pipelines.length,
													 &this.createInfo,
													 null,
													 this.pipelines.ptr);

			if(res != VK_SUCCESS)
			{
				Console.get().err("Failed to create pipeline");
				return res;
			}

			this.meshesToRender.length = this.pipelines.length;
			return res;
		}

		final override VkResult recreate(GraphicsContext gc, uint pipelineCount)
		{
			//this.addViewport(viewport);
			//this.addScissor(scissor);

			Mesh*[][] tmpMeshes = this.meshesToRender.dup;

			VkResult res = this.create(gc, pipelineCount);

			this.meshesToRender.length = this.pipelines.length;
			foreach(i; 0 .. this.meshesToRender.length)
			{
				if(i < tmpMeshes.length)
					this.meshesToRender[i] = tmpMeshes[i].dup;
			}

			super.recreate(gc, pipelineCount);

			return res;
		}

		//final void pushConstants(ref VkCommandBuffer cmdBuffer, VkShaderStageFlags flags, uint offset, uint size, const void* values)
		//{
		//	vkCmdPushConstants(cmdBuffer, this.pipelineLayout, flags, offset, size, values);
		//}

		final override void drawPass(GraphicsContext gc, ref VkCommandBuffer cmdBuffer, int curFrame, int imgIdx)
		{
			super.drawPass(gc, cmdBuffer, curFrame, imgIdx);

			vkCmdSetViewport(cmdBuffer, 0, cast(uint)this.viewports.length, this.viewports.ptr);
			vkCmdSetScissor(cmdBuffer, 0, cast(uint)this.scissors.length, this.scissors.ptr);

			VkRenderPassBeginInfo beginInfo = gc.getRenderPass(this.renderPassNum).createBeginInfo(gc, imgIdx);

			vkCmdBeginRenderPass(cmdBuffer, &beginInfo, VK_SUBPASS_CONTENTS_INLINE);
			
			foreach(i, ref pipeline; this.pipelines)
			{
				vkCmdBindPipeline(cmdBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline);

				this.draw(gc, cmdBuffer, cast(int)i);
			}
			vkCmdEndRenderPass(cmdBuffer);
		}

		final override void reset()
		{
			clearViewports();
			clearScissor();
		}

		final override void bindDescriptorSets(GraphicsContext gc, ref VkCommandBuffer cmdBuffer, int index)
		{
			foreach(shader; this.shaderNames)
			{
				for(uint i = 0; i < 3; i++)
				{
					VkDescriptorSet set = gc.getDescriptorSet(i, index);
					auto info = gc.getDynamicOffsets(i);
					vkCmdBindDescriptorSets(cmdBuffer,
											VK_PIPELINE_BIND_POINT_GRAPHICS,
											this.pipelineLayout,
											i,
											1,
											&set,
											cast(uint)info.length,
											info.length > 0 ? info.ptr : null);
				}
			}
		}

		final void addMesh(Mesh* mesh, uint pipelineNum)
		{
			this.meshesToRender[pipelineNum] ~= mesh;
		}

		final void addViewport(VkViewport viewport)
		{
			this.viewports ~= viewport;
		}

		final void clearViewports()
		{
			this.viewports.length = 0;
		}

		final void addScissor(VkRect2D scissor)
		{
			this.scissors ~= scissor;
		}

		final void clearScissor()
		{
			this.scissors.length = 0;
		}

		final void setRenderPass(GraphicsContext gc, int num)
		{
			this.renderPassNum = num;
			this.createInfo.renderPass = gc.getRenderPass(this.renderPassNum).getPass();
		}

	protected:
		Framebuffer[] framebuffers;
		int renderPassNum;

		// Other creation info
		VkViewport[] viewports;
		VkRect2D[] scissors;
		VkDynamicState[] dynamicStates;
		VkPipelineVertexInputStateCreateInfo vertexInfo;
		VkPipelineInputAssemblyStateCreateInfo inputAssembly = {};
		VkPipelineRasterizationStateCreateInfo rasterizeInfo = {};
		VkPipelineMultisampleStateCreateInfo multisampleInfo = {};
		VkPipelineDepthStencilStateCreateInfo dsCreateInfo = {};
		VkPipelineColorBlendAttachmentState[] colorBlendstateAttachment;
		VkPipelineColorBlendStateCreateInfo colorInfo = {};
		VkPipelineDynamicStateCreateInfo dynamicInfo = {};
		VkGraphicsPipelineCreateInfo createInfo = {};

		Mesh*[][] meshesToRender;

		//abstract void prepareDraw();

		final void duplicateBlendStateForAll(GraphicsContext gc, VkPipelineColorBlendAttachmentState colorBlendState)
		{
			foreach(i, attachment; gc.getRenderPass(this.renderPassNum).getAttachments())
			{
				if(!vkHelpers_isDepthFormat(attachment.format))
					this.colorBlendstateAttachment ~= colorBlendState;
			}
		}
		
	private:
		void freePipelines(GraphicsContext gc)
		{
			super.freePipelines(gc);
			foreach(ref toRender; this.meshesToRender)
			{
				toRender.length = 0;
			}
		}
}