module vivarium.graphics.framebuffer;

import std.stdio;

import erupted;

import vivarium.core.console,
	   vivarium.graphics.graphicscontext;

class Framebuffer
{
	public:
		VkResult create(GraphicsContext gc, VkRenderPass pass)
		{
			this.free(gc);

			VkFramebufferCreateInfo createInfo = {};
			createInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
			createInfo.renderPass = pass;
			createInfo.attachmentCount = cast(uint)this.attachments.length;
			createInfo.pAttachments = this.attachments.ptr;
			createInfo.width = gc.getExtents().width;
			createInfo.height = gc.getExtents().height;
			createInfo.layers = 1;

			VkResult ret = vkCreateFramebuffer(gc.getDevice(), &createInfo, null, &this.buffer);
			if(ret != VK_SUCCESS)
			{
				Console.get().err("Failed to create a usable framebuffer");
				return ret;
			}

			return VK_SUCCESS;
		}

		void free(GraphicsContext gc)
		{
			if(this.buffer != VK_NULL_HANDLE)
			{
				vkDestroyFramebuffer(gc.getDevice(), this.buffer, null);
				this.buffer = VK_NULL_HANDLE;
			}
		}

		ref VkFramebuffer getFramebuffer()
		{
			return this.buffer;
		}

		void addAttachment(VkImageView attachment)
		{
			this.attachments ~= attachment;
		}

		//ref VkRenderPass getAttachedRenderPass()
		//{
		//	return this.renderPass;
		//}

	private:
		VkFramebuffer buffer = VK_NULL_HANDLE;
		VkImageView[] attachments;
}