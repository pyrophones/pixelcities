module vivarium.graphics.lights;

import dlib.math;

struct Light
{
	vec3 color = vec3(0, 0, 0);
	float intensity = 0;
}

struct DirectionalLight
{
	Light light;
	vec3 dir = vec3(0, 0, 0);
}

struct PointLight
{
	Light light;
	vec3 pos = vec3(0, 0, 0);
	float radius = 0;
}