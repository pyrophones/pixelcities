module vivarium.graphics.renderpass;

import std.stdio,
	   std.array;

import erupted;

import vivarium.core.console,
	   vivarium.graphics.graphicscontext,
	   vivarium.graphics.framebuffer,
	   vivarium.graphics.texture;

class RenderPass
{
	//@nogc:
    public:
        VkResult create(GraphicsContext gc)
        {
			VkRenderPassCreateInfo renderPassInfo = {};
			renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
			renderPassInfo.attachmentCount = cast(uint)this.attachments.length;
			renderPassInfo.pAttachments = this.attachments.ptr;
			renderPassInfo.subpassCount = cast(uint)this.subpasses.length;
			renderPassInfo.pSubpasses = this.subpasses.ptr;
			renderPassInfo.dependencyCount = cast(uint)this.subpassDependencies.length;
			renderPassInfo.pDependencies = this.subpassDependencies.ptr;

			VkResult res = vkCreateRenderPass(gc.getDevice(), &renderPassInfo, null, &this.pass);
			if(res != VK_SUCCESS)
				Console.get().err("Failed to create renderpass");

			return res;
        }

		void free(GraphicsContext gc)
		{
			this.freeFramebuffers(gc);
			if(this.pass != VK_NULL_HANDLE)
			{
				vkDestroyRenderPass(gc.getDevice(), this.pass, null);
				this.pass = VK_NULL_HANDLE;
			}
		}

		VkResult createFramebuffers(GraphicsContext gc, int count, Texture* textures = null, ulong texturesLength = 0)
		{
			if(textures != null && texturesLength > 0)
			{
				this.textures = textures;
				this.texturesLength = texturesLength;
			}
			
			this.freeFramebuffers(gc);
			this.framebuffers.length = count;
			
			VkResult res = VK_ERROR_UNKNOWN;
			int skipAmount = 0;
			foreach(i, ref framebuffer; this.framebuffers)
			{
				framebuffer = new Framebuffer();

				foreach(j, attachment; this.attachments)
				{
					if(skipAmount > 0)
					{
						skipAmount--;
						continue;
					}

					if(attachment.format > 123 && attachment.format < 131)
						framebuffer.addAttachment(gc.getDepthTexture().getImageView(0).getImageView());

					else
					{
						if(this.textures != null)
						{
							skipAmount = this.textures[j].getLayers() - 1;
							foreach(layer; 0 .. this.textures[j].getLayers())
								framebuffer.addAttachment(this.textures[j].getImageView(layer).getImageView());
						}
						else
							framebuffer.addAttachment(gc.getImageView(cast(uint)i));
					}
				}

				res = framebuffer.create(gc, this.pass);
				if(res != VK_SUCCESS)
				{
					Console.get().err("Failed to create usable framebuffer");
					return res;
				}
			}

			return res;
		}

		void addAttachment(VkAttachmentDescription attachment)
		{
			this.attachments ~= attachment;

			if(attachment.format > 123 && attachment.format < 131)
				this.attachmentRefs ~= VkAttachmentReference(cast(uint)this.attachments.length - 1,
															 VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL);

			else
				this.attachmentRefs ~=  VkAttachmentReference(cast(uint)this.attachments.length - 1,
															  VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL);
		}

		void addSubpass(VkPipelineBindPoint bindPoint, int colorAttachmentCount, int attachmentNum, int depthAttachmentNum)
		{
			VkSubpassDescription subpassDesc = {};
			subpassDesc.pipelineBindPoint = bindPoint;
			subpassDesc.colorAttachmentCount = colorAttachmentCount;
			subpassDesc.pColorAttachments = attachmentNum > -1 ? &this.attachmentRefs[attachmentNum] : null;
			subpassDesc.pDepthStencilAttachment = depthAttachmentNum > -1 ? &this.attachmentRefs[depthAttachmentNum] : null;

			this.subpasses ~= subpassDesc;
		}

		void addSubpassDependency(VkSubpassDependency dependency)
		{
			this.subpassDependencies ~= dependency;
		}

		void addClearColor(VkClearValue color)
		{
			this.clearColors ~= color;
		}

		@nogc VkRenderPass getPass()
		{
			return this.pass;
		}

		@nogc ulong getAttachmentCount()
		{
			return attachments.length;
		}

		@nogc VkAttachmentDescription[] getAttachments()
		{
			return attachments;
		}

		//@nogc VkFramebuffer getFramebuffer(int loc)
		//{
		//	if(this.contains(loc))
		//		return this.framebuffers[loc].getFramebuffer();
		//	else
		//		return VK_NULL_HANDLE;
		//}

		VkRenderPassBeginInfo createBeginInfo(GraphicsContext gc, int framebufferIdx)
		{
			VkRenderPassBeginInfo beginInfo = {};
			beginInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
			beginInfo.renderPass = this.pass;
			beginInfo.framebuffer = this.framebuffers[framebufferIdx].getFramebuffer();
			beginInfo.renderArea.offset = VkOffset2D(0, 0);
			beginInfo.renderArea.extent = gc.getExtents();
			beginInfo.clearValueCount = cast(uint)this.clearColors.length;
			beginInfo.pClearValues = this.clearColors.ptr;

			return beginInfo;
		}

		@nogc Texture* getTexturesPtr()
		{
			return this.textures;
		}

		@nogc ulong getTexturesLength()
		{
			return this.texturesLength;
		}


    private:
        VkRenderPass pass = VK_NULL_HANDLE;
		VkAttachmentDescription[] attachments;
		VkAttachmentReference[] attachmentRefs;
		VkSubpassDescription[] subpasses;
		VkSubpassDependency[] subpassDependencies;
		VkClearValue[] clearColors;

		Framebuffer[] framebuffers;

		Texture* textures;
		ulong texturesLength = 0;

		void freeFramebuffers(GraphicsContext gc)
		{
			foreach(framebuffer; this.framebuffers)
				framebuffer.free(gc);
			this.framebuffers.length = 0;
		}
}