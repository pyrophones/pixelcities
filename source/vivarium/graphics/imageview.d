module vivarium.graphics.imageview;

import core.memory,
	   std.stdio;

import erupted;

import vivarium.core.console,
	   vivarium.graphics.graphicscontext;

class ImageView
{
    public:
        VkResult create(GraphicsContext gc, VkImageViewCreateInfo createInfo, int imgNum)
        {
            this.free(gc);

			this.imgNum = imgNum;
            
            VkResult res = vkCreateImageView(gc.getDevice(),
                                             &createInfo,
                                             null,
                                             &this.imgView);

	    	if(res != VK_SUCCESS)
	    		Console.get().err("Failed to create an image view");

	    	return res;
        }

        void free(GraphicsContext gc)
        {
            if(imgView != VK_NULL_HANDLE)
            {
                vkDestroyImageView(gc.getDevice(), this.imgView, null);
                this.imgView = VK_NULL_HANDLE;
            }
        }

        @nogc VkImageView getImageView()
        {
            return this.imgView;
        }

		int getImageNum()
        {
            return this.imgNum;
        }
    
    private:
        VkImageView imgView = VK_NULL_HANDLE;
		int imgNum;
}