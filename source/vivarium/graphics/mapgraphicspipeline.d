module vivarium.graphics.mapgraphicspipeline;

import erupted,
	   dlib.math;

import vivarium.graphics.graphicspipeline,
	   vivarium.graphics.graphicscontext,
	   vivarium.graphics.memory;

struct MatConstants
{
	mat4 world;
	mat3 normal;
}

class MapGraphicsPipeline : GraphicsPipeline
{
	public:
		this(GraphicsContext gc, int renderpass = 0)
		{
			super(gc, renderpass);

			VkViewport viewport;
			viewport.x = 0.0;
			viewport.y = 0.0;
			viewport.width = gc.getExtents().width;
			viewport.height = gc.getExtents().height;
			viewport.minDepth = 0.0f;
			viewport.maxDepth = 1.0f;

			VkRect2D scissor;
			scissor.offset = VkOffset2D(0, 0);
			scissor.extent = gc.getExtents();

			this.viewports ~= viewport;
			this.scissors ~= scissor;

			this.inputAssembly.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
			this.inputAssembly.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
			this.inputAssembly.primitiveRestartEnable = VK_FALSE;

			this.rasterizeInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
			this.rasterizeInfo.depthClampEnable = VK_FALSE;
			this.rasterizeInfo.rasterizerDiscardEnable = VK_FALSE;
			this.rasterizeInfo.polygonMode = VK_POLYGON_MODE_FILL;
			this.rasterizeInfo.lineWidth = 1.0f;
			this.rasterizeInfo.cullMode = VK_CULL_MODE_BACK_BIT;
			this.rasterizeInfo.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
			this.rasterizeInfo.depthBiasEnable = VK_FALSE;
			this.rasterizeInfo.depthBiasConstantFactor = 0.0f;
			this.rasterizeInfo.depthBiasClamp = 0.0f;
			this.rasterizeInfo.depthBiasSlopeFactor = 0.0f;

			this.multisampleInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
			this.multisampleInfo.sampleShadingEnable = VK_FALSE;
			this.multisampleInfo.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
			this.multisampleInfo.minSampleShading = 1.0f;
			this.multisampleInfo.pSampleMask = null;
			this.multisampleInfo.alphaToCoverageEnable = VK_FALSE;
			this.multisampleInfo.alphaToOneEnable = VK_FALSE;

			this.dsCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
			this.dsCreateInfo.depthTestEnable = VK_TRUE;
			this.dsCreateInfo.depthWriteEnable = VK_TRUE;
			this.dsCreateInfo.depthCompareOp = VK_COMPARE_OP_LESS;
			this.dsCreateInfo.depthBoundsTestEnable = VK_FALSE;
			this.dsCreateInfo.stencilTestEnable = VK_FALSE;

			VkPipelineColorBlendAttachmentState colorBlend;
			colorBlend.colorWriteMask = VK_COLOR_COMPONENT_R_BIT
										 | VK_COLOR_COMPONENT_G_BIT
										 | VK_COLOR_COMPONENT_B_BIT
										 | VK_COLOR_COMPONENT_A_BIT;
			colorBlend.blendEnable = VK_FALSE;
			//colorBlend.srcColorBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA;
			//colorBlend.dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
			//colorBlend.colorBlendOp = VK_BLEND_OP_ADD;
			//colorBlend.srcAlphaBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA;
			//colorBlend.dstAlphaBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
			//colorBlend.alphaBlendOp = VK_BLEND_OP_ADD;
			this.duplicateBlendStateForAll(gc, colorBlend);

			this.colorInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
			this.colorInfo.logicOpEnable = VK_FALSE;
			this.colorInfo.logicOp = VK_LOGIC_OP_COPY;
			this.colorInfo.attachmentCount = cast(uint)this.colorBlendstateAttachment.length;
			this.colorInfo.pAttachments = this.colorBlendstateAttachment.ptr;
			this.colorInfo.blendConstants[0] = 0.0f;
			this.colorInfo.blendConstants[1] = 0.0f;
			this.colorInfo.blendConstants[2] = 0.0f;
			this.colorInfo.blendConstants[3] = 0.0f;

			this.dynamicStates ~= VK_DYNAMIC_STATE_VIEWPORT;
			this.dynamicStates ~= VK_DYNAMIC_STATE_SCISSOR;

			this.dynamicInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
			this.dynamicInfo.dynamicStateCount = cast(uint)this.dynamicStates.length;
			this.dynamicInfo.pDynamicStates = this.dynamicStates.ptr;

			this.createInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
			this.createInfo.pInputAssemblyState = &inputAssembly;
			this.createInfo.pRasterizationState = &rasterizeInfo;
			this.createInfo.pMultisampleState = &multisampleInfo;
			this.createInfo.pDepthStencilState = &dsCreateInfo;
			this.createInfo.pColorBlendState = &colorInfo;
			this.createInfo.pDynamicState = &dynamicInfo;
			this.createInfo.subpass = 0;
			this.createInfo.basePipelineHandle = VK_NULL_HANDLE;
			this.createInfo.basePipelineIndex = -1;
		}
		
	protected:
		override void draw(GraphicsContext gc, ref VkCommandBuffer cmdBuffer, int imgNum)
		{
			//TODO Switch this to alloc meshes based on pipelines
			foreach(i, ref mesh; this.meshesToRender[imgNum])
			{
				foreach(j; 0 .. mesh.amounts.length)
				{
					//mat4f[2] pushConstants = [ mesh.getTransform().getModel(), mesh.getTransform().getNormal() ]
					MatConstants mc = MatConstants(mesh.transform.model, mesh.transform.normal);
					vkCmdPushConstants(cmdBuffer,
									   this.pipelineLayout,
									   VK_SHADER_STAGE_VERTEX_BIT,
									   MatConstants.world.offsetof,
									   MatConstants.world.sizeof,
									   &mc);
					vkCmdPushConstants(cmdBuffer,
									   this.pipelineLayout,
									   VK_SHADER_STAGE_FRAGMENT_BIT,
									   MatConstants.normal.offsetof,
									   MatConstants.normal.sizeof,
									   &mc);

					if(mesh.getInstanceCount() > 0)
					{
						BufferBlock vertBlock = mesh.vertexBufferBlock();
						VkBuffer[1] vertexBuffers;
						vertexBuffers[0] = MemoryManager.get().getDeviceLocalBuffer(vertBlock.poolId).getBuffer();
						VkDeviceSize[1] offsets = [ vertBlock.offset ];
						vkCmdBindVertexBuffers(cmdBuffer, 0, 1, vertexBuffers.ptr, offsets.ptr);

						BufferBlock idxBlock = mesh.indexBufferBlock();
						vkCmdBindIndexBuffer(cmdBuffer,
											 MemoryManager.get().getDeviceLocalBuffer(idxBlock.poolId).getBuffer(),
											 idxBlock.offset,
											 VK_INDEX_TYPE_UINT32);

						vkCmdDrawIndexed(cmdBuffer,
										 cast(uint)mesh.getAmountAt(cast(uint)j),
										 mesh.getInstanceCount(),
										 0,
										 mesh.getOffsetAt(cast(uint)j),
										 0);
					}
				}
			}
		}

	private:
}