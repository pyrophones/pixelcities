module vivarium.graphics.computepipeline;

// External
import erupted;

import vivarium.core,
	   vivarium.graphics.graphicscontext,
	   vivarium.graphics.pipeline,
	   vivarium.graphics.shader;

class ComputePipeline : Pipeline
{
	public:
		final override VkResult create(GraphicsContext gc, uint pipelineCount)
		{
			this.createInfos.length = 0;
			this.freePipelines(gc);
			this.createPipelineLayout(gc);

			foreach(shaderName; this.shaderNames)
			{
				VkPipelineShaderStageCreateInfo shaderStageInfo = gc.getShaderInfo(shaderName).pipelineStageInfo;

				if(shaderStageInfo.stage != VK_SHADER_STAGE_COMPUTE_BIT)
				{
					Console.get().log("Shader '" ~ shaderName ~ "' is not a compute shader");
					continue;
				}

				VkComputePipelineCreateInfo createInfo = {};
				createInfo.sType = VK_STRUCTURE_TYPE_COMPUTE_PIPELINE_CREATE_INFO;
				createInfo.stage = shaderStageInfo;
				createInfo.basePipelineHandle = VK_NULL_HANDLE;
				createInfo.basePipelineIndex = -1;
				createInfo.layout = this.pipelineLayout;

				this.createInfos ~= createInfo;
			}

			this.pipelines.length = this.createInfos.length;
			VkResult res = vkCreateComputePipelines(gc.getDevice(),
													VK_NULL_HANDLE,
													cast(uint)this.createInfos.length,
													this.createInfos.ptr,
													null,
													this.pipelines.ptr);

			if(res != VK_SUCCESS)
			{
				Console.get().err("Failed to create compute pipeline");
				return res;
			}

			return res;
		}

		final override VkResult recreate(GraphicsContext gc, uint pipelineCount)
		{
			VkResult res = this.create(gc, pipelineCount);
			super.recreate(gc, pipelineCount);
			return res;
		}

		final override void bindDescriptorSets(GraphicsContext gc, ref VkCommandBuffer cmdBuffer, int index)
		{
			foreach(shader; this.shaderNames)
			{
				for(uint i = 0; i < 3; i++)
				{
					vkCmdBindDescriptorSets(cmdBuffer,
											VK_PIPELINE_BIND_POINT_COMPUTE,
											this.pipelineLayout,
											i,
											1,
											&gc.getDescriptorSet(i, index),
											cast(uint)gc.getDynamicOffsets(i).length,
											gc.getDynamicOffsets(i).length > 0 ? gc.getDynamicOffsets(i).ptr : null);
				}
			}
		}

		//final void pushConstants(ref VkCommandBuffer cmdBuffer, VkShaderStageFlags flags, uint offset, uint size, const void* values)
		//{
		//	vkCmdPushConstants(cmdBuffer, this.pipelineLayout, flags, offset, size, values);
		//}

		final override void drawPass(GraphicsContext gc, ref VkCommandBuffer cmdBuffer, int curFrame, int imgIdx)
		{
			super.drawPass(gc, cmdBuffer, curFrame, imgIdx);
			
			foreach(i, ref pipeline; this.pipelines)
			{
				this.draw(gc, cmdBuffer, cast(int)i);
			}
		}

	protected:
		// Other creation info
		VkComputePipelineCreateInfo[] createInfos;

		//abstract void prepareDraw();
}