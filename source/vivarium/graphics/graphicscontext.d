module vivarium.graphics.graphicscontext;

import std.algorithm,
	   std.algorithm.sorting,
	   std.container.rbtree,
	   std.conv,
	   std.stdio,
	   std.string,
	   std.math,
	   std.process,
	   std.file,
	   std.path,
	   core.memory,
	   std.typecons : Tuple;

// External
import bindbc.sdl,
	   erupted,
	   erupted.platform_extensions,
	   dlib.math;

import vivarium.core,
	   vivarium.graphics;

// Linux version info
version (linux)
{
	public import X11.Xlib;
	mixin Platform_Extensions!USE_PLATFORM_XLIB_KHR;
}

struct SceneLighting
{
	PointLight[1024] pointLights;
	DirectionalLight dirLight;
}

struct TileInfo
{
	uint activeLightCount;
 	uint[1024] pointLightIndex;
}

class GraphicsContext
{
	public:
		this()
		{
			assert(!isInstanceCreated);
			this.isInstanceCreated = true;
		}

		~this()
		{
			this.isInstanceCreated = false;
		}

		int initialize(SDL_Window* window)
		{
			// Vulkan instance creation
			uint extCount;
			if(!SDL_Vulkan_GetInstanceExtensions(window, &extCount, null))
			{
				Console.get().crit("Could not get vulkan extension count. SDL Error: " ~ SDL_GetError().fromStringz().idup);
				return -1;
			}

			const(char*)[] extensions;
			extensions.length = extCount;
			if(!SDL_Vulkan_GetInstanceExtensions(window, &extCount, cast(const(char)**)extensions))
			{
				Console.get().crit("Could not get vulkan extensions. SDL Error: " ~ SDL_GetError().fromStringz().idup);
				return -1;
			}

			extensionNames ~= VK_KHR_SWAPCHAIN_EXTENSION_NAME;
			extensions ~= VK_KHR_SURFACE_EXTENSION_NAME;

			version(linux)
			{
				extensions ~= VK_KHR_XLIB_SURFACE_EXTENSION_NAME;
			}

			debug
			{
				extensions ~= VK_EXT_DEBUG_UTILS_EXTENSION_NAME;
			}

			VkApplicationInfo appInfo = {};
			appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
			appInfo.pNext = null;
			appInfo.pApplicationName = "vulkan";
			appInfo.applicationVersion = VK_MAKE_API_VERSION(0, 0, 1, 0);
			appInfo.pEngineName = "vivarium";
			appInfo.engineVersion = VK_MAKE_API_VERSION(0, 0, 1, 0);
			appInfo.apiVersion = VK_API_VERSION_1_0;

			VkInstanceCreateInfo createInfo = {};
			createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
			createInfo.enabledExtensionCount = cast(uint)extensions.length;
			createInfo.ppEnabledExtensionNames = extensions.ptr;
			createInfo.pApplicationInfo = &appInfo;

			VkResult res; 
			debug
			{
				bool doValidation = true;
				const(char*)[] validationLayers;
				validationLayers ~= "VK_LAYER_KHRONOS_validation";
				uint layerCount;

				vkEnumerateInstanceLayerProperties(&layerCount, null);

				VkLayerProperties[] availableLayers;
				availableLayers.length = layerCount;
				availableLayers[0 .. $] = VK_NULL_HANDLE;
				vkEnumerateInstanceLayerProperties(&layerCount, availableLayers.ptr);
				foreach(layerName; validationLayers)
				{
					bool layerFound = false;

					foreach(layerProperties; availableLayers)
					{
						if(to!string(layerProperties.layerName).split("\0")[0] == to!string(layerName))
						{
							layerFound = true;
							break;
						}
					}

					if(!layerFound)
					{
						Console.get().err("Failed to find all validation layers");
						doValidation = false;
						break;
					}
				}

				VkDebugUtilsMessengerCreateInfoEXT instanceDbgCreateInfo = {};

				if(doValidation)
				{
					instanceDbgCreateInfo.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
					instanceDbgCreateInfo.messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT | 
													VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT | 
													VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
					instanceDbgCreateInfo.messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT |
												VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT |
												VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
					instanceDbgCreateInfo.pfnUserCallback = &vkHelpers_vulkanDebugCallback;
					instanceDbgCreateInfo.pUserData = null;

					createInfo.enabledLayerCount = cast(uint)validationLayers.length;
					createInfo.ppEnabledLayerNames = validationLayers.ptr;
					createInfo.pNext = &instanceDbgCreateInfo;
				}
			}

			res = vkCreateInstance(&createInfo, null, &this.instance);
			loadInstanceLevelFunctions(this.instance);
			loadInstanceLevelFunctionsExt(this.instance);
			loadDeviceLevelFunctions(this.instance);

			if(res != VK_SUCCESS)
			{
				Console.get().crit("Error creating vulkan instance.");
				return -1;
			}

			debug
			{
				VkDebugUtilsMessengerCreateInfoEXT dbgCreateInfo = {};
				dbgCreateInfo.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
				dbgCreateInfo.messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT | 
												VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT | 
												VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
				dbgCreateInfo.messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT |
											VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT |
											VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
				dbgCreateInfo.pfnUserCallback = &vkHelpers_vulkanDebugCallback;
				dbgCreateInfo.pUserData = null;

				res = vkCreateDebugUtilsMessengerEXT(this.instance, &dbgCreateInfo, null, &this.debugMessenger);
				if(res != VK_SUCCESS)
				{
					Console.get().crit("Could not create debug messenger");
					return res;
				}
			}

			// Vulkan surface creation
			if(SDL_Vulkan_CreateSurface(window, this.instance, &this.surface) == SDL_FALSE)
			{
				Console.get().crit("Could not create vulkan surface. SDL error: " ~ SDL_GetError().fromStringz().idup);
				return res;
			}

			// Get gpus
			uint deviceCount = 0;
			vkEnumeratePhysicalDevices(this.instance, &deviceCount, null);

			if(deviceCount == 0)
			{
				Console.get().crit("No GPUs with vulkan support found.");
				return -1;
			}

			VkPhysicalDevice[] physDevices;
			physDevices.length = deviceCount;
			res = vkEnumeratePhysicalDevices(this.instance, &deviceCount, physDevices.ptr);

			if(res != VK_SUCCESS)
			{
				Console.get().crit("Error getting physical devices.");
				return res;
			}

			VkPhysicalDevice*[int] candidates;

			foreach(ref pDev; physDevices)
				candidates[vkHelpers_getDeviceSuitability(pDev, extensionNames)] = &pDev;

			// Select most appropriate device
			VkQueueFamilyProperties[int] qfis;

			VkBool32 presentSupport;

			QueueFamilies curQueueFam;

			// Sort order from lowest to highest, reverse foreach to start with best GPU
			// Loop through all physical device candidates
			foreach(key; candidates.keys.sort)
			{
				if(key == 0)
					continue;

				VkPhysicalDevice pDev = *candidates[key];
				qfis = vkHelpers_findQueueFamilies(pDev);

				curQueueFam.presentFamily = -1;
				curQueueFam.gfxFamily = -1;
				curQueueFam.xferFamily = -1;
				curQueueFam.compFamily = -1;
				curQueueFam.xferQueueCount = 0;

				//Check the queue families to see if there is a valid configuartion available
				foreach(i, queueFamProps; qfis)
				{
					vkGetPhysicalDeviceSurfaceSupportKHR(pDev, i, this.surface, &presentSupport);

					//Check for a graphics queue
					if(presentSupport && curQueueFam.presentFamily == -1)
						curQueueFam.presentFamily = i;
					if(queueFamProps.queueFlags & VK_QUEUE_GRAPHICS_BIT && curQueueFam.gfxFamily == -1)
						curQueueFam.gfxFamily = i;

					// Check if it's possible to grab an transfer queue for GPUs with a dedicated one
					else if(queueFamProps.queueFlags & VK_QUEUE_TRANSFER_BIT && curQueueFam.xferFamily == -1)
					{
						curQueueFam.xferFamily = i;
						curQueueFam.xferQueueCount++;
						if(queueFamProps.queueFlags & VK_QUEUE_COMPUTE_BIT && queueFamProps.queueCount > 1)
						{
							curQueueFam.compFamily = i;
							curQueueFam.xferQueueCount++;
						}
					}

					// Check if it's possible to grab an async queue for GPUS with dedicated compute queues
					else if(queueFamProps.queueFlags & VK_QUEUE_COMPUTE_BIT && curQueueFam.compFamily == -1)
						curQueueFam.compFamily = i;
				}

				// If all families are available, break
				if(curQueueFam.presentFamily != -1 && curQueueFam.gfxFamily != -1)
				{
					// If there are no dedicated queues for transfer / compute, just use the gfx queue
					if(curQueueFam.xferFamily == -1)
						curQueueFam.xferFamily = curQueueFam.gfxFamily;

					if(curQueueFam.compFamily == -1)
						curQueueFam.compFamily = curQueueFam.gfxFamily;


					if(curQueueFam.xferQueueCount > this.qfs.xferQueueCount)
					{
						//this.qfs.gfxFamily = -1;
						//this.qfs.xferFamily = -1;
						//this.qfs.compFamily = -1;

						this.qfs = curQueueFam;
						this.physDev = pDev;
					}
				}
			}

			if(!this.physDev)
			{
				Console.get().crit("No suitable physical device found.");
				return -1;
			}

			//TODO: Avoid doing this twice?
			vkGetPhysicalDeviceProperties(physDev, &this.curDeviceProperties);

			auto uniqueQueueFamilies = redBlackTree(this.qfs.presentFamily, this.qfs.gfxFamily, this.qfs.xferFamily, this.qfs.compFamily);

			// Device queue creation info
			VkDeviceQueueCreateInfo[] queueCreateInfos;
			queueCreateInfos.reserve(uniqueQueueFamilies.length);
			float queuePriority = 1.0;

			// Primary / graphics queue
			// async compute queue
			// async transfer queue

			foreach(family; uniqueQueueFamilies)
			{
				int queues = 1;

				if(family == this.qfs.xferFamily && this.qfs.xferQueueCount > 0)
					queues = this.qfs.xferQueueCount;

				VkDeviceQueueCreateInfo queueInfo = {};
				queueInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
				queueInfo.queueFamilyIndex = family;
				queueInfo.queueCount = queues;
				queueInfo.pQueuePriorities = &queuePriority;
				queueCreateInfos ~= queueInfo;
			}

			// Get device features
			VkPhysicalDeviceFeatures devFeatures;
			vkGetPhysicalDeviceFeatures(this.physDev, &devFeatures);

			// Create device creation info
			VkDeviceCreateInfo devCreateInfo = {};
			devCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
			devCreateInfo.queueCreateInfoCount = cast(uint)queueCreateInfos.length;
			devCreateInfo.pQueueCreateInfos = queueCreateInfos.ptr;
			devCreateInfo.pEnabledFeatures = &devFeatures;
			devCreateInfo.enabledExtensionCount = cast(uint)extensionNames.length;
			devCreateInfo.ppEnabledExtensionNames = extensionNames.ptr;
			devCreateInfo.enabledLayerCount = 0;

			// Create logical device
			res = vkCreateDevice(this.physDev, &devCreateInfo, null, &this.dev);
			if(res != VK_SUCCESS)
			{
				Console.get().crit("Could not create vulkan logical device");
				return res;
			}

			// Get necessary queues
			vkGetDeviceQueue(this.dev, this.qfs.gfxFamily, 0, &this.gfxQueue);
			vkGetDeviceQueue(this.dev, this.qfs.presentFamily, 0, &this.presentQueue);
			vkGetDeviceQueue(this.dev, this.qfs.xferFamily, 0, &this.xferQueue);
			vkGetDeviceQueue(this.dev, this.qfs.compFamily, this.qfs.xferQueueCount > 1 ? 1 : 0, &this.compQueue);

			VkSurfaceFormatKHR format;
			res = this.createSwapchain(format);
			if(res != VK_SUCCESS)
				return res;

			res = this.createImageViews(format);
			if(res != VK_SUCCESS)
				return res;

			// Initialize graphics memory
			MemoryManager.get();
			MemoryManager.get().createBuffers(this);
			this.createDescriptorPool();

			this.depthTexture = new Texture();

			for(uint i = 0; i < MAX_FRAMES_IN_FLIGHT; i++)
			{
				this.gBuffer[i] = new Texture();
				this.pingPongTextures[i] = new Texture();
			}

			this.createTexturesForRendering();

			VkAttachmentDescription deferredColorAttachmentAlbedo = {};
			deferredColorAttachmentAlbedo.format = VK_FORMAT_R8G8B8A8_UNORM;
			deferredColorAttachmentAlbedo.samples = VK_SAMPLE_COUNT_1_BIT;
			deferredColorAttachmentAlbedo.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
			deferredColorAttachmentAlbedo.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
			deferredColorAttachmentAlbedo.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
			deferredColorAttachmentAlbedo.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
			deferredColorAttachmentAlbedo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
			deferredColorAttachmentAlbedo.finalLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

			VkAttachmentDescription deferredColorAttachmentWorldNorm = {};
			deferredColorAttachmentWorldNorm.format = VK_FORMAT_R16G16B16A16_SFLOAT;
			deferredColorAttachmentWorldNorm.samples = VK_SAMPLE_COUNT_1_BIT;
			deferredColorAttachmentWorldNorm.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
			deferredColorAttachmentWorldNorm.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
			deferredColorAttachmentWorldNorm.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
			deferredColorAttachmentWorldNorm.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
			deferredColorAttachmentWorldNorm.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
			deferredColorAttachmentWorldNorm.finalLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

			VkAttachmentDescription depthAttachment = {};
			depthAttachment.format = vkHelpers_findDepthFormat(this.physDev);
			depthAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
			depthAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
			depthAttachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
			depthAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
			depthAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
			depthAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
			depthAttachment.finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_READ_ONLY_OPTIMAL;

			VkSubpassDependency[1] dependencyGBuffer;
			dependencyGBuffer[0].srcSubpass = VK_SUBPASS_EXTERNAL;
			dependencyGBuffer[0].dstSubpass = 0;
			dependencyGBuffer[0].srcStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
			dependencyGBuffer[0].srcAccessMask = 0;
			dependencyGBuffer[0].dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT | VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
			dependencyGBuffer[0].dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
			//dependencyGBuffer[0].dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

			//dependencyGBuffer[1].srcSubpass = 0;
			//dependencyGBuffer[1].dstSubpass = VK_SUBPASS_EXTERNAL;
			//dependencyGBuffer[1].srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT | VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
			//dependencyGBuffer[1].srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
			//dependencyGBuffer[1].dstStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT | VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
			//dependencyGBuffer[1].dstAccessMask = VK_ACCESS_MEMORY_READ_BIT;
			//dependencyGBuffer[1].dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

			VkClearValue[2] clearValues = [
											{ color: VkClearColorValue([0.0f, 0.0f, 0.0f, 1.0f]) },
											{ depthStencil: VkClearDepthStencilValue(1.0f, 0) }
										  ];

			RenderPass mapRenderPass = this.addRenderPass();
			mapRenderPass.addAttachment(deferredColorAttachmentAlbedo);
			mapRenderPass.addAttachment(deferredColorAttachmentWorldNorm);
			mapRenderPass.addAttachment(deferredColorAttachmentWorldNorm);
			mapRenderPass.addAttachment(depthAttachment);
			mapRenderPass.addSubpass(VK_PIPELINE_BIND_POINT_GRAPHICS, 3, 0, 3);
			mapRenderPass.addSubpassDependency(dependencyGBuffer[0]);
			//mapRenderPass.addSubpassDependency(dependencyGBuffer[1]);
			mapRenderPass.addClearColor(clearValues[0]);
			mapRenderPass.addClearColor(clearValues[0]);
			mapRenderPass.addClearColor(clearValues[0]);
			mapRenderPass.addClearColor(clearValues[1]);
			res = mapRenderPass.create(this);
			if(res != VK_SUCCESS)
				return res;

			res = mapRenderPass.createFramebuffers(this, this.imgCount, this.gBuffer.ptr, this.gBuffer.length);

			if(res != VK_SUCCESS)
				return res;

			VkAttachmentDescription colorAttachmentScreen = {};
			colorAttachmentScreen.format = format.format;
			colorAttachmentScreen.samples = VK_SAMPLE_COUNT_1_BIT;
			colorAttachmentScreen.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
			colorAttachmentScreen.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
			colorAttachmentScreen.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
			colorAttachmentScreen.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
			colorAttachmentScreen.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
			colorAttachmentScreen.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

			VkSubpassDependency dependencyScreen = {};
			dependencyScreen.srcSubpass = VK_SUBPASS_EXTERNAL;
			dependencyScreen.dstSubpass = 0;
			dependencyScreen.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT | VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT;
			dependencyScreen.srcAccessMask = 0;
			dependencyScreen.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
			dependencyScreen.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;

			RenderPass screenRenderPass = this.addRenderPass();
			screenRenderPass.addAttachment(colorAttachmentScreen);
			screenRenderPass.addSubpass(VK_PIPELINE_BIND_POINT_GRAPHICS, 1, 0, -1);
			screenRenderPass.addSubpassDependency(dependencyScreen);
			screenRenderPass.addClearColor(clearValues[0]);
			res = screenRenderPass.create(this);
			if(res != VK_SUCCESS)
				return res;

			res = screenRenderPass.createFramebuffers(this, this.imgCount);

			if(res != VK_SUCCESS)
				return res;

			// UI render pass
			VkAttachmentDescription colorAttachmentUI = {};
			colorAttachmentUI.format = format.format;
			colorAttachmentUI.samples = VK_SAMPLE_COUNT_1_BIT;
			colorAttachmentUI.loadOp = VK_ATTACHMENT_LOAD_OP_LOAD;
			colorAttachmentUI.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
			colorAttachmentUI.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
			colorAttachmentUI.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
			colorAttachmentUI.initialLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
			colorAttachmentUI.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

			VkSubpassDependency dependency;
			dependency.srcSubpass = VK_SUBPASS_EXTERNAL;
			dependency.dstSubpass = 0;
			dependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
			dependency.srcAccessMask = 0;
			dependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
			dependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;

			RenderPass uiRenderPass = this.addRenderPass();
			uiRenderPass.addAttachment(colorAttachmentUI);
			uiRenderPass.addSubpass(VK_PIPELINE_BIND_POINT_GRAPHICS, 1, 0, -1);
			uiRenderPass.addSubpassDependency(dependency);
			uiRenderPass.addClearColor(clearValues[0]);

			res = uiRenderPass.create(this);
			if(res != VK_SUCCESS)
				return res;

			res = uiRenderPass.createFramebuffers(this, this.imgCount);
			if(res != VK_SUCCESS)
				return res;

			Console.get().log("Loading shaders");
			this.loadShader("shaders/mesh.vert.spv");
			this.loadShader("shaders/map.frag.spv", this.shaders["shaders/mesh.vert.spv"].getPushConstantSize());
			this.loadShader("shaders/fullscreenQuad.vert.spv");
			this.loadShader("shaders/screenOut.frag.spv");
			this.loadShader("shaders/ui.vert.spv");
			this.loadShader("shaders/ui.frag.spv");
			this.loadShader("shaders/tiledCulling.comp.spv");
			this.loadShader("shaders/tiledLighting.comp.spv");
			this.createDescriptorLayoutBindings();
			this.allocateDescriptorSets();

			this.pipelines ~= new MapGraphicsPipeline(this, 0);
			this.pipelines[$ - 1].addShader("shaders/mesh.vert.spv");
			this.pipelines[$ - 1].addShader("shaders/map.frag.spv");

			this.pipelines ~= new TiledCullingComputePipeline();
			this.pipelines[$ - 1].addShader("shaders/tiledCulling.comp.spv");

			this.pipelines ~= new TiledLightingComputePipeline();
			this.pipelines[$ - 1].addShader("shaders/tiledLighting.comp.spv");

			this.pipelines ~= new ScreenOutGraphicsPipeline(this, 1);
			this.pipelines[$ - 1].addShader("shaders/fullscreenQuad.vert.spv");
			this.pipelines[$ - 1].addShader("shaders/screenOut.frag.spv");

			this.pipelines ~= new UIGraphicsPipeline(this, 2);
			this.pipelines[$ - 1].addShader("shaders/ui.vert.spv");
			this.pipelines[$ - 1].addShader("shaders/ui.frag.spv");

			res = createGraphicsPipelines();
			if(res != VK_SUCCESS)
				return res;

			if(res != VK_SUCCESS)
				return res;

			//Command pools
			VkCommandPoolCreateInfo gfxPoolInfo = {};
			gfxPoolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;

			//TODO: Add 3rd for compute
			cmdPools.length = 3;

			foreach(ref cmdPool; this.cmdPools)
				cmdPool = new CommandPool();

			res = this.cmdPools[0].create(this, qfs.gfxFamily, VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT);
			if(res != VK_SUCCESS)
			{
				Console.get().crit("Failed to create usuable graphics command pool");
				return res;
			}

			res = this.cmdPools[1].create(this, qfs.xferFamily, VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT);
			if(res != VK_SUCCESS)
				return res;

			res = this.cmdPools[2].create(this, qfs.compFamily, VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT);
			if(res != VK_SUCCESS)
				return res;

			// Alloc command buffers
			foreach(ref buffer; this.cmdBuffers)
				buffer = new CommandBuffer();

			res = cmdBuffers[0].create(this, &this.cmdPools[0], VK_COMMAND_BUFFER_LEVEL_PRIMARY, MAX_FRAMES_IN_FLIGHT);
			if(res != VK_SUCCESS)
				return res;

			res = cmdBuffers[1].create(this, &this.cmdPools[1], VK_COMMAND_BUFFER_LEVEL_PRIMARY, MAX_FRAMES_IN_FLIGHT);
			if(res != VK_SUCCESS)
				return res;

			//res = cmdBuffers[2].create(this, &this.cmdPools[2], VK_COMMAND_BUFFER_LEVEL_PRIMARY, MAX_FRAMES_IN_FLIGHT);
			//if(res != VK_SUCCESS)
			//	return res;

			VkSemaphoreCreateInfo semaphoreInfo = {};
			semaphoreInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

			this.imageAvailableSemaphores[0 .. $] = VK_NULL_HANDLE;
			this.renderFinishedSemaphores[0 .. $] = VK_NULL_HANDLE;
			this.transferFinishedSemaphores[0 .. $] = VK_NULL_HANDLE;

			foreach(i; 0 .. MAX_FRAMES_IN_FLIGHT)
			{
				if(vkCreateSemaphore(this.dev, &semaphoreInfo, null, &this.imageAvailableSemaphores[i]) != VK_SUCCESS
				|| vkCreateSemaphore(this.dev, &semaphoreInfo, null, &this.renderFinishedSemaphores[i]) != VK_SUCCESS
				|| vkCreateSemaphore(this.dev, &semaphoreInfo, null, &this.transferFinishedSemaphores[i]) != VK_SUCCESS)
				{
					Console.get().crit("Failed to create semaphores");
					return -1;
				}
			}

			VkFenceCreateInfo fenceInfo = {};
			fenceInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
			fenceInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;

			this.gfxFences[0 .. $] = VK_NULL_HANDLE;

			foreach(ref fence; this.gfxFences)
				res = vkCreateFence(this.dev, &fenceInfo, null, &fence);

			this.xferFences[0 .. $] = VK_NULL_HANDLE;
			foreach(ref fence; this.xferFences)
				res = vkCreateFence(this.dev, &fenceInfo, null, &fence);

			if(res != VK_SUCCESS)
			{
				Console.get().crit("Failed to create fence");
				return res;
			}

			// Load assets
			//Vertex[3] vertices = [
			//						Vertex(vec3f(-1.0, -1.0, 0.0), vec2f(0.0, 0.0)),
			//						Vertex(vec3f(-1.0,  3.0, 0.0), vec2f(0.0, 2.0)),
			//						Vertex(vec3f( 3.0, -1.0, 0.0), vec2f(2.0, 0.0))
			//					 ];
//
			//uint[3] indices = [0, 1, 2];
			//uint[1] offsets = [0];
			//uint[1] amounts = [3];

			// Quad Verts
			Vertex[4] vertices = [
									Vertex(vec3(-1.0, 0.0, -1.0), vec2(0.0, 0.0), vec3(0.0f, 1.0f, 0.0f), vec3(1.0f, 0.0f, 0.0f)),
									Vertex(vec3(-1.0, 0.0,  1.0), vec2(0.0, 1.0), vec3(0.0f, 1.0f, 0.0f), vec3(1.0f, 0.0f, 0.0f)),
									Vertex(vec3( 1.0, 0.0, -1.0), vec2(1.0, 0.0), vec3(0.0f, 1.0f, 0.0f), vec3(1.0f, 0.0f, 0.0f)),
									Vertex(vec3( 1.0, 0.0,  1.0), vec2(1.0, 1.0), vec3(0.0f, 1.0f, 0.0f), vec3(1.0f, 0.0f, 0.0f))
								 ];

			uint[6] indices = [ 0, 1, 2, 2, 1, 3 ];
			uint[1] offsets = [0];
			uint[1] amounts = [6];

			this.textures.length = 2;
			foreach(ref texture; this.textures)
			{
				texture = new Texture();
			}
			textures[0].create(this, ivec3(512, 512, 1), false, 5, VK_IMAGE_TYPE_2D, false, VK_FORMAT_R8G8B8A8_UNORM);
			textures[1].create(this, ivec3(256, 256, 1), false, 5, VK_IMAGE_TYPE_2D, false, VK_FORMAT_R8G8B8A8_UNORM);

			// Use gfx queue for these commands
			res = this.cmdBuffers[0].reset();
			if(res != VK_SUCCESS)
				return res;

			res = this.cmdBuffers[0].begin(0, null);
			if(res != VK_SUCCESS)
				return res;

			VkCommandBuffer current = this.cmdBuffers[0].getCurrent();

			this.meshes.length = 1;
			this.meshes[0] = new Mesh();
			this.meshes[0].create(vertices, indices, offsets, amounts);
			this.meshes[0].transferData(this, current);
			this.meshes[0].transform.scale = vec3(10.0, 10.0, 10.0);

			this.textures[0].loadTextures(this, ["assets/terrain.png", "assets/roads.png"], current);
			this.textures[1].loadTextures(this, ["assets/terrainMap.png", "assets/roadsMap.png"], current);

			foreach(pipeline; this.pipelines)
				pipeline.createCustomData(this, current);

			res = this.cmdBuffers[0].end();
			if(res != VK_SUCCESS)
				return res;

			VkSubmitInfo submitInfo = {};
			submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
			submitInfo.commandBufferCount = 1;
			submitInfo.pCommandBuffers = &current;

			res = vkQueueSubmit(this.gfxQueue, 1, &submitInfo, null);
			if(res != VK_SUCCESS)
				return res;

			// TODO: Fence this?
			vkQueueWaitIdle(this.gfxQueue);

			this.meshes[0].addInstance();
			(cast(GraphicsPipeline)this.pipelines[0]).addMesh(&this.meshes[0], 0);

			this.textures[0].createSampler(this,
										   VK_FILTER_NEAREST,
										   VK_FILTER_NEAREST,
										   VK_SAMPLER_MIPMAP_MODE_LINEAR,
										   VK_SAMPLER_ADDRESS_MODE_REPEAT,
										   VK_SAMPLER_ADDRESS_MODE_REPEAT,
										   VK_SAMPLER_ADDRESS_MODE_REPEAT,
										   VK_BORDER_COLOR_INT_OPAQUE_BLACK,
										   VK_FALSE);
			this.textures[0].createImageView(this);

			this.textures[1].createSampler(this,
										   VK_FILTER_NEAREST,
										   VK_FILTER_NEAREST,
										   VK_SAMPLER_MIPMAP_MODE_LINEAR,
										   VK_SAMPLER_ADDRESS_MODE_REPEAT,
										   VK_SAMPLER_ADDRESS_MODE_REPEAT,
										   VK_SAMPLER_ADDRESS_MODE_REPEAT,
										   VK_BORDER_COLOR_INT_OPAQUE_BLACK,
										   VK_FALSE);
			this.textures[1].createImageView(this);

			foreach(i; 0 .. MAX_FRAMES_IN_FLIGHT)
			{
				this.sceneCameraBlock[i] = MemoryManager.get().allocDeviceLocalBuffer(SceneCameraInfo.sizeof);
				this.sceneLightingBlock[i] = MemoryManager.get().allocDeviceLocalBuffer(PointLight.sizeof * 1024 + DirectionalLight.sizeof);
				this.createTiledDeferredBuffer();
			}

			this.createDescriptorWrites();

			this.camera = new Camera();
			this.camera.transform.position = vec3(-2.0f, 10.0f, 0.0f);
			this.camera.transform.setRotationFromEuler(vec3(degtorad(-45.0f), degtorad(22.5f), 0.0f));

			float aspect = this.ext.width / this.ext.height;
			this.camera.setProjection(-aspect * 5.0f * 2.0f, aspect * 5.0f * 2.0f, 1.0f * 5.0f, -1.0f * 5.0f, 0.05f, 100.0f);
			this.camera.setProjection(85.0f, aspect, 0.05f, 100.0f);
			this.camera._update();

			this.sceneLighting.dirLight = DirectionalLight(Light(vec3(1.0, 1.0, 1.0), 0.5), vec3(0.0, 1.0, 0.0));

			import std.random;
			foreach(ref light; this.sceneLighting.pointLights)
			{
				vec3 offset = vec3(cast(float)uniform(0, 101) / 100.0f - 0.5, cast(float)uniform(0, 101) / 100.0f - 0.5, cast(float)uniform(0, 101) / 100.0f - 0.5);
				vec3 color = vec3(cast(float)uniform(0, 101) / 100.0f, cast(float)uniform(0, 101) / 100.0f, cast(float)uniform(0, 101) / 100.0f);
				float intensity = cast(float)uniform(0, 2) + (cast(float)uniform(0, 11) / 10.0f);
				vec3 pos = vec3(cast(float)uniform(0, 21) - 10.0f + (cast(float)uniform(0, 11) / 10.0f), 0.5, cast(float)uniform(0, 21) - 10.0 + (cast(float)uniform(0, 11) / 10.0f));
				float range = cast(float)uniform(0, 1) + (cast(float)uniform(0, 11) / 10.0f);
				light = PointLight(Light(color + offset, intensity), pos, range);
			}

			Timer.get().update();

			return 0;
		}

		int render()
		{
			Timer.get().update();
			MemoryManager.get().invalidateTransferBuffer();

			// Skip rendering if the window is minimized
			if(Input.get().isMinimized())
				return VK_SUCCESS;

			VkResult res;

			if(this.doShaderRecompile)
			{
				Console.get().log("Reloading Shaders");

				string exeDir = thisExePath().dirName ~ "/..";

				string inputDir = exeDir ~ "/source/vivarium/graphics/shaders";
				string pythonScript = exeDir ~ "/compileshaders.py";
				string outDir = exeDir ~ "/bin/shaders";

				Tuple!(int, "status", string, "output") compileShaders;

				version(Windows)
				{
					compileShaders = executeShell(["python " ~ pythonScript ~ " " ~ inputDir ~ " " ~ outDir]);
				}

				version(linux)
				{
					compileShaders = executeShell("python3 " ~ pythonScript ~ " " ~ inputDir ~ " " ~ outDir);
				}

				if(compileShaders.status != 0)
				{
					Console.get().err("Compiling shaders failed, please examine logs");
					Console.get().err(compileShaders.output);
				}

				else
					Console.get().log(compileShaders.output);

				vkDeviceWaitIdle(this.dev);
				
				this.resetDescriptorSets();
				foreach(shader; this.shaders)
				{
					shader.create(this, -1);
				}
				this.createDescriptorLayoutBindings();
				this.allocateDescriptorSets();
				
				this.recreateGraphicsPipelines();
				this.createDescriptorWrites();
				this.doShaderRecompile = false;

				core.memory.GC.collect();
			}

			if(Input.get().isKeyPressed(SDLK_UP))
			{
				camera.move(this.camera.transform.up * Timer.get().getDeltaTime() * 1.0);
			}

			if(Input.get().isKeyPressed(SDLK_DOWN))
			{
				camera.move(-this.camera.transform.up() * Timer.get().getDeltaTime() * 1.0);
			}

			if(Input.get().isKeyPressed(SDLK_RIGHT))
			{
				camera.move(this.camera.transform.right() * Timer.get().getDeltaTime() * 1.0);
			}

			if(Input.get().isKeyPressed(SDLK_LEFT))
			{
				camera.move(-this.camera.transform.right() * Timer.get().getDeltaTime() * 1.0);
			}

			this.camera._update();

			foreach(ref mesh; this.meshes)
				mesh.transform._update();

			BufferBlock camTransferblock = MemoryManager.get().allocTransfer(SceneCameraInfo.sizeof);
			MemoryManager.get().copyIntoTransferBuffer(this, &this.camera.getSceneCameraInfo(), camTransferblock);

			BufferBlock lightTransferBlock = MemoryManager.get().allocTransfer(SceneLighting.sizeof);
			MemoryManager.get().copyIntoTransferBuffer(this, &this.sceneLighting, lightTransferBlock);

			// Render a frame
			VkFence[2] fences = [ this.gfxFences[this.curFrame], this.xferFences[this.curFrame] ];
			res = vkWaitForFences(this.dev, 2, fences.ptr, VK_TRUE, ulong.max);
			if(res != VK_SUCCESS)
			{
				Console.get().crit("Failed while waiting for a fence");
				return res;
			}

			if(Input.get().isResizing())
			{
				this.recreateSwapchain();
				return VK_SUCCESS;
			}

			uint imageIndex = 0;

			res = vkAcquireNextImageKHR(this.dev,
								  		this.swapchain,
								  		ulong.max,
								  		this.imageAvailableSemaphores[this.curFrame],
								  		VK_NULL_HANDLE,
								  		&imageIndex);

			if(res == VK_ERROR_OUT_OF_DATE_KHR)
			{
				this.recreateSwapchain();
				return VK_SUCCESS;
			}

			else if(res != VK_SUCCESS && res != VK_SUBOPTIMAL_KHR)
			{
				Console.get().crit("Failed when acquiring an image");
				return res;
			}

			res = vkResetFences(this.dev, 2, fences.ptr);
			if(res != VK_SUCCESS)
			{
				Console.get().crit("Failed to reset a fence");
				return res;
			}

			//res = this.cmdBuffers[2].reset();
			//if(res != VK_SUCCESS)
			//	return res;
//
			//res = this.cmdBuffers[2].begin(0, null);
			//if(res != VK_SUCCESS)
			//	return res;
//
			//VkCommandBuffer computeBuffer = this.cmdBuffers[2].getCurrent();
			//
			//foreach(ref pipeline; this.computePipelines)
			//	pipeline.drawPass(this, computeBuffer, curFrame, imageIndex);
//
			//res = this.cmdBuffers[2].end();
			//if(res != VK_SUCCESS)
			//	return res;

			res = this.cmdBuffers[1].reset();
			if(res != VK_SUCCESS)
				return res;

			res = this.cmdBuffers[1].begin(0, null);
			if(res != VK_SUCCESS)
				return res;

			// Perform 
			VkCommandBuffer xferBuffer = this.cmdBuffers[1].getCurrent();
			MemoryManager.get().copyToDeviceLocalBuffer(xferBuffer, camTransferblock, this.sceneCameraBlock[this.curFrame]);
			MemoryManager.get().copyToDeviceLocalBuffer(xferBuffer, lightTransferBlock, this.sceneLightingBlock[this.curFrame]);
			res = this.cmdBuffers[1].end();
			if(res != VK_SUCCESS)
				return res;

			VkSemaphore[1] xferSignalSemaphores = [ this.transferFinishedSemaphores[this.curFrame] ];

			VkSubmitInfo xferSubmitInfo = {};
			xferSubmitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
			xferSubmitInfo.waitSemaphoreCount = 0;
			xferSubmitInfo.pWaitSemaphores = null;
			xferSubmitInfo.pWaitDstStageMask = null;
			xferSubmitInfo.commandBufferCount = 1;
			xferSubmitInfo.pCommandBuffers = &xferBuffer;
			xferSubmitInfo.signalSemaphoreCount = 1;
			xferSubmitInfo.pSignalSemaphores = xferSignalSemaphores.ptr;

			res = vkQueueSubmit(this.xferQueue, 1, &xferSubmitInfo, this.xferFences[this.curFrame]);

			res = this.cmdBuffers[0].reset();
			if(res != VK_SUCCESS)
				return res;

			res = this.cmdBuffers[0].begin(0, null);
			if(res != VK_SUCCESS)
				return res;

			VkCommandBuffer gfxBuffer = this.cmdBuffers[0].getCurrent();

			if(this.freshSwapchain)
			{
				foreach(pingPongTex; this.pingPongTextures)
				{
					MemoryManager.get().transitionImageLayout(gfxBuffer,
															  pingPongTex,
															  VK_IMAGE_LAYOUT_UNDEFINED,
															  VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
															  0,
															  VK_ACCESS_SHADER_WRITE_BIT,
															  VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT,
															  VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT);
				}

				this.freshSwapchain = false;
			}

			foreach(ref pipeline; this.pipelines)
				pipeline.drawPass(this, gfxBuffer, curFrame, imageIndex);

			res = this.cmdBuffers[0].end();
			if(res != VK_SUCCESS)
				return res;

			VkSemaphore[2] waitSemaphores = [
												this.imageAvailableSemaphores[this.curFrame],
												this.transferFinishedSemaphores[this.curFrame]
											];
			VkPipelineStageFlags[2] waitStages = [
													VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
													VK_PIPELINE_STAGE_TRANSFER_BIT
												 ];
			VkSemaphore[1] signalSemaphores = [ this.renderFinishedSemaphores[this.curFrame] ];

			VkSubmitInfo submitInfo = {};
			submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
			submitInfo.waitSemaphoreCount = 2;
			submitInfo.pWaitSemaphores = waitSemaphores.ptr;
			submitInfo.pWaitDstStageMask = waitStages.ptr;
			submitInfo.commandBufferCount = 1;
			submitInfo.pCommandBuffers = &gfxBuffer;
			submitInfo.signalSemaphoreCount = 1;
			submitInfo.pSignalSemaphores = signalSemaphores.ptr;

			res = vkQueueSubmit(this.gfxQueue, 1, &submitInfo, this.gfxFences[this.curFrame]);

			VkSwapchainKHR[1] swapchains = [ this.swapchain ];

			VkPresentInfoKHR presentInfo = {};
			presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
			presentInfo.waitSemaphoreCount = 1;
			presentInfo.pWaitSemaphores = signalSemaphores.ptr;
			presentInfo.swapchainCount = 1;
			presentInfo.pSwapchains = swapchains.ptr;
			presentInfo.pImageIndices = &imageIndex;
			presentInfo.pResults = null;

			vkQueuePresentKHR(this.presentQueue, &presentInfo);

			if(res == VK_ERROR_OUT_OF_DATE_KHR || res == VK_SUBOPTIMAL_KHR || Input.get().isResizing())
				this.recreateSwapchain();

			else if(res != VK_SUCCESS)
			{
				Console.get().crit("Failed to submit draw command buffer");
				return res;
			}

			this.curFrame = (this.curFrame + 1) % MAX_FRAMES_IN_FLIGHT;

			//core.memory.GC.collect();
			return res;
		}

		void addDynamicOffset(int setNum, string name, uint offset)
		{
			if(name !in this.bindings[setNum])
				this.dynamicOffsets[setNum] ~= offset;
		}

		void addDescriptorLayout(int setNum, string bindingName, VkDescriptorSetLayoutBinding binding)
		{
			if(bindingName !in this.bindings[setNum])
				this.bindings[setNum][bindingName] = binding;
			else
				this.bindings[setNum][bindingName].stageFlags |= binding.stageFlags;
		}

		VkResult createDescriptorLayoutBindings()
		{
			VkResult res = VK_ERROR_UNKNOWN;
			VkDescriptorSetLayoutBinding[] bindings;
			for(int i = 0; i < 3; i++)
			{
				bindings.length = 0;
				foreach(binding; this.bindings[i])
				{
					bindings ~= binding;
				}

				VkDescriptorSetLayoutCreateInfo descriptorLayoutCreateInfo = {};
				descriptorLayoutCreateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
				descriptorLayoutCreateInfo.bindingCount = cast(uint)bindings.length;
				descriptorLayoutCreateInfo.pBindings = bindings.ptr;

				this.setLayouts[i] = VkDescriptorSetLayout();

				res = vkCreateDescriptorSetLayout(this.dev,
												  &descriptorLayoutCreateInfo,
												  null,
												  &this.setLayouts[i]);

				if (res != VK_SUCCESS)
					return res;
			}

			return res;
		}

		void updateDescriptorSets(VkWriteDescriptorSet[] descriptorWrites)
		{
			vkUpdateDescriptorSets(this.dev,
								   cast(uint)descriptorWrites.length,
								   descriptorWrites.ptr,
								   0,
								   null);
		}

		void addPushConstantRange(string name, VkPushConstantRange range)
		{
			if(name !in this.pushConstantRanges)
				this.pushConstantRanges[name] = range;
		}

		@nogc VkDescriptorPool getDescriptorPool()
		{
			return this.descriptorPool;
		}

		@nogc ref VkDescriptorSetLayout[3] getDescriptorSetLayouts()
		{
			return this.setLayouts;
		}

		@nogc uint[] getDynamicOffsets(uint i)
		{
			return this.dynamicOffsets[i];
		}

		@nogc VkPushConstantRange getPushConstantRanges(string name)
		{
			return this.pushConstantRanges[name];
		}

		@nogc ref VkDescriptorSet getDescriptorSet(int set, int index)
		{
			return this.descriptorSets[set][index];
		}

		@nogc ref VkPhysicalDevice getPhysicalDevice()
		{
			return this.physDev;
		}

		@nogc ref VkDevice getDevice()
		{
			return this.dev;
		}

		@nogc ref VkPhysicalDeviceProperties getDeviceProperties()
		{
			return this.curDeviceProperties;
		}

		@nogc VkImageView getImageView(int num)
		{
			return this.imgViews[num].getImageView();
		}

		@nogc Texture getDepthTexture()
		{
			return this.depthTexture;
		}

		@nogc Texture getCurrentGBufferTextures()
		{
			return this.gBuffer[this.curFrame];
		}

		@nogc Texture getCurrentPingPongTexture()
		{
			return this.pingPongTextures[this.curFrame];
		}

		@nogc uint getImageCount()
		{
			return this.imgCount;
		}

		@nogc VkExtent2D getExtents()
		{
			return this.ext;
		}

		@nogc Camera getActiveCamera()
		{
			return camera;
		}

		@nogc uint getMaxFramesInFlight()
		{
			return this.MAX_FRAMES_IN_FLIGHT;
		}

		void recompileShaders()
		{
			this.doShaderRecompile = true;
		}

		@nogc VkShaderModule getShaderModule(string name)
		{
			if(name in this.shaders)
				return this.shaders[name].getShaderModule();
			else
				return VK_NULL_HANDLE;
		}

		@nogc ref ShaderInfo getShaderInfo(string name)
		{
			//if(name in this.shaders)
			return this.shaders[name].getInfo();
			//else
			//	return ShaderInfo();
		}

		@nogc ref RenderPass getRenderPass(int num)
		{
			return this.passes[num];
		}
		
	private:
		// Variables
		const(char*)[] extensionNames;
		VkInstance instance = VK_NULL_HANDLE;
		VkSurfaceKHR surface = VK_NULL_HANDLE;
		VkPhysicalDevice physDev = VK_NULL_HANDLE;
		VkDevice dev = VK_NULL_HANDLE;
		VkQueue gfxQueue = VK_NULL_HANDLE;
		VkQueue presentQueue = VK_NULL_HANDLE;
		VkQueue xferQueue = VK_NULL_HANDLE;
		VkQueue compQueue = VK_NULL_HANDLE;
		VkExtent2D ext;
		VkSwapchainKHR swapchain = VK_NULL_HANDLE;
		ImageView[] imgViews;

		VkPipelineLayout pipelineLayout = VK_NULL_HANDLE;
		VkRenderPass[] renderPasses;
		Pipeline[] pipelines;
		Framebuffer[] framebuffers;
		CommandPool[] cmdPools;
		CommandBuffer[3] cmdBuffers;
		VkSemaphore[MAX_FRAMES_IN_FLIGHT] imageAvailableSemaphores;
		VkSemaphore[MAX_FRAMES_IN_FLIGHT] renderFinishedSemaphores;
		VkSemaphore[MAX_FRAMES_IN_FLIGHT] transferFinishedSemaphores;
		VkFence[MAX_FRAMES_IN_FLIGHT] gfxFences;
		VkFence[MAX_FRAMES_IN_FLIGHT] xferFences;

		Buffer* staticDataBuffer = null;
		Buffer* stagingBuffer = null;
		
		uint imgCount = 0;

		BufferBlock[MAX_FRAMES_IN_FLIGHT] sceneCameraBlock;
		BufferBlock[MAX_FRAMES_IN_FLIGHT] sceneLightingBlock;
		BufferBlock[MAX_FRAMES_IN_FLIGHT] tileInfoBlock;

		Camera camera;

		debug
		{
			VkDebugUtilsMessengerEXT debugMessenger;
		}

		//Descriptor Stuff
		VkDescriptorPool descriptorPool;

		VkPushConstantRange[string] pushConstantRanges;
		VkDescriptorSetLayout[3] setLayouts;
		VkDescriptorSetLayoutBinding[string][3] bindings;
		uint[][3] dynamicOffsets;
		VkDescriptorSet[2][3] descriptorSets;

		//Renderpasses
		RenderPass[] passes;

		//TODO: load everything from folders instead
		Shader[string] shaders;
		Mesh[] meshes;
		Texture[] textures;

		Texture depthTexture;
		Texture[2] gBuffer;
		Texture[2] pingPongTextures;

		VkPhysicalDeviceProperties curDeviceProperties;
		const(int) MAX_FRAMES_IN_FLIGHT = 2;
		uint curFrame = 0;

		QueueFamilies qfs;

		bool doShaderRecompile = false;
		bool freshSwapchain = true;

		SceneLighting sceneLighting;

		static bool isInstanceCreated = false;

		void loadShader(string fileName, int pushConstantOffset = 0)
		{
			this.shaders[fileName] = new Shader(fileName);
			this.shaders[fileName].create(this, pushConstantOffset);
		}

		void freeSwapchain()
		{
			foreach(ref framebuffer; this.framebuffers)
				framebuffer.free(this);

			foreach(ref view; this.imgViews)
				view.free(this);

			vkDestroySwapchainKHR(this.dev, this.swapchain, null);
		}

		VkResult createSwapchain(out VkSurfaceFormatKHR format)
		{
			//Create swapchain
			SwapChainSupportDetails details = vkHelpers_querySwapChainSupport(this.physDev, this.surface);

			if(details.formats.length == 0 || details.presentModes.length == 0)
			{
				Console.get().crit("Cannot find valid swapchain details!");
				return VK_ERROR_UNKNOWN;
			}

			format = vkHelpers_chooseSwapSurfaceFormat(details.formats);
			VkPresentModeKHR mode = vkHelpers_choosePresentMode(details.presentModes);

			ivec2 size = Engine.get().getWindowSize();
			this.ext = vkHelpers_chooseExtents(details.capabilities, size.x, size.y);

			uint imageCount = details.capabilities.minImageCount + 1;

			if(details.capabilities.maxImageCount > 0 && imageCount > details.capabilities.maxImageCount)
				imageCount = details.capabilities.maxImageCount;

			VkSwapchainCreateInfoKHR swapchainCreateInfo = {};
			swapchainCreateInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
			swapchainCreateInfo.surface = this.surface;
			swapchainCreateInfo.minImageCount = imageCount;
			swapchainCreateInfo.imageFormat = format.format;
			swapchainCreateInfo.imageColorSpace = format.colorSpace;
			swapchainCreateInfo.imageExtent = this.ext;
			swapchainCreateInfo.imageArrayLayers = 1;
			swapchainCreateInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
			swapchainCreateInfo.preTransform = details.capabilities.currentTransform;
			swapchainCreateInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
			swapchainCreateInfo.presentMode = mode;
			swapchainCreateInfo.clipped = VK_TRUE; //Disable?
			swapchainCreateInfo.oldSwapchain = VK_NULL_HANDLE;

			if(this.qfs.presentFamily != this.qfs.gfxFamily)
			{
				uint[2] queueFamIndices = [this.qfs.gfxFamily, this.qfs.presentFamily];

				swapchainCreateInfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
				swapchainCreateInfo.queueFamilyIndexCount = 2;
				swapchainCreateInfo.pQueueFamilyIndices = queueFamIndices.ptr;
			}

			else
			{
				swapchainCreateInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
				swapchainCreateInfo.queueFamilyIndexCount = 0;
				swapchainCreateInfo.pQueueFamilyIndices = null;
			}

			VkResult res = vkCreateSwapchainKHR(this.dev, &swapchainCreateInfo, null, &this.swapchain);
			if(res != VK_SUCCESS)
				Console.get().crit("Failed to create swapchain!");

			this.freshSwapchain = true;

			return res;
		}

		VkResult createImageViews(VkSurfaceFormatKHR format)
		{
			//Get swap chain images
			vkGetSwapchainImagesKHR(this.dev, this.swapchain, &this.imgCount, null);
			VkImage[] swapchainImgs;
			swapchainImgs.length = this.imgCount;
			swapchainImgs[0 .. $] = VK_NULL_HANDLE;
			vkGetSwapchainImagesKHR(this.dev, this.swapchain, &this.imgCount, swapchainImgs.ptr);

			//Create image views
			this.imgViews.length = this.imgCount;

			VkResult res = VK_ERROR_UNKNOWN;

			foreach(i, ref imgView; this.imgViews)
			{
				VkImageViewCreateInfo ivCreateInfo = {};
				ivCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
				ivCreateInfo.image = swapchainImgs[i];
				ivCreateInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
				ivCreateInfo.format = format.format;
				ivCreateInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
				ivCreateInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
				ivCreateInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
				ivCreateInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
				ivCreateInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
				ivCreateInfo.subresourceRange.baseMipLevel = 0;
				ivCreateInfo.subresourceRange.levelCount = 1;
				ivCreateInfo.subresourceRange.baseArrayLayer = 0;
				ivCreateInfo.subresourceRange.layerCount = 1;

				imgView = new ImageView();
				res = imgView.create(this, ivCreateInfo, -1);
				if(res != VK_SUCCESS)
				{
					Console.get().crit("Failed to create an image view!");
					return res;
				}
			}

			return res;
		}

		VkResult recreateSwapchain()
		{
			vkDeviceWaitIdle(this.dev);

			this.freeSwapchain();

			VkSurfaceFormatKHR format;
			VkResult res = this.createSwapchain(format);
			if(res != VK_SUCCESS)
				return res;

			res = this.createImageViews(format);
			if(res != VK_SUCCESS)
				return res;

			this.recreateTexturesForRendering();
			
			res = this.recreateFramebuffers(this.imgCount);
			if(res != VK_SUCCESS)
				return res;

			this.recreateGraphicsPipelines();
			this.createDescriptorWrites();

			float aspect = this.ext.width / this.ext.height;
			this.camera.setProjection(-aspect * 5.0, aspect * 5.0, 1.0f * 5.0, -1.0f * 5.0, 0.05f, 100.0f);
			this.camera.setProjection(85.0f, aspect, 0.05f, 100.0f);

			this.createTiledDeferredBuffer();

			core.memory.GC.collect();
			
			return res;
		}

		void createTiledDeferredBuffer()
		{
			for(uint i = 0; i < MAX_FRAMES_IN_FLIGHT; i++)
				MemoryManager.get().freeDeviceLocalBuffer(this, this.tileInfoBlock[i]);

			vec2 dimensions = vec2(this.ext.width, this.ext.height) / 32.0;
			dimensions = vec2(ceil(dimensions.x), ceil(dimensions.y));

			for(uint i = 0; i < MAX_FRAMES_IN_FLIGHT; i++)
				this.tileInfoBlock[i] = MemoryManager.get().allocDeviceLocalBuffer(cast(int)(TileInfo.sizeof * dimensions.x * dimensions.y));
		}

		void addCurrentViewportAndScissorToPipeline(GraphicsPipeline pipeline)
		{
			VkViewport viewport;
			viewport.x = 0.0;
			viewport.y = 0.0;
			viewport.width = cast(float)this.ext.width;
			viewport.height = cast(float)this.ext.height;
			viewport.minDepth = 0.0f;
			viewport.maxDepth = 1.0f;

			VkRect2D scissor;
			scissor.offset = VkOffset2D(0, 0);
			scissor.extent = this.ext;

			pipeline.clearViewports();
			pipeline.clearScissor();
			pipeline.addViewport(viewport);
			pipeline.addScissor(scissor);
		}

		VkResult createGraphicsPipelines()
		{
			VkResult finalRes = VK_SUCCESS;
			foreach(i, ref pipeline; this.pipelines)
			{
				if(GraphicsPipeline pipe = cast(GraphicsPipeline)pipeline)
				{
					this.addCurrentViewportAndScissorToPipeline(pipe);
				}

				VkResult res = pipeline.create(this, 1);
				if(res != VK_SUCCESS)
				{
					Console.get().err("Failed creating graphics pipeline #" ~ to!string(i));
					finalRes = res;
				}
			}

			return finalRes;
		}

		VkResult recreateGraphicsPipelines()
		{
			VkResult finalRes = VK_SUCCESS;
			foreach(i, ref pipeline; this.pipelines)
			{
				if(GraphicsPipeline pipe = cast(GraphicsPipeline)pipeline)
				{
					this.addCurrentViewportAndScissorToPipeline(pipe);
				}

				VkResult res = pipeline.recreate(this, 1);
				if(res != VK_SUCCESS)
				{
					Console.get().err("Failed creating graphics pipeline #" ~ to!string(i));
					finalRes = res;
				}
			}

			return finalRes;
		}

		VkResult createDescriptorPool()
		{
			VkDescriptorPoolSize[5] poolSizes;
			poolSizes[0].type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
			poolSizes[0].descriptorCount = 256;
			poolSizes[1].type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
			poolSizes[1].descriptorCount = 256;
			poolSizes[2].type = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
			poolSizes[2].descriptorCount = 256;
			poolSizes[3].type = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC;
			poolSizes[3].descriptorCount = 256;
			poolSizes[4].type = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE;
			poolSizes[4].descriptorCount = 256;

			VkDescriptorPoolCreateInfo descriptorPoolCreateInfo = {};
			descriptorPoolCreateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
			descriptorPoolCreateInfo.poolSizeCount = cast(uint)poolSizes.length;
			descriptorPoolCreateInfo.pPoolSizes = poolSizes.ptr;
			descriptorPoolCreateInfo.maxSets = 256;
			//descriptorPoolCreateInfo.flags = VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT;

			VkResult res = vkCreateDescriptorPool(this.dev, 
												  &descriptorPoolCreateInfo,
												  null,
												  &this.descriptorPool);
			if(res != VK_SUCCESS)
			{
				Console.get().err("Failed to create descriptor pool");
			}

			return res;
		}

		ref RenderPass addRenderPass()
		{
			this.passes ~= new RenderPass();
			return this.passes[$ - 1];
		}

		VkResult recreateFramebuffers(int amount)
		{
			VkResult res = VK_ERROR_UNKNOWN;
			foreach(pass; passes)
			{
				res = pass.createFramebuffers(this, amount, pass.getTexturesPtr(), pass.getTexturesLength());
			}

			return res;
		}

		VkResult allocateDescriptorSets()
		{
			VkResult res;

			for(uint i = 0; i < 3; i++)
			{
				VkDescriptorSetLayout[2] layouts = [ this.setLayouts[i], this.setLayouts[i] ];

				VkDescriptorSetAllocateInfo allocInfo = {};
				allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
				allocInfo.descriptorPool = this.descriptorPool;
				allocInfo.descriptorSetCount = layouts.length;
				allocInfo.pSetLayouts = layouts.ptr;

				this.descriptorSets[i] = [ VK_NULL_HANDLE, VK_NULL_HANDLE ];

				res = vkAllocateDescriptorSets(this.dev, &allocInfo, this.descriptorSets[i].ptr);
				if(res != VK_SUCCESS)
					Console.get().err("Failed to allocate descriptor set at " ~ i.to!string);
			}

			return res;
		}

		void resetDescriptorSets()
		{
			foreach(layout; this.setLayouts)
			{
				if(layout != VK_NULL_HANDLE)
					vkDestroyDescriptorSetLayout(this.dev, layout, null);
			}

			this.setLayouts[0..$] = VK_NULL_HANDLE;
			this.descriptorSets[0..$] = [ VK_NULL_HANDLE, VK_NULL_HANDLE];
			for(uint i = 0; i < 3; i++)
			{
				this.dynamicOffsets[i].length = 0;
				this.bindings[i].clear();
			}

			this.pushConstantRanges.clear();
		}

		void freeDescriptorPool()
		{
			if(this.descriptorPool != VK_NULL_HANDLE)
			{
				vkDestroyDescriptorPool(this.dev, this.descriptorPool, null);
				this.descriptorPool = VK_NULL_HANDLE;
			}
		}

		void createTexturesForRendering()
		{
			this.createDepthBuffer();
			this.createGBuffer();
			this.createPingPongBuffer();
		}

		void recreateTexturesForRendering()
		{
			this.recreateDepthBuffer();
			this.recreateGBuffer();
			this.recreatePingPongBuffer();
		}

		void createDepthBuffer()
		{
			this.depthTexture.create(this,
									 ivec3(this.ext.width,
									 	   this.ext.height,
										   1),
									 false,
									 1,
									 VK_IMAGE_TYPE_2D,
									 false,
									 vkHelpers_findDepthFormat(this.physDev),
									 false);
			this.depthTexture.createSampler(this,
											VK_FILTER_LINEAR,
										    VK_FILTER_LINEAR,
										    VK_SAMPLER_MIPMAP_MODE_LINEAR,
										    VK_SAMPLER_ADDRESS_MODE_REPEAT,
										    VK_SAMPLER_ADDRESS_MODE_REPEAT,
										    VK_SAMPLER_ADDRESS_MODE_REPEAT,
										    VK_BORDER_COLOR_INT_OPAQUE_BLACK,
										    VK_FALSE);
			this.depthTexture.createImageView(this, VK_IMAGE_VIEW_TYPE_2D);
		}

		void recreateDepthBuffer()
		{
			this.depthTexture.free(this);
			this.createDepthBuffer();
		}

		void createGBuffer()
		{
			this.gBuffer[0].create(this,
								   ivec3(this.ext.width,
								   		 this.ext.height,
										 1),
								   false,
								   1,
								   VK_IMAGE_TYPE_2D,
								   false,
								   VK_FORMAT_R8G8B8A8_UNORM);
			this.gBuffer[0].createSampler(this,
										  VK_FILTER_LINEAR,
										  VK_FILTER_LINEAR,
										  VK_SAMPLER_MIPMAP_MODE_LINEAR,
										  VK_SAMPLER_ADDRESS_MODE_REPEAT,
										  VK_SAMPLER_ADDRESS_MODE_REPEAT,
										  VK_SAMPLER_ADDRESS_MODE_REPEAT,
										  VK_BORDER_COLOR_INT_OPAQUE_BLACK,
										  VK_FALSE);
			this.gBuffer[0].createImageView(this, VK_IMAGE_VIEW_TYPE_2D);

			this.gBuffer[1].create(this,
								   ivec3(this.ext.width,
								   		 this.ext.height,
										 1),
								   false,
								   2,
								   VK_IMAGE_TYPE_2D,
								   false,
								   VK_FORMAT_R16G16B16A16_SFLOAT);
			this.gBuffer[1].createSampler(this,
										  VK_FILTER_LINEAR,
										  VK_FILTER_LINEAR,
										  VK_SAMPLER_MIPMAP_MODE_LINEAR,
										  VK_SAMPLER_ADDRESS_MODE_REPEAT,
										  VK_SAMPLER_ADDRESS_MODE_REPEAT,
										  VK_SAMPLER_ADDRESS_MODE_REPEAT,
										  VK_BORDER_COLOR_INT_OPAQUE_BLACK,
										  VK_FALSE);									   
			this.gBuffer[1].createImageView(this, VK_IMAGE_VIEW_TYPE_2D, 0, 1);
			this.gBuffer[1].createImageView(this, VK_IMAGE_VIEW_TYPE_2D, 1, 1);
		}

		void recreateGBuffer()
		{
			foreach(buffer; gBuffer)
				buffer.free(this);
				
			this.createGBuffer();
		}

		void createPingPongBuffer()
		{
			for(uint i = 0; i < MAX_FRAMES_IN_FLIGHT; i++)
			{
				this.pingPongTextures[i].create(this,
												ivec3(this.ext.width,
													  this.ext.height,
													  1),
												false,
												1,
												VK_IMAGE_TYPE_2D,
												false,
												VK_FORMAT_R8G8B8A8_UNORM);
				this.pingPongTextures[i].createSampler(this,
													   VK_FILTER_LINEAR,
													   VK_FILTER_LINEAR,
													   VK_SAMPLER_MIPMAP_MODE_LINEAR,
													   VK_SAMPLER_ADDRESS_MODE_REPEAT,
													   VK_SAMPLER_ADDRESS_MODE_REPEAT,
													   VK_SAMPLER_ADDRESS_MODE_REPEAT,
													   VK_BORDER_COLOR_INT_OPAQUE_BLACK,
													   VK_FALSE);
				this.pingPongTextures[i].createImageView(this, VK_IMAGE_VIEW_TYPE_2D);
			}
		}

		void recreatePingPongBuffer()
		{
			for(uint i = 0; i < MAX_FRAMES_IN_FLIGHT; i++)
				this.pingPongTextures[i].free(this);

			this.createPingPongBuffer();
		}

		void createDescriptorWrites()
		{
			// 0, 0 = SceneCamera
			// 0, 1 = SceneLighting
			// 0, 2 = TiledDeferredInfo
			// 0, 3 = screenTexture
			// 0, 4 = uiTexture

			//GBuffer
			// 1, 0 = albedo / metal
			// 1, 1 = normal / ao
			// 1, 2 = world
			// 1, 3 = roughness / emissive 
			// 1, 4 = depth

			// 2, 0 = tileTexs array
			// 2, 1 = mapTexs array
			// 2, 2 = storge image

			foreach(i; 0 .. this.MAX_FRAMES_IN_FLIGHT)
			{
				VkDescriptorBufferInfo camBufferInfo = {};
				camBufferInfo.buffer = MemoryManager.get().getDeviceLocalBuffer(this.sceneCameraBlock[i].poolId).getBuffer();
				camBufferInfo.offset = this.sceneCameraBlock[i].offset;
				camBufferInfo.range = SceneCameraInfo.sizeof;

				VkDescriptorBufferInfo lightingBufferInfo = {};
				lightingBufferInfo.buffer = MemoryManager.get().getDeviceLocalBuffer(this.sceneLightingBlock[i].poolId).getBuffer();
				lightingBufferInfo.offset = this.sceneLightingBlock[i].offset;
				lightingBufferInfo.range = this.sceneLightingBlock[i].size;

				VkDescriptorBufferInfo tileBufferInfo = {};
				tileBufferInfo.buffer = MemoryManager.get().getDeviceLocalBuffer(this.tileInfoBlock[i].poolId).getBuffer();
				tileBufferInfo.offset = this.tileInfoBlock[i].offset;
				tileBufferInfo.range = this.tileInfoBlock[i].size;

				VkDescriptorImageInfo[] textureInfos;
				foreach(texture; this.textures)
				{
					VkDescriptorImageInfo info = {};
					info.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
					info.imageView = texture.getImageView(0).getImageView();
					info.sampler = texture.getSampler();
					textureInfos ~= info;
				}
				
				VkDescriptorImageInfo gAlbedoMetalInfo = {};
				gAlbedoMetalInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
				gAlbedoMetalInfo.imageView = this.gBuffer[0].getImageView(0).getImageView();
				gAlbedoMetalInfo.sampler = this.gBuffer[0].getSampler();

				VkDescriptorImageInfo gNormalAoInfo = {};
				gNormalAoInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
				gNormalAoInfo.imageView = this.gBuffer[1].getImageView(0).getImageView();
				gNormalAoInfo.sampler = this.gBuffer[1].getSampler();

				VkDescriptorImageInfo gWorldInfo = {};
				gWorldInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
				gWorldInfo.imageView = this.gBuffer[1].getImageView(1).getImageView();
				gWorldInfo.sampler = this.gBuffer[1].getSampler();

				VkDescriptorImageInfo depthInfo = {};
				depthInfo.imageLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_READ_ONLY_OPTIMAL;
				depthInfo.imageView = this.depthTexture.getImageView(0).getImageView();
				depthInfo.sampler = this.depthTexture.getSampler();

				VkDescriptorImageInfo pingpongInfo = {};
				pingpongInfo.imageLayout = VK_IMAGE_LAYOUT_GENERAL;
				pingpongInfo.imageView = this.pingPongTextures[i].getImageView(0).getImageView();
				pingpongInfo.sampler = this.pingPongTextures[i].getSampler();

				VkDescriptorImageInfo screenInfo = {};
				screenInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
				screenInfo.imageView = this.pingPongTextures[i].getImageView(0).getImageView();
				screenInfo.sampler = this.pingPongTextures[i].getSampler();

				//TODO: Auto create these from shader and store them
				VkWriteDescriptorSet[11] writes = {};
				writes[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
				writes[0].dstSet = this.getDescriptorSet(0, i);
				writes[0].dstBinding = 0;
				writes[0].dstArrayElement = 0;
				writes[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
				writes[0].descriptorCount = 1;
				writes[0].pBufferInfo = &camBufferInfo;

				writes[1].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
				writes[1].dstSet = this.getDescriptorSet(0, i);
				writes[1].dstBinding = 1;
				writes[1].dstArrayElement = 0;
				writes[1].descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
				writes[1].descriptorCount = 1;
				writes[1].pBufferInfo = &lightingBufferInfo;

				writes[2].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
				writes[2].dstSet = this.getDescriptorSet(0, i);
				writes[2].dstBinding = 2;
				writes[2].dstArrayElement = 0;
				writes[2].descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC;
				writes[2].descriptorCount = 1;
				writes[2].pBufferInfo = &tileBufferInfo;

				writes[3].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
				writes[3].dstSet = this.getDescriptorSet(0, i);
				writes[3].dstBinding = 3;
				writes[3].dstArrayElement = 0;
				writes[3].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
				writes[3].descriptorCount = 1;
				writes[3].pImageInfo = &screenInfo;

				writes[4].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
				writes[4].dstSet = this.getDescriptorSet(1, i);
				writes[4].dstBinding = 0;
				writes[4].dstArrayElement = 0;
				writes[4].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
				writes[4].descriptorCount = 1;
				writes[4].pImageInfo = &gAlbedoMetalInfo;

				writes[5].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
				writes[5].dstSet = this.getDescriptorSet(1, i);
				writes[5].dstBinding = 1;
				writes[5].dstArrayElement = 0;
				writes[5].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
				writes[5].descriptorCount = 1;
				writes[5].pImageInfo = &gNormalAoInfo;

				writes[6].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
				writes[6].dstSet = this.getDescriptorSet(1, i);
				writes[6].dstBinding = 2;
				writes[6].dstArrayElement = 0;
				writes[6].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
				writes[6].descriptorCount = 1;
				writes[6].pImageInfo = &gWorldInfo;

				//writes[6].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
				//writes[6].dstSet = this.getDescriptorSet("gRoughnessEmissive", i);
				//writes[6].dstBinding = 3;
				//writes[6].dstArrayElement = 0;
				//writes[6].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
				//writes[6].descriptorCount = 1;
				//writes[6].pImageInfo = &depthInfo;

				writes[7].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
				writes[7].dstSet = this.getDescriptorSet(1, i);
				writes[7].dstBinding = 4;
				writes[7].dstArrayElement = 0;
				writes[7].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
				writes[7].descriptorCount = 1;
				writes[7].pImageInfo = &depthInfo;

				writes[8].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
				writes[8].dstSet = this.getDescriptorSet(2, i);
				writes[8].dstBinding = 0;
				writes[8].dstArrayElement = 0;
				writes[8].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
				writes[8].descriptorCount = 1;
				writes[8].pImageInfo = &textureInfos[0];

				writes[9].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
				writes[9].dstSet = this.getDescriptorSet(2, i);
				writes[9].dstBinding = 1;
				writes[9].dstArrayElement = 0;
				writes[9].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
				writes[9].descriptorCount = 1;
				writes[9].pImageInfo = &textureInfos[1];

				writes[10].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
				writes[10].dstSet = this.getDescriptorSet(2, i);
				writes[10].dstBinding = 2;
				writes[10].dstArrayElement = 0;
				writes[10].descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE;
				writes[10].descriptorCount = 1;
				writes[10].pImageInfo = &pingpongInfo;

				this.updateDescriptorSets(writes);
			}
		}

		void free()
		{
			vkDeviceWaitIdle(this.dev);

			foreach(pass; this.passes)
			{
				pass.free(this);
			}

			foreach(shader; this.shaders)
			{
				shader.free(this);
			}

			this.resetDescriptorSets();
			this.freeDescriptorPool();

			foreach(ref texture; this.textures)
				texture.free(this);

			foreach(ref mesh; this.meshes)
				mesh.free(this);

			for(uint i = 0; i < MAX_FRAMES_IN_FLIGHT; i++)
			{
				this.pingPongTextures[i].free(this);
				this.gBuffer[i].free(this);
			}

			depthTexture.free(this);

			// Cleanup vulkan
			foreach(ref fence; this.xferFences)
				vkDestroyFence(this.dev, fence, null);

			foreach(ref fence; this.gfxFences)
				vkDestroyFence(this.dev, fence, null);

			foreach(ref semaphore; this.transferFinishedSemaphores)
				vkDestroySemaphore(this.dev, semaphore, null);

			foreach(ref semaphore; this.renderFinishedSemaphores)
				vkDestroySemaphore(this.dev, semaphore, null);

			foreach(ref semaphore; this.imageAvailableSemaphores)
				vkDestroySemaphore(this.dev, semaphore, null);

			foreach(ref cmdBuffer; this.cmdBuffers)
				cmdBuffer.free(this);

			foreach(ref cmdPool; this.cmdPools)
				cmdPool.free(this);

			foreach(ref pipeline; this.pipelines)
				pipeline.free(this);

			foreach(ref renderPass; this.renderPasses)
				vkDestroyRenderPass(this.dev, renderPass, null);

			vkDestroyPipelineLayout(this.dev, this.pipelineLayout, null);

			for(uint i = 0; i < MAX_FRAMES_IN_FLIGHT; i++)
			{
				MemoryManager.get().freeDeviceLocalBuffer(this, this.sceneCameraBlock[i]);
				MemoryManager.get().freeDeviceLocalBuffer(this, this.sceneLightingBlock[i]);
				MemoryManager.get().freeDeviceLocalBuffer(this, this.tileInfoBlock[i]);
			}


			MemoryManager.release();
			this.freeSwapchain();

			vkDestroyDevice(this.dev, null);
			vkDestroySurfaceKHR(this.instance, this.surface, null);

			debug
			{
				vkDestroyDebugUtilsMessengerEXT(this.instance, this.debugMessenger, null);
			}

			vkDestroyInstance(this.instance, null);
		}
}