module vivarium.graphics.vkhelpers;

import std.string,
	   std.conv,
	   std.algorithm,
	   std.stdio;

import erupted;

import vivarium.core.console;

public:

struct QueueFamilyInfo
{
	int idx = -1;
	int count = 0;
}

struct SwapChainSupportDetails
{
	VkSurfaceCapabilitiesKHR capabilities;
	VkSurfaceFormatKHR[] formats;
	VkPresentModeKHR[] presentModes;
}

struct QueueFamilies
{
	int gfxFamily = -1;
	int presentFamily = -1;
	int xferFamily = -1;
	int compFamily = -1;
	int xferQueueCount = -1;
}

VkQueueFamilyProperties[int] vkHelpers_findQueueFamilies(VkPhysicalDevice devCandidate)
{
	VkQueueFamilyProperties[int] qfis;

	// Get queue family count
	uint queueFamCount = 0;
	vkGetPhysicalDeviceQueueFamilyProperties(devCandidate, &queueFamCount, null);

	// Get queue family properties
	VkQueueFamilyProperties[] queueFamProperties;
	queueFamProperties.length = queueFamCount;
	vkGetPhysicalDeviceQueueFamilyProperties(devCandidate, &queueFamCount, queueFamProperties.ptr);

	// Get all available queue families and their properties
	for(uint i = 0; i < queueFamProperties.length; i++)
	{
		if(queueFamProperties[i].queueCount > 0 )
			qfis[i] = queueFamProperties[i];
	}

	return qfis;
}

int vkHelpers_getDeviceSuitability(VkPhysicalDevice devCandidate, const(char*)[] extensionNames)
{
	int score = 0;
	VkPhysicalDeviceProperties devProperties;
	vkGetPhysicalDeviceProperties(devCandidate, &devProperties);

	uint extensionCount = 0;
	vkEnumerateDeviceExtensionProperties(devCandidate, null, &extensionCount, null);

	if(extensionCount == 0)
		return 0;

	VkExtensionProperties[] availableExtensions;
	availableExtensions.length = extensionCount;
	vkEnumerateDeviceExtensionProperties(devCandidate, null, &extensionCount, availableExtensions.ptr);

	bool[string] requiredExtensions;

	foreach (ext; extensionNames)
		requiredExtensions[to!string(ext)] = true;

	foreach (extension; availableExtensions)
	{
		string extName = to!string(fromStringz(extension.extensionName));
		requiredExtensions.remove(extName);
	}

	if(requiredExtensions.length != 0)
		return 0;

	VkPhysicalDeviceFeatures features;
	vkGetPhysicalDeviceFeatures(devCandidate, &features);

	//TODO: Add more feature checks ?
	if(!features.geometryShader
	|| !features.tessellationShader
	|| !features.multiDrawIndirect
	|| !features.samplerAnisotropy)
		return 0;

	if(devProperties.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU)
		score += 1000;

	score += devProperties.limits.maxImageDimension2D;

	return score;
}

SwapChainSupportDetails vkHelpers_querySwapChainSupport(VkPhysicalDevice devCandidate, VkSurfaceKHR surface)
{
	SwapChainSupportDetails details;

	uint formatCount;
	vkGetPhysicalDeviceSurfaceCapabilitiesKHR(devCandidate, surface, &details.capabilities);
	vkGetPhysicalDeviceSurfaceFormatsKHR(devCandidate, surface, &formatCount, null);

	if(formatCount != 0)
	{
		details.formats.length = formatCount;
		vkGetPhysicalDeviceSurfaceFormatsKHR(devCandidate, surface, &formatCount, details.formats.ptr);
	}

	uint presentModeCount;
	vkGetPhysicalDeviceSurfacePresentModesKHR(devCandidate, surface, &presentModeCount, null);

	if(presentModeCount != 0)
	{
		details.presentModes.length = presentModeCount;
		vkGetPhysicalDeviceSurfacePresentModesKHR(devCandidate,
												  surface,
												  &presentModeCount,
												  details.presentModes.ptr);
	}

	return details;
}

VkSurfaceFormatKHR vkHelpers_chooseSwapSurfaceFormat(VkSurfaceFormatKHR[] formats)
{
	foreach(format; formats)
	{
		if(format.format == VK_FORMAT_B8G8R8A8_SRGB && format.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR)
			return format;
	}

	return formats[0];
}

VkFormat vkHelpers_findDepthFormat(VkPhysicalDevice dev)
{
	return vkHelpers_chooseFormat(dev,
								  [ VK_FORMAT_D32_SFLOAT, VK_FORMAT_D32_SFLOAT_S8_UINT, VK_FORMAT_D24_UNORM_S8_UINT ], 
								  VK_IMAGE_TILING_OPTIMAL,
								  VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT);
}

bool vkHelpers_isDepthFormat(VkFormat format)
{
	return format == VK_FORMAT_D32_SFLOAT
		|| format == VK_FORMAT_D32_SFLOAT_S8_UINT
		|| format == VK_FORMAT_D24_UNORM_S8_UINT;
}

bool vkHelpers_isStencilFormat(VkFormat format)
{
	return format == VK_FORMAT_D32_SFLOAT_S8_UINT || format == VK_FORMAT_D24_UNORM_S8_UINT;
}

VkPresentModeKHR vkHelpers_choosePresentMode(VkPresentModeKHR[] presentModes)
{
	foreach(mode; presentModes)
	{
		if(mode == VK_PRESENT_MODE_MAILBOX_KHR)
			return mode;
	}

	return VK_PRESENT_MODE_FIFO_KHR;
}

VkExtent2D vkHelpers_chooseExtents(VkSurfaceCapabilitiesKHR capabilities, int width, int height)
{
	if(capabilities.currentExtent.width != int.max)
		return capabilities.currentExtent;

	VkExtent2D extent = {
							max(min(width, capabilities.maxImageExtent.width), capabilities.minImageExtent.width),
							max(min(height, capabilities.maxImageExtent.height), capabilities.minImageExtent.height)
						};
	return extent;
}

void vkHelpers_checkError(VkResult res, const(char) errorMsg)
{
	//if(res != VK_SUCCESS)
	//{
	//	throw mallocNew!Exception(errorMsg);
	//}
}

extern(C) VkBool32 vkHelpers_vulkanDebugCallback(VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity, 
							 					 VkDebugUtilsMessageTypeFlagsEXT messageTypes,
												 const(VkDebugUtilsMessengerCallbackDataEXT)* pCallbackData,
							 					 void* pUserData) nothrow @nogc
{
	if(messageSeverity >= VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT)
	{
		fprintf(core.stdc.stdio.stdout, "Vulkan validation error: %s\n", pCallbackData.pMessage);
		fprintf(core.stdc.stdio.stdout, "Related vulkan object handles:\n");
		for(uint i = 0; i < pCallbackData.objectCount; i++)
		{
			fprintf(core.stdc.stdio.stdout,
					"\tName: %s Type: %d, Handle: %llu\n",
					pCallbackData.pObjects[i].pObjectName,
					pCallbackData.pObjects[i].objectType,
					pCallbackData.pObjects[i].objectHandle);
		}
	}
	return VK_FALSE;
}

private:
VkFormat vkHelpers_chooseFormat(VkPhysicalDevice dev,
								VkFormat[] formats,
								VkImageTiling tiling,
								VkFormatFeatureFlags features)
{
	foreach(format; formats)
	{
		VkFormatProperties props;
		vkGetPhysicalDeviceFormatProperties(dev, format, &props);

		if(tiling == VK_IMAGE_TILING_LINEAR && (props.optimalTilingFeatures & features) == features)
			return format;
		else if(tiling == VK_IMAGE_TILING_OPTIMAL && (props.optimalTilingFeatures & features) == features)
			return format;
	}

	Console.get().err("Could not find supported format");
	return formats[0];
}