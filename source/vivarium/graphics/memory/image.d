module vivarium.graphics.memory.image;

import std.stdio,
	   std.algorithm;

import erupted,
	   dlib.math;

import vivarium.core.console,
	   vivarium.graphics.graphicscontext,
	   vivarium.graphics.memory.memorypool;

struct GpuSubImage
{
	uvec3 offset;
    uvec3 size;
	uint idx;
	uint arrayLayer;
	uint poolId;
}

class GpuImage
{
	public:
		this()
		{
			this.pool = new MemoryPool();
			this.subImages.reserve(50);
		}

		VkResult create(GraphicsContext gc,
						uvec3 size,
                        uint mipLevels,
                        uint arrayLayers,
                        VkImageType type,
                        VkFormat format,
                        VkSampleCountFlagBits samples,
                        VkImageTiling tiling,
                        VkImageUsageFlags flags,
                        VkSharingMode sharingMode,
                        VkMemoryPropertyFlags memFlags,
						int imageIndex)
		{
			this.free(gc);

			if(size.x > gc.getDeviceProperties().limits.maxImageDimension2D)
			{
				writeln("Requested x size too large, using max image size instead");
				size.x = gc.getDeviceProperties().limits.maxImageDimension2D;
			}

			if(size.y > gc.getDeviceProperties().limits.maxImageDimension2D)
			{
				writeln("Requested y size too large, using max image size instead");
				size.y = gc.getDeviceProperties().limits.maxImageDimension2D;
			}

			if(size.z > gc.getDeviceProperties().limits.maxImageDimension2D)
			{
				writeln("Requested x size too large, using max image size instead");
				size.z = gc.getDeviceProperties().limits.maxImageDimension2D;
			}

			this.mipLevels = mipLevels;

			VkImageCreateInfo createInfo = {};
			createInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
			createInfo.extent = VkExtent3D(size.x, size.y, size.z);
			createInfo.mipLevels = this.mipLevels;
			createInfo.arrayLayers = arrayLayers;
			createInfo.imageType = type;
			createInfo.format = format;
			createInfo.samples = samples;
			createInfo.tiling = tiling;
			createInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
			createInfo.usage = flags;
			createInfo.sharingMode = sharingMode;

			VkResult res = vkCreateImage(gc.getDevice(), &createInfo, null, &this.image);

			if(res != VK_SUCCESS)
			{
				Console.get().err("Error creating image");
				return res;
			}


			VkMemoryRequirements memRequirements;
			vkGetImageMemoryRequirements(gc.getDevice(), this.image, &memRequirements);
            res = this.pool.allocateMemory(gc, memRequirements, memFlags);

			this.format = format;
			this.maxSize = size;
			this.maxLayer = arrayLayers - 1;
			this.curOffsets.length = arrayLayers;
			this.curOffsets[0 .. $] = uvec3(0);

			if(res != VK_SUCCESS)
			{
				Console.get().err("Failed to allocate memory for an image");
				return res;
			}

			vkBindImageMemory(gc.getDevice(), this.image, this.pool.getMemory(), 0);
			this.index = imageIndex;

			return res;
		}

		GpuSubImage* alloc(uvec3 size, uint layer)
		{
			if(layer > maxLayer)
			{
				Console.get().err("Layer outside of bounds");
				return null;
			}

			if(size.x + this.curOffsets[layer].x > this.maxSize.x
			|| size.y + this.curOffsets[layer].y > this.maxSize.y
			|| size.z + this.curOffsets[layer].z > this.maxSize.z)
			{
				Console.get().err("No more free memory available");
				return null;
			}

			this.subImages ~= GpuSubImage(this.curOffsets[layer], size, cast(uint)this.subImages.length, layer, this.index);
			this.curOffsets[layer] += size;
			return &this.subImages[$ - 1];
		}

		void freeSubImage(ref GpuSubImage* subImg)
		{
			if(subImg == null)
				return;
			
			uint idx = subImg.idx;
			uint layer = subImg.arrayLayer;
			uvec3 offset = subImg.offset;
			foreach(i; idx + 1 .. this.subImages.length)
			{
				if(this.subImages[i].arrayLayer == layer)
				{
					this.subImages[i].offset -= offset;
				}

				this.subImages[i].idx = cast(uint)i - 1;
			}
			this.subImages = this.subImages.remove(subImg.idx);
		
			subImg = null;
		}

		void invalidate()
		{
			this.curLayer = 0;
			this.curOffsets[0 .. $] = uvec3(0);
			this.subImages.length = 0;
			this.subImages.reserve(100);
		}

		void free(GraphicsContext gc)
		{
			if(this.image != VK_NULL_HANDLE)
			{
				vkDestroyImage(gc.getDevice(), this.image, null);
				this.image = VK_NULL_HANDLE;
			}

			this.pool.free(gc);
		}

		@nogc VkImage getImage()
		{
			return this.image;
		}

		@nogc VkFormat getFormat()
		{
			return this.format;
		}

		@nogc int getMipLevels()
		{
			return this.mipLevels;
		}

		@nogc int getIndex()
		{
			return this.index;
		}

	private:
		VkImage image = VK_NULL_HANDLE;
		VkFormat format;
		uint curLayer = 0;
		uint maxLayer = 0;
		uint mipLevels = 0;
		uvec3[] curOffsets;
		uvec3 maxSize = uvec3(0);

		GpuSubImage[] subImages;
		MemoryPool pool;
		int index = -1;

		//void destroyImage(GraphicsContext gc)
		//{
		//	if(this.image != VK_NULL_HANDLE)
		//	{
		//		vkDestroyImage(gc.getDevice(), this.image, null);
		//		this.image = VK_NULL_HANDLE;
		//	}
		//}
}