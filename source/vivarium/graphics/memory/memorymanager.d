module vivarium.graphics.memory.memorymanager;

import std.stdio,
	   std.range.primitives,
	   core.memory;

import erupted,
	   dlib.math,
	   dlib.core,
	   dlib.container: Array;

import vivarium.core.console,
	   vivarium.graphics.graphicscontext,
	   vivarium.graphics.texture,
	   vivarium.graphics.memory.memorypool,
	   vivarium.graphics.memory.buffer,
	   vivarium.graphics.memory.image;

class MemoryManager
{
	public:
		static MemoryManager get()
    	{
    	    if (!MemoryManager.instantiated_)
    	    {
    	        synchronized(MemoryManager.classinfo)
    	        {
    	            if (!MemoryManager.instance_)
    	            {
    	                MemoryManager.instance_ = new MemoryManager();
    	            }

    	            MemoryManager.instantiated_ = true;
    	        }
    	    }

    	    return MemoryManager.instance_;
    	}

		static void release()
		{
			synchronized(MemoryManager.classinfo)
    	    {
    	        if (MemoryManager.instance_)
    	        {
					foreach(ref buffer; MemoryManager.instance_.deviceLocalBuffers)
						destroy(buffer);

					destroy(MemoryManager.instance_.transferBuffer);
					destroy(MemoryManager.instance_);
    	        }
    	    }
		}

		VkResult createBuffers(GraphicsContext gc)
		{
			VkResult res = VK_ERROR_UNKNOWN;

			// Alloc 256 megs for device local buffers
			foreach(ref buffer; this.deviceLocalBuffers)
			{
				//536_870_9126UL
				buffer = new Buffer();
				res = buffer.create(gc,
									268_435_456UL,
									VK_BUFFER_USAGE_TRANSFER_DST_BIT
									 | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT
									 | VK_BUFFER_USAGE_INDEX_BUFFER_BIT
									 | VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT
									 | VK_BUFFER_USAGE_STORAGE_BUFFER_BIT,
									VK_SHARING_MODE_EXCLUSIVE,
									VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

				if(res != VK_SUCCESS)
					return res;
			}

			// Alloc 256 megs for static data buffer
			this.transferBuffer = new Buffer();
			res = this.transferBuffer.create(gc,
											 268_435_456UL,
										 	 VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
											 VK_SHARING_MODE_EXCLUSIVE,
											 VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);

			return res;
		}

		void free(GraphicsContext gc)
		{
			foreach(i, ref image; MemoryManager.instance_.deviceLocalImages)
			{
				bool initialized = true;
				foreach(imgNum; MemoryManager.instance_.freeImageSlots)
					if(imgNum == i)
						initialized = false;

				if(initialized)
					image.free(gc);
			}

			this.deviceLocalImages.length = 0;
			this.freeImageSlots.length = 0;
		}

		void freeImage(GraphicsContext gc, int imgNum)
		{
			this.deviceLocalImages[imgNum].free(gc);
			this.freeImageSlots ~= imgNum;
		}

		VkResult createImage(GraphicsContext gc,
							 uvec3 size,
							 uint mipLevels,
							 uint arrayLayers,
							 VkImageType type,
							 VkFormat format,
							 VkSampleCountFlagBits samples,
							 VkImageTiling tiling,
							 VkImageUsageFlags flags,
							 VkSharingMode sharingMode,
							 VkMemoryPropertyFlags memFlags,
							 out GpuImage img)
		{
			int index;
			if(this.freeImageSlots.length > 0)
			{
				index = this.freeImageSlots[$ - 1];
				this.freeImageSlots.popBack();
				this.deviceLocalImages[index] = new GpuImage();
			}

			else
			{
				this.deviceLocalImages ~= new GpuImage();
				index = cast(int)this.deviceLocalImages.length - 1;
			}

			VkResult res = this.deviceLocalImages[index].create(gc,
																size,
																mipLevels,
																arrayLayers,
																type,
																format,
																samples,
																tiling,
																flags,
																sharingMode,
																memFlags,
																index);

			if(res == VK_SUCCESS)
				img = this.deviceLocalImages[index];

			// Create images
			return res;
		}

		BufferBlock allocDeviceLocalBuffer(VkDeviceSize size)
		{
			BufferBlock block;
			foreach(i, ref buffer; this.deviceLocalBuffers)
			{
				block = buffer.alloc(size);

				if(block.isValid())
				{
					block.poolId = cast(uint)i;
					return block;
				}
			}

			Console.get().err("No more available buffers found");
			return block;
		}

		void freeDeviceLocalBuffer(GraphicsContext gc, BufferBlock block)
		{
			this.deviceLocalBuffers[block.poolId].freeBlock(block);
		}

		BufferBlock allocTransfer(VkDeviceSize size)
		{
			BufferBlock block = this.transferBuffer.alloc(size);

			if(block.isValid())
				return block;

			Console.get().warn("Transfer buffer full");
			return block;
		}

		void freeTransfer(GraphicsContext gc, BufferBlock block)
		{
			this.transferBuffer.freeBlock(block);
		}

		void copyToDeviceLocalBuffer(VkCommandBuffer commandBuffer,
									 BufferBlock srcBlock,
									 BufferBlock dstBlock,
									 int dstOffset = 0,
									 int size = 0)
		{
			VkBufferCopy copyRegion = {};
			copyRegion.srcOffset = srcBlock.offset;
			copyRegion.dstOffset = dstBlock.offset + dstOffset;
			copyRegion.size = size == 0 ? srcBlock.size : size;
			vkCmdCopyBuffer(commandBuffer,
							this.transferBuffer.getBuffer(),
							this.deviceLocalBuffers[dstBlock.poolId].getBuffer(),
							1,
							&copyRegion);
		}

		void transitionImageLayout(VkCommandBuffer commandBuffer,
								   Texture tex,
								   VkImageLayout oldLayout,
								   VkImageLayout newLayout,
								   VkAccessFlags srcMask,
								   VkAccessFlags dstMask,
								   VkPipelineStageFlags srcFlags,
								   VkPipelineStageFlags dstFlags)
		{
			//SubImage* dst = tex.getSubImage();

			VkImageMemoryBarrier barrier = {};
			barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
			barrier.oldLayout = oldLayout;
			barrier.newLayout = newLayout;
			barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
			barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
			barrier.image = tex.getImage().getImage();
			barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
			barrier.subresourceRange.baseMipLevel = 0;
			barrier.subresourceRange.levelCount = tex.getMipLevels();
			barrier.subresourceRange.baseArrayLayer = 0;
			barrier.subresourceRange.layerCount = tex.getLayers();
			barrier.srcAccessMask = srcMask;
			barrier.dstAccessMask = dstMask;

			//if(oldLayout == VK_IMAGE_LAYOUT_UNDEFINED && newLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL)
			//{
			//	barrier.srcAccessMask = 0;
	    	//	barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
//
	    	//	srcStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
	    	//	dstStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
			//}
//
			//else if(oldLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL && newLayout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL)
			//{
			//	barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
			//	barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
//
			//	srcStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
	    	//	dstStage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
			//}
//
			//else
			//{
			//	barrier.srcAccessMask = 0;
			//	barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
//
			//	srcStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
	    	//	dstStage = VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT;
			//}

			vkCmdPipelineBarrier(commandBuffer, srcFlags, dstFlags, 0, 0, null, 0, null, 1, &barrier);
		}

		void copyToDeviceLocalImage(VkCommandBuffer commandBuffer,
									BufferBlock src,
									GpuSubImage* dst)
		{
			VkBufferImageCopy copyRegion = {};
			copyRegion.bufferOffset = src.offset;
			copyRegion.bufferRowLength = 0;
			copyRegion.bufferImageHeight = 0;
			copyRegion.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
			copyRegion.imageSubresource.mipLevel = 0;
			copyRegion.imageSubresource.baseArrayLayer = dst.arrayLayer;
			copyRegion.imageSubresource.layerCount = 1;
			copyRegion.imageOffset = VkOffset3D(dst.offset.x, dst.offset.y, dst.offset.z);
			copyRegion.imageExtent = VkExtent3D(dst.size.x, dst.size.y, dst.size.z);

			vkCmdCopyBufferToImage(commandBuffer,
								   transferBuffer.getBuffer(),
								   this.deviceLocalImages[dst.poolId].getImage(),
								   VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
								   1,
								   &copyRegion);
		}

		void generateMipmaps(VkCommandBuffer commandBuffer, Texture tex, VkFilter filter)
		{
			uvec3 mipSize = tex.getSize();
			//SubImage* dst = tex.getSubImages();

			VkImageMemoryBarrier barrier = {};
			barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
			barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
			barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
			barrier.image = tex.getImage().getImage();
			barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
			barrier.subresourceRange.levelCount = 1;
			barrier.subresourceRange.baseArrayLayer = 0;
			barrier.subresourceRange.layerCount = tex.getLayers();

			foreach(i; 1 .. tex.getMipLevels())
			{
				barrier.subresourceRange.baseMipLevel = i - 1;
				barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
				barrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
				barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
				barrier.dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;

				vkCmdPipelineBarrier(commandBuffer,
	    							 VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT, 0,
	    							 0,
									 null,
	    							 0,
									 null,
	    							 1,
									 &barrier);

				uvec3 halfSize = uvec3(mipSize.x > 1 ? mipSize.x / 2 : 1,
									   mipSize.y > 1 ? mipSize.y / 2 : 1,
									   mipSize.z > 1 ? mipSize.z / 2 : 1);

				VkImageBlit blit = {};
				blit.srcOffsets[0] = VkOffset3D(0, 0, 0);
				blit.srcOffsets[1] = VkOffset3D(mipSize.x, mipSize.y, mipSize.z);
				blit.srcSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
				blit.srcSubresource.mipLevel = i - 1;
				blit.srcSubresource.baseArrayLayer = 0;
				blit.srcSubresource.layerCount = tex.getLayers();
				blit.dstOffsets[0] = VkOffset3D(0, 0, 0);
				blit.dstOffsets[1] = VkOffset3D(halfSize.x, halfSize.y, halfSize.z);
				blit.dstSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
				blit.dstSubresource.mipLevel = i;
				blit.dstSubresource.baseArrayLayer = 0;
				blit.dstSubresource.layerCount = tex.getLayers();

				vkCmdBlitImage(commandBuffer,
							   tex.getImage().getImage(),
							   VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
							   tex.getImage().getImage(),
							   VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
							   1,
							   &blit,
							   filter);

				barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
				barrier.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
				barrier.srcAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
				barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

				vkCmdPipelineBarrier(commandBuffer,
	    							 VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, 0,
	    							 0,
									 null,
	    							 0,
									 null,
	    							 1,
									 &barrier);

				mipSize = halfSize;
			}

			barrier.subresourceRange.baseMipLevel = tex.getMipLevels() - 1;
			barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
			barrier.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
			barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
			barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

			vkCmdPipelineBarrier(commandBuffer,
	    						 VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, 0,
	    						 0,
								 null,
	    						 0,
								 null,
	    						 1,
								 &barrier);
		}

		VkResult copyIntoTransferBuffer(GraphicsContext gc,
										const void* inputData,
										BufferBlock block,
										int offset = 0,
										int size = 0)
		{
			return this.transferBuffer.copyDataInto(gc, inputData, block, offset, size);
		}

		void invalidateTransferBuffer()
		{
			this.transferBuffer.invalidate();
		}

		ref Buffer getDeviceLocalBuffer(uint loc)
		{
			return this.deviceLocalBuffers[loc];
		}

		ref GpuImage getDeviceLocalImage(uint loc)
		{
			return this.deviceLocalImages[loc];
		}

		ref Buffer getTransferBuffer()
		{
			return this.transferBuffer;
		}

	private:
		Buffer[2] deviceLocalBuffers;
		GpuImage[] deviceLocalImages;
		Buffer transferBuffer;
		//Buffer perframeBuffer; TODO: Finish this
		int[] freeImageSlots;

		// Thread local
		static bool instantiated_;

    	// Thread global
		__gshared MemoryManager instance_;

		this()
		{
			
		}
}