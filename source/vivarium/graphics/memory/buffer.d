module vivarium.graphics.memory.buffer;

import std.stdio,
	   std.algorithm,
	   std.container.slist,
	   core.stdc.string;

import erupted;

import vivarium.core.console,
	   vivarium.graphics.graphicscontext,
	   vivarium.graphics.memory.memorypool;

struct BufferBlock
{
	public:
		VkDeviceSize offset;
		VkDeviceSize size;
		uint poolId;

		bool isValid()
		{
			return valid;
		}

	private:
		bool valid = false;
}

class Buffer
{
	public:
		this()
		{
			this.pool = new MemoryPool();
		}

		VkResult create(GraphicsContext gc,
						ulong size,
						VkBufferUsageFlags flags,
						VkSharingMode sharingMode,
						VkMemoryPropertyFlags memFlags)
		{
			this.free(gc);

			VkBufferCreateInfo createInfo = {};
			createInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
			createInfo.size = size;
			createInfo.usage = flags;
			createInfo.sharingMode = sharingMode;

			VkResult res = vkCreateBuffer(gc.getDevice(), &createInfo, null, &this.buffer);

			if(res != VK_SUCCESS)
			{
				Console.get().err("Error creating buffer");
				return res;
			}

			VkMemoryRequirements memRequirements;
			vkGetBufferMemoryRequirements(gc.getDevice(), this.buffer, &memRequirements);
			res = this.pool.allocateMemory(gc, memRequirements, memFlags);

			this.maxSize = memRequirements.size;

			if(res != VK_SUCCESS)
			{
				Console.get().err("Failed to allocate memory for a buffer");
				return res;
			}

			vkBindBufferMemory(gc.getDevice(), this.buffer, this.pool.getMemory(), 0);

			return res;
		}

		BufferBlock alloc(VkDeviceSize size)
		{
			foreach(idx; 0 .. this.freeList.length)
			{
				if(this.freeList[idx].size >= size)
				{
					VkDeviceSize newOffset = alignOffset(this.freeList[idx].offset);
					BufferBlock block = BufferBlock(newOffset, size, 0, true);
					BufferBlock newFreeBlock = BufferBlock(newOffset + size, this.freeList[idx].size - size, 0, true);
					this.blocks.insertFront(block);
					this.freeList = this.freeList.remove(idx);
					this.freeList ~= newFreeBlock;
					return this.blocks.front();
				}
			}

			//Align everything to 16 bytes
			VkDeviceSize newOffset = this.alignOffset(this.curOffset);

			if(size + newOffset > this.maxSize)
			{
				Console.get().warn("No more free memory available. Trying another buffer");
				return BufferBlock(-1, -1, 0, false);
			}

			this.curOffset = newOffset;
			this.blocks.insertFront(BufferBlock(this.curOffset, size, 0, true));
			this.curOffset += size;
			return this.blocks.front();
		}

		void freeBlock(BufferBlock block)
		{
			if(block.valid)
			{
				this.freeList ~= block;
				this.blocks.linearRemoveElement(block);

				for(int i = 0; i < this.freeList.length - 1; i++)
				{
					VkDeviceSize newOffset = this.alignOffset(this.freeList[i].size + this.freeList[i].offset);
						
					if(newOffset == this.freeList[i + 1].offset)
					{
						this.freeList[i].size = newOffset + this.freeList[i + 1].size;
						this.freeList = this.freeList.remove(i + 1);
					}
				}
			}
		}

		VkResult copyDataInto(GraphicsContext gc, const void* inputData, BufferBlock block, int offset = 0, int size = 0)
		{
			void* newData;
			VkResult res = this.mapMemory(gc, block, newData, offset);

			if(size > 0)
				(cast(ubyte*)newData)[0 .. size] = (cast(const(ubyte)*)inputData)[0 .. size];

			else
				(cast(ubyte*)newData)[0 .. block.size] = (cast(const(ubyte)*)inputData)[0 .. block.size];
			//memcpy(newData, inputData, block.size);
			this.unmapMemory(gc);

			return res;
		}

		VkResult mapMemory(GraphicsContext gc, BufferBlock block, out void* data, int offset = 0)
		{
			VkResult res = vkMapMemory(gc.getDevice(),
									   this.pool.getMemory(),
									   block.offset + offset,
									   block.size,
									   0,
									   &data);
			if(res != VK_SUCCESS)
				Console.get().err("Failed to map buffer for writing");
			return res;

		}

		void unmapMemory(GraphicsContext gc)
		{
			vkUnmapMemory(gc.getDevice(), this.pool.getMemory());
		}

		void invalidate()
		{
			this.curOffset = VkDeviceSize(0);
			this.blocks.clear();
		}

		void free(GraphicsContext gc)
		{
			if(this.buffer != VK_NULL_HANDLE)
			{
				vkDestroyBuffer(gc.getDevice(), this.buffer, null);
				this.buffer = VK_NULL_HANDLE;
			}

			this.pool.free(gc);
		}

		@nogc VkBuffer getBuffer()
		{
			return this.buffer;
		}

	private:
		VkBuffer buffer = VK_NULL_HANDLE;
		VkDeviceSize curOffset = VkDeviceSize(0);
		VkDeviceSize maxSize = VkDeviceSize(0);

		SList!BufferBlock blocks;
		BufferBlock[] freeList;

		MemoryPool pool;

		immutable(int) ALIGNMENT = 64; // Alignment 64 because nvidia yells at me (TODO: change this to use minUniformBufferOffsetAlignment)

		VkDeviceSize alignOffset(VkDeviceSize offset)
		{
			VkDeviceSize mod = offset % ALIGNMENT;
			VkDeviceSize newOffset = offset;

			if(mod > 0)
				newOffset += ALIGNMENT - mod;

			return newOffset;
		}
}