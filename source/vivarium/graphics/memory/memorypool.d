module vivarium.graphics.memory.memorypool;

import std.stdio,
	   std.algorithm;

import erupted;

import vivarium.core.console,
	   vivarium.graphics.graphicscontext;

class MemoryPool
{
	public:
		VkResult allocateMemory(GraphicsContext gc, VkMemoryRequirements memRequirements, VkMemoryPropertyFlags memFlags)
        {
            VkMemoryAllocateInfo memInfo = {};
			memInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
			memInfo.allocationSize = memRequirements.size;
			memInfo.memoryTypeIndex = findMemoryType(gc, memRequirements.memoryTypeBits, memFlags);

			return vkAllocateMemory(gc.getDevice(), &memInfo, null, &this.memory);
        }

		void free(GraphicsContext gc)
		{
			if(this.memory != VK_NULL_HANDLE)
			{
				vkFreeMemory(gc.getDevice(), this.memory, null);
				this.memory = VK_NULL_HANDLE;
			}

		}

		VkDeviceMemory getMemory()
		{
			return this.memory;
		}
		
	private:
		VkDeviceMemory memory = VK_NULL_HANDLE;

		uint findMemoryType(GraphicsContext gc, uint typeFilter, const ref VkMemoryPropertyFlags props)
		{
			VkPhysicalDeviceMemoryProperties memProps;
			vkGetPhysicalDeviceMemoryProperties(gc.getPhysicalDevice(), &memProps);

			foreach(i; 0 .. memProps.memoryTypeCount)
			{
				if(typeFilter & (1 << i) && (memProps.memoryTypes[i].propertyFlags & props) == props)
				{
					return i;
				}
			}

			Console.get().err("Could not find valid memory type");
			return 0;
		}
}