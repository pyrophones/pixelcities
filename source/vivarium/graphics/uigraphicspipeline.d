module vivarium.graphics.uigraphicspipeline;

import core.memory,
	   core.stdc.stdio,
	   core.stdc.string;

import std.stdio,
	   std.string;

import erupted,
	   dlib.math,
	   bindbc.nuklear,
	   bindbc.sdl;

import vivarium.core.console,
	   vivarium.core.engine,
	   vivarium.core.input,
	   vivarium.core.timer,
	   vivarium.graphics.graphicspipeline,
	   vivarium.graphics.graphicscontext,
	   vivarium.graphics.texture,
	   vivarium.graphics.imageview,
	   vivarium.graphics.memory;

class UIGraphicsPipeline : GraphicsPipeline
{
	public:
		this(GraphicsContext gc, int renderpass = 0)
		{
			super(gc, renderpass);

			VkViewport viewport;
			viewport.x = 0.0;
			viewport.y = 0.0;
			viewport.width = gc.getExtents().width;
			viewport.height = gc.getExtents().height;
			viewport.minDepth = 0.0f;
			viewport.maxDepth = 1.0f;

			VkRect2D scissor;
			scissor.offset = VkOffset2D(0, 0);
			scissor.extent = gc.getExtents();

			this.viewports ~= viewport;
			this.scissors ~= scissor;

			this.inputAssembly.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
			this.inputAssembly.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
			this.inputAssembly.primitiveRestartEnable = VK_FALSE;

			this.rasterizeInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
			this.rasterizeInfo.depthClampEnable = VK_FALSE;
			this.rasterizeInfo.rasterizerDiscardEnable = VK_FALSE;
			this.rasterizeInfo.polygonMode = VK_POLYGON_MODE_FILL;
			this.rasterizeInfo.lineWidth = 1.0f;
			this.rasterizeInfo.cullMode = VK_CULL_MODE_NONE;
			this.rasterizeInfo.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
			this.rasterizeInfo.depthBiasEnable = VK_FALSE;
			this.rasterizeInfo.depthBiasConstantFactor = 0.0f;
			this.rasterizeInfo.depthBiasClamp = 0.0f;
			this.rasterizeInfo.depthBiasSlopeFactor = 0.0f;

			this.multisampleInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
			this.multisampleInfo.sampleShadingEnable = VK_FALSE;
			this.multisampleInfo.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
			this.multisampleInfo.minSampleShading = 1.0f;
			this.multisampleInfo.pSampleMask = null;
			this.multisampleInfo.alphaToCoverageEnable = VK_FALSE;
			this.multisampleInfo.alphaToOneEnable = VK_FALSE;

			this.dsCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
			this.dsCreateInfo.depthTestEnable = VK_FALSE;
			this.dsCreateInfo.depthWriteEnable = VK_FALSE;
			this.dsCreateInfo.depthCompareOp = VK_COMPARE_OP_LESS;
			this.dsCreateInfo.depthBoundsTestEnable = VK_FALSE;
			this.dsCreateInfo.stencilTestEnable = VK_FALSE;

			VkPipelineColorBlendAttachmentState colorBlend;
			colorBlend.colorWriteMask = VK_COLOR_COMPONENT_R_BIT
										 | VK_COLOR_COMPONENT_G_BIT
										 | VK_COLOR_COMPONENT_B_BIT
										 | VK_COLOR_COMPONENT_A_BIT;
			colorBlend.blendEnable = VK_TRUE;
			colorBlend.srcColorBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA;
			colorBlend.dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
			colorBlend.colorBlendOp = VK_BLEND_OP_ADD;
			colorBlend.srcAlphaBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA;
			colorBlend.dstAlphaBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
			colorBlend.alphaBlendOp = VK_BLEND_OP_ADD;

			this.duplicateBlendStateForAll(gc, colorBlend);

			this.colorInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
			this.colorInfo.logicOpEnable = VK_FALSE;
			this.colorInfo.logicOp = VK_LOGIC_OP_COPY;
			this.colorInfo.attachmentCount = cast(uint)this.colorBlendstateAttachment.length;
			this.colorInfo.pAttachments = this.colorBlendstateAttachment.ptr;
			this.colorInfo.blendConstants[0] = 0.0f;
			this.colorInfo.blendConstants[1] = 0.0f;
			this.colorInfo.blendConstants[2] = 0.0f;
			this.colorInfo.blendConstants[3] = 0.0f;

			this.dynamicStates ~= VK_DYNAMIC_STATE_VIEWPORT;
			this.dynamicStates ~= VK_DYNAMIC_STATE_SCISSOR;

			this.dynamicInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
			this.dynamicInfo.dynamicStateCount = cast(uint)this.dynamicStates.length;
			this.dynamicInfo.pDynamicStates = this.dynamicStates.ptr;

			this.createInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
			this.createInfo.pInputAssemblyState = &inputAssembly;
			this.createInfo.pRasterizationState = &rasterizeInfo;
			this.createInfo.pMultisampleState = &multisampleInfo;
			this.createInfo.pDepthStencilState = &dsCreateInfo;
			this.createInfo.pColorBlendState = &colorInfo;
			this.createInfo.pDynamicState = &dynamicInfo;
			this.createInfo.subpass = 0;
			this.createInfo.basePipelineHandle = VK_NULL_HANDLE;
			this.createInfo.basePipelineIndex = -1;
			this.createInfo.layout = this.pipelineLayout;

			this.initalizeNuklear();
		}

		override void createCustomData(GraphicsContext gc, VkCommandBuffer current)
		{
			nk_font_atlas_init_default(&this.sdl.atlas);			
			nk_font_atlas_begin(&this.sdl.atlas);

			int w;
			int h;

			const void* image = nk_font_atlas_bake(&this.sdl.atlas, &w, &h, NK_FONT_ATLAS_RGBA32);

			this.sdl.fontTexture.create(gc, ivec3(w, h, 1), false, 1, VK_IMAGE_TYPE_2D, false, VK_FORMAT_R8G8B8A8_SRGB, false);
			this.sdl.fontTexture.loadTextures(gc, [image], current);

			this.uploadAtlas(gc);

			super.createCustomData(gc, current);
		}

		@nogc Texture getFontTexture()
		{
			return this.sdl.fontTexture;
		}

		override void createDescriptorWrites(GraphicsContext gc)
		{
			foreach(i; 0 .. gc.getMaxFramesInFlight())
			{
				VkDescriptorImageInfo fontInfo = {};
				fontInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
				fontInfo.imageView = this.sdl.fontTexture.getImageView(0).getImageView();
				fontInfo.sampler = this.sdl.fontTexture.getSampler();

				VkWriteDescriptorSet[1] writes = {};
				writes[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
				writes[0].dstSet = gc.getDescriptorSet(0, i);
				writes[0].dstBinding = 4;
				writes[0].dstArrayElement = 0;
				writes[0].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
				writes[0].descriptorCount = 1;
				writes[0].pImageInfo = &fontInfo;

				gc.updateDescriptorSets(writes);
			}
		}

	protected:
		override void predraw(GraphicsContext gc, ref VkCommandBuffer cmdBuffer)
		{
			this.handleEvents();

			nk_context* ctx = &this.sdl.ctx;
			if (nk_begin(ctx, "Test", nk_rect(500, 0, 230, 250),
				NK_WINDOW_BORDER | NK_WINDOW_MOVABLE | NK_WINDOW_SCALABLE |
				NK_WINDOW_MINIMIZABLE | NK_WINDOW_TITLE))
			{
				static int op = 1;
				static int property = 20;

				static nk_colorf bg = { r: 0.10f, g: 0.18f, b: 0.24f, a: 1.0f};

				nk_layout_row_static(ctx, 30, 80, 1);
				if (nk_button_label(ctx, "button"))
				    printf("button pressed!\n");
				nk_layout_row_dynamic(ctx, 30, 2);
				if (nk_option_label(ctx, "easy", op == 1))
					op = 1;
				if (nk_option_label(ctx, "hard", op == 2))
					op = 2;
				nk_layout_row_dynamic(ctx, 22, 1);
				nk_property_int(ctx, "Compression:", 0, &property, 100, 10, 1);
				nk_layout_row_dynamic(ctx, 20, 1);
				nk_label(ctx, "background:", NK_TEXT_LEFT);
				nk_layout_row_dynamic(ctx, 25, 1);
				if (nk_combo_begin_color(ctx, nk_rgb_cf(bg), nk_vec2(nk_widget_width(ctx),400)))
				{
					nk_layout_row_dynamic(ctx, 120, 1);
					bg = nk_color_picker(ctx, bg, NK_RGBA);
					nk_layout_row_dynamic(ctx, 25, 1);
					bg.r = nk_propertyf(ctx, "#R:", 0, bg.r, 1.0f, 0.01f, 0.005f);
					bg.g = nk_propertyf(ctx, "#G:", 0, bg.g, 1.0f, 0.01f, 0.005f);
					bg.b = nk_propertyf(ctx, "#B:", 0, bg.b, 1.0f, 0.01f, 0.005f);
					bg.a = nk_propertyf(ctx, "#A:", 0, bg.a, 1.0f, 0.01f, 0.005f);
					nk_combo_end(ctx);
				}
			}
			nk_end(ctx);

			Console.get().drawWidget(ctx);

			//if(nk_begin(ctx,
			//			"Vivarium Console",
			//			nk_rect(200, 200, 400, 300),
			//   			NK_WINDOW_BORDER | NK_WINDOW_MOVABLE | NK_WINDOW_SCALABLE | NK_WINDOW_MINIMIZABLE | NK_WINDOW_TITLE))
			//{
			//	nk_layout_row_dynamic(ctx, nk_window_get_height(ctx) - 90, 1);
			//	if(nk_group_begin(ctx, "ConsoleLog", NK_WINDOW_BORDER))
			//	{
			//		foreach(log; Console.get().getLog())
			//		{
			//			nk_layout_row_dynamic(ctx, 20, 1);
			//			//nk_text_colored(ctx, toStringz(log.text), cast(int)log.text.length, NK_TEXT_LEFT, log.color);
			//			nk_select_text(ctx, toStringz(log.text), cast(int)log.text.length, NK_TEXT_LEFT, false);
			//		}

			//		nk_group_end(ctx);
			//	}

			//	nk_layout_row_dynamic(ctx, 30, 1);
				//nk_flags event = nk_edit_string(ctx,
				//								NK_EDIT_FIELD | NK_EDIT_SIG_ENTER,
				//								buffer.ptr,
				//								&this.textLen,
				//								256,
				//								&nk_filter_default);
//
				//if(event & NK_EDIT_ACTIVATED)
				//{
				//	Input.get().startTextInput();
				//}
//
				//if(event & NK_EDIT_DEACTIVATED)
				//{
				//	Input.get().stopTextInput();						
				//}
//
				//if(event & NK_EDIT_ACTIVE)
				//{
				//	if(event & NK_EDIT_COMMITED)
				//	{
				//		if(this.textLen > 0)
				//		{
				//			string toSubmit = buffer[0..this.textLen].idup();
//
				//			if(toSubmit in this.commands && this.commands[toSubmit] == true)
				//			{
				//				this.commandEnter(toSubmit);
				//				this.runCommand(toSubmit);
				//			}
//
				//			else
				//				this.commandEnter("Command '" ~ toSubmit ~ "' not found", LogLevel.ERROR);
//
				//			this.buffer[0..$] = '\xFF';
				//			this.textLen = 0;
				//		}
				//	}
				//}
			//}
			//nk_end(ctx);

			if(nk_begin(ctx,
						"Stats",
						nk_rect(0, 0, 100, 100),
			   			NK_WINDOW_BORDER | NK_WINDOW_TITLE))
			{
				nk_layout_row_dynamic(ctx, 0, 1);
				int fps = cast(int)(1.0 / Timer.get().getDeltaTime());
				nk_color color = nk_color(255, 255, 255, 255);

				if(fps > 59)
					color = nk_color(66, 203, 71, 255);
				else if(fps > 29)
					color = nk_color(143, 203, 125, 255);
				else if(fps > 19)
					color = nk_color(229, 85, 2, 255);
				else
					color = nk_color(242, 10, 10, 255);

				string txt = "FPS - %d".format(fps);

				nk_text_colored(ctx, txt.toStringz(), cast(int)txt.length, NK_TEXT_LEFT, color);
			}
			nk_end(ctx);
			
			this.convert(gc, cmdBuffer);
		}

		override void draw(GraphicsContext gc, ref VkCommandBuffer cmdBuffer, int imgNum)
		{
			uint offset = 0;

			uvec2 win = Engine.get().getWindowSize();
			VkExtent2D ext = gc.getExtents();

			vec2 scale = vec2(cast(float)(ext.width) / cast(float)(win.x), cast(float)(ext.height) / cast(float)(win.y));

			//mat4 ortho = orthoMatrix(0.0f, cast(float)float(win.x), 0.0f, cast(float)float(win.y), 0.0f, 1.0f);
			mat4 ortho = mat4(
								[
								   2.0f / cast(float)float(win.x), 0.0f, 						 0.0f, -1.0f,
								   0.0f,			  			   2.0f / cast(float)(win.y),    0.0f, -1.0f,
								   0.0f,			  			   0.0f,		  				-1.0f,  0.0f,
								   0.0f,			  			   0.0f,				 		 0.0f,  1.0f
								]
							);

			vkCmdPushConstants(cmdBuffer,
							   this.pipelineLayout,
							   VK_SHADER_STAGE_VERTEX_BIT,
							   0,
							   mat4.sizeof,
							   &ortho);

			for(const(nk_draw_command)* cmd = nk__draw_begin(&this.sdl.ctx, &this.sdl.cmds);
				cmd != null;
				cmd = nk__draw_next(cmd, &this.sdl.cmds, &this.sdl.ctx))
			{
				if(!cmd.elem_count)
					continue;

				VkRect2D[1] rects = [
										VkRect2D(VkOffset2D(NK_MAX(cast(int)(cmd.clip_rect.x * scale.x), 0),
															NK_MAX(cast(int)(cmd.clip_rect.y * scale.y), 0)),
												 VkExtent2D(cast(uint)(cmd.clip_rect.w * scale.x),
															cast(uint)(cmd.clip_rect.h * scale.y)))
									];

				vkCmdSetScissor(cmdBuffer, 0, 1, rects.ptr);

				VkBuffer[1] vertexBuffers;
				vertexBuffers[0] = MemoryManager.get().getDeviceLocalBuffer(this.vertexBlock.poolId).getBuffer();
				VkDeviceSize[1] offsets = [this.vertexBlock.offset];
				vkCmdBindVertexBuffers(cmdBuffer, 0, 1, vertexBuffers.ptr, offsets.ptr);	

				vkCmdBindIndexBuffer(cmdBuffer,
									 MemoryManager.get().getDeviceLocalBuffer(this.indexBlock.poolId).getBuffer(),
									 this.indexBlock.offset,
									 VK_INDEX_TYPE_UINT32);

				vkCmdDrawIndexed(cmdBuffer, cmd.elem_count, 1, offset, 0, 0);

				offset += cmd.elem_count;
			}

			nk_clear(&this.sdl.ctx);
		}

		override void freeExtra(GraphicsContext gc)
		{
			nk_font_atlas_clear(&this.sdl.atlas);
			this.sdl.fontTexture.free(gc);
			MemoryManager.get().freeDeviceLocalBuffer(gc, vertexBlock);
			MemoryManager.get().freeDeviceLocalBuffer(gc, indexBlock);
			nk_free(&this.sdl.ctx);
			nk_buffer_free(&this.sdl.cmds);
		}

	private:
		immutable int MAX_VERTEX_MEMORY = 512 * 1024;
		immutable int MAX_INDEX_MEMORY = 128 * 1024;

		NkSdl sdl;
		BufferBlock vertexBlock;
		BufferBlock indexBlock;

		void initalizeNuklear()
		{
			this.sdl.fontTexture = new Texture();
			
			immutable NuklearSupport nuksup = loadNuklear();
			if(nuksup != NuklearSupport.Nuklear4)
			{
				printf("Error: Nuklear library is not found.\n");
				return;
			}

			this.vertexBlock = MemoryManager.get().allocDeviceLocalBuffer(this.MAX_VERTEX_MEMORY);
			this.indexBlock = MemoryManager.get().allocDeviceLocalBuffer(this.MAX_INDEX_MEMORY);
			
			this.sdl.win = Engine.get().getWindowPtr();
			nk_init_default(&this.sdl.ctx, null);
			this.sdl.ctx.clip.copy = &nk_sdl_clipboard_copy;
			this.sdl.ctx.clip.paste = &nk_sdl_clipboard_paste;
			this.sdl.ctx.clip.userdata = nk_handle_ptr(null);

			nk_buffer_init_default(&this.sdl.cmds);
		}

		void convert(GraphicsContext gc, ref VkCommandBuffer cmdBuffer)
		{
			nk_convert_config config = {};
			
			static immutable nk_draw_vertex_layout_element[4] vertexLayout = [
						{ NK_VERTEX_POSITION, NK_FORMAT_FLOAT, UIVertex.position.offsetof },
						{ NK_VERTEX_TEXCOORD, NK_FORMAT_FLOAT, UIVertex.uv.offsetof },
						{ NK_VERTEX_COLOR, NK_FORMAT_R8G8B8A8, UIVertex.col.offsetof },
						NK_VERTEX_LAYOUT_END
			];
			
			config.vertex_layout = vertexLayout.ptr;
			config.vertex_size = UIVertex.sizeof;
			config.vertex_alignment = UIVertex.alignof;
			config.tex_null = sdl.texNull;
			config.circle_segment_count = 22;
			config.curve_segment_count = 22;
			config.arc_segment_count = 22;
			config.global_alpha = 1.0f;
			config.shape_AA = NK_ANTI_ALIASING_ON;
			config.line_AA = NK_ANTI_ALIASING_ON;

			nk_buffer vBuf;
			nk_buffer iBuf;

			nk_buffer_init_default(&vBuf);
			nk_buffer_init_default(&iBuf);

			nk_convert(&this.sdl.ctx, &this.sdl.cmds, &vBuf, &iBuf, &config);

			nk_memory_status vbInfo;
			nk_buffer_info(&vbInfo, &vBuf);

			nk_memory_status ibInfo;
			nk_buffer_info(&ibInfo, &iBuf);

			BufferBlock vTransferBlock = MemoryManager.get().allocTransfer(vbInfo.size);
			VkResult res = MemoryManager.get().copyIntoTransferBuffer(gc, vbInfo.memory, vTransferBlock);
			BufferBlock iTransferBlock = MemoryManager.get().allocTransfer(ibInfo.size);
			res = MemoryManager.get().copyIntoTransferBuffer(gc, ibInfo.memory, iTransferBlock);

			MemoryManager.get().copyToDeviceLocalBuffer(cmdBuffer, vTransferBlock, this.vertexBlock);
			MemoryManager.get().copyToDeviceLocalBuffer(cmdBuffer, iTransferBlock, this.indexBlock);

			nk_buffer_free(&vBuf);
			nk_buffer_free(&iBuf);
		}

		void handleEvents()
		{
			nk_context* ctx = &this.sdl.ctx;
			nk_input_begin(ctx);
			if (ctx.input.mouse.grab)
			{
				SDL_SetRelativeMouseMode(SDL_TRUE);
				ctx.input.mouse.grab = 0;
			}
			
			else if (ctx.input.mouse.ungrab)
			{
				int x = cast(int)ctx.input.mouse.prev.x, y = cast(int)ctx.input.mouse.prev.y;
				SDL_SetRelativeMouseMode(SDL_FALSE);
				SDL_WarpMouseInWindow(this.sdl.win, x, y);
				ctx.input.mouse.ungrab = 0;
			}

			// Translating NK inputs
			if(Input.get().didKeyRecieveEvent(SDLK_LSHIFT) || Input.get().didKeyRecieveEvent(SDLK_RSHIFT))
				nk_input_key(ctx, NK_KEY_SHIFT, Input.get().isKeyPressed(SDLK_LSHIFT) ||
												Input.get().isKeyPressed(SDLK_RSHIFT));
			if(Input.get().didKeyRecieveEvent(SDLK_DELETE))
				nk_input_key(ctx, NK_KEY_DEL, Input.get().isKeyPressed(SDLK_DELETE));
			if(Input.get().didKeyRecieveEvent(SDLK_RETURN))
				nk_input_key(ctx, NK_KEY_ENTER, Input.get().isKeyPressed(SDLK_RETURN));
			if(Input.get().didKeyRecieveEvent(SDLK_TAB))
				nk_input_key(ctx, NK_KEY_TAB, Input.get().isKeyPressed(SDLK_TAB));
			if(Input.get().didKeyRecieveEvent(SDLK_BACKSPACE))
				nk_input_key(ctx, NK_KEY_BACKSPACE, Input.get().isKeyPressed(SDLK_BACKSPACE));
			if(Input.get().didKeyRecieveEvent(SDLK_HOME))
			{
				nk_input_key(ctx, NK_KEY_TEXT_START, Input.get().isKeyPressed(SDLK_HOME));
				nk_input_key(ctx, NK_KEY_SCROLL_START, Input.get().isKeyPressed(SDLK_HOME));
			}
			if(Input.get().didKeyRecieveEvent(SDLK_END))
			{
				nk_input_key(ctx, NK_KEY_TEXT_END, Input.get().isKeyPressed(SDLK_END));
				nk_input_key(ctx, NK_KEY_SCROLL_END, Input.get().isKeyPressed(SDLK_END));
			}
			if(Input.get().didKeyRecieveEvent(SDLK_PAGEDOWN))
				nk_input_key(ctx, NK_KEY_SCROLL_DOWN, Input.get().isKeyPressed(SDLK_PAGEDOWN));
			if(Input.get().didKeyRecieveEvent(SDLK_PAGEUP))
				nk_input_key(ctx, NK_KEY_SCROLL_UP, Input.get().isKeyPressed(SDLK_PAGEUP));
			if(Input.get().didKeyRecieveEvent(SDLK_z) && Input.get().isKeyPressed(SDLK_LCTRL))
				nk_input_key(ctx, NK_KEY_TEXT_UNDO, Input.get().isKeyPressed(SDLK_z) &&
													Input.get().isKeyPressed(SDLK_LCTRL));
			if(Input.get().didKeyRecieveEvent(SDLK_z)
			&& Input.get().isKeyPressed(SDLK_LCTRL)
			&& Input.get().isKeyPressed(SDLK_LSHIFT))
				nk_input_key(ctx, NK_KEY_TEXT_REDO, Input.get().isKeyPressed(SDLK_z) &&
													Input.get().isKeyPressed(SDLK_LCTRL) &&
													Input.get().isKeyPressed(SDLK_LSHIFT));
			if(Input.get().didKeyRecieveEvent(SDLK_c) && Input.get().isKeyPressed(SDLK_LCTRL))
				nk_input_key(ctx, NK_KEY_COPY, Input.get().isKeyPressed(SDLK_c) &&
											   Input.get().isKeyPressed(SDLK_LCTRL));
			if(Input.get().didKeyRecieveEvent(SDLK_v) && Input.get().isKeyPressed(SDLK_LCTRL))
				nk_input_key(ctx, NK_KEY_PASTE, Input.get().isKeyPressed(SDLK_v) &&
												Input.get().isKeyPressed(SDLK_LCTRL));
			if(Input.get().didKeyRecieveEvent(SDLK_x) && Input.get().isKeyPressed(SDLK_LCTRL))
				nk_input_key(ctx, NK_KEY_CUT, Input.get().isKeyPressed(SDLK_x) &&
											  Input.get().isKeyPressed(SDLK_LCTRL));
			if(Input.get().didKeyRecieveEvent(SDLK_b) && Input.get().isKeyPressed(SDLK_LCTRL))
				nk_input_key(ctx, NK_KEY_TEXT_LINE_START, Input.get().isKeyPressed(SDLK_b) &&
														  Input.get().isKeyPressed(SDLK_LCTRL));
			if(Input.get().didKeyRecieveEvent(SDLK_e) && Input.get().isKeyPressed(SDLK_LCTRL))
				nk_input_key(ctx, NK_KEY_TEXT_LINE_END, Input.get().isKeyPressed(SDLK_e) &&
														Input.get().isKeyPressed(SDLK_LCTRL));
			if(Input.get().didKeyRecieveEvent(SDLK_UP))
				nk_input_key(ctx, NK_KEY_UP, Input.get().isKeyPressed(SDLK_UP));
			if(Input.get().didKeyRecieveEvent(SDLK_DOWN))
				nk_input_key(ctx, NK_KEY_DOWN, Input.get().isKeyPressed(SDLK_DOWN));

			if(Input.get().isNewChar())
			{
				nk_glyph glyph;
				memcpy(glyph.ptr, Input.get().getCurrentTextInput().ptr, NK_UTF_SIZE);
				nk_input_glyph(ctx, glyph.ptr);
			}

			if(Input.get().didKeyRecieveEvent(SDLK_LEFT))
			{
				bool pressed = Input.get().isKeyPressed(SDLK_LEFT);
				if(Input.get().isKeyPressed(SDLK_LCTRL))
					nk_input_key(ctx, NK_KEY_TEXT_WORD_LEFT, pressed);
				else
					nk_input_key(ctx, NK_KEY_LEFT, pressed);
			}

			if(Input.get().didKeyRecieveEvent(SDLK_RIGHT))
			{
				bool pressed = Input.get().isKeyPressed(SDLK_RIGHT);
				if(Input.get().isKeyPressed(SDLK_LCTRL))
					nk_input_key(ctx, NK_KEY_TEXT_WORD_RIGHT, pressed);
				else
					nk_input_key(ctx, NK_KEY_RIGHT, pressed);
			}

			ivec2 mousePosition = Input.get().getMousePosition();

			if(Input.get().didMouseRecieveEvent(SDL_BUTTON_LEFT))
			{
				nk_input_button(ctx,
								NK_BUTTON_DOUBLE,
								mousePosition.x,
								mousePosition.y,
								Input.get().isButtonDoublePressed(SDL_BUTTON_LEFT));
				nk_input_button(ctx,
								NK_BUTTON_LEFT,
								mousePosition.x,
								mousePosition.y,
								Input.get().isButtonPressed(SDL_BUTTON_LEFT));
			}
			//if(Input.get().isButtonPressed(SDL_BUTTON_LEFT))
			//	writeln("Pressed ", Input.get().isButtonPressed(SDL_BUTTON_LEFT), " ", mousePosition);
			//else
			//	writeln("Pressed ", Input.get().isButtonPressed(SDL_BUTTON_LEFT), " ", mousePosition);

			if(Input.get().didMouseRecieveEvent(SDL_BUTTON_MIDDLE))
			{
				nk_input_button(ctx,
								NK_BUTTON_MIDDLE,
								mousePosition.x,
								mousePosition.y,
								Input.get().isButtonPressed(SDL_BUTTON_MIDDLE));
			}

			if(Input.get().didMouseRecieveEvent(SDL_BUTTON_RIGHT))
			{
				nk_input_button(ctx,
								NK_BUTTON_RIGHT,
								mousePosition.x,
								mousePosition.y,
								Input.get().isButtonPressed(SDL_BUTTON_RIGHT));
			}

			mousePosition = Input.get().getMousePosition();
			if(Input.get().mouseMoved())
			{
				if (ctx.input.mouse.grabbed)
				{
					ivec2 relPosition = Input.get().getRelativeMousePosition();

					int x = cast(int)ctx.input.mouse.prev.x, y = cast(int)ctx.input.mouse.prev.y;
					nk_input_motion(ctx, x + relPosition.x, y + relPosition.y);
				}

				else
					nk_input_motion(ctx, mousePosition.x, mousePosition.y);
			}

			nk_input_end(ctx);

			//switch(evt.type)
			//{
		    //    case SDL_MOUSEMOTION:
			//		if (ctx.input.mouse.grabbed)
			//		{
			//			int x = cast(int)ctx.input.mouse.prev.x, y = cast(int)ctx.input.mouse.prev.y;
			//			nk_input_motion(ctx, x + evt.motion.xrel, y + evt.motion.yrel);
			//		}
//
			//		else
			//			nk_input_motion(ctx, evt.motion.x, evt.motion.y);
			//		return 1;
			//	case SDL_MOUSEWHEEL:
			//		nk_input_scroll(ctx, nk_vec2(cast(float)evt.wheel.x, cast(float)evt.wheel.y));
			//		return 1;
			//	default:
			//		break;
			//}
		}

		void uploadAtlas(GraphicsContext gc)
		{
			this.sdl.fontTexture.createSampler(gc,
											   VK_FILTER_LINEAR,
											   VK_FILTER_LINEAR,
											   VK_SAMPLER_MIPMAP_MODE_LINEAR,
											   VK_SAMPLER_ADDRESS_MODE_REPEAT,
											   VK_SAMPLER_ADDRESS_MODE_REPEAT,
											   VK_SAMPLER_ADDRESS_MODE_REPEAT,
											   VK_BORDER_COLOR_FLOAT_OPAQUE_BLACK,
											   VK_FALSE);
			this.sdl.fontTexture.createImageView(gc, VK_IMAGE_VIEW_TYPE_2D);

			nk_font_atlas_end(&this.sdl.atlas,
							  nk_handle_ptr(this.sdl.fontTexture.getImageView(0).getImageView()),
							  &this.sdl.texNull);
			if (this.sdl.atlas.default_font)
				nk_style_set_font(&this.sdl.ctx, &this.sdl.atlas.default_font.handle);
		}
}

private:
struct UIVertex
{
	vec2 position;
	vec2 uv;
	Vector!(byte, 4) col;
}

struct NkSdl
{
	SDL_Window* win = null;
	nk_context ctx;
	nk_font_atlas atlas;
	nk_buffer cmds;
	nk_draw_null_texture texNull;
	Texture fontTexture;
}

@nogc:
extern(C)
{
	void nk_sdl_clipboard_paste(nk_handle usr, nk_text_edit *edit) nothrow
	{
	    const(char*) text = SDL_GetClipboardText();
	    if (text)
			nk_textedit_paste(edit, text, nk_strlen(text));
	    usr.ptr = null;
	}

	void nk_sdl_clipboard_copy(nk_handle usr, const(char)*text, int len) nothrow
	{
		usr.ptr = null;

		if (!len)
			return;

		char* str = cast(char*)pureMalloc(cast(size_t)len + 1);

		if (!str)
			return;

		str[0 .. len] = text[0 .. len];
		//memcpy(str, text, cast(size_t)len);
		str[len] = '\0';
		SDL_SetClipboardText(str);
		pureFree(str);
	}
}