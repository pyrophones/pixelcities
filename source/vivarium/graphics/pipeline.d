module vivarium.graphics.pipeline;

// External
import erupted;

import vivarium.core,
	   vivarium.graphics.graphicscontext,
	   vivarium.graphics.shader;

abstract class Pipeline
{
	public:
		this()
		{

		}

		this(GraphicsContext gc)
		{

		}

		final VkResult createPipelineLayout(GraphicsContext gc)
		{
			this.freePipelineLayout(gc);

			VkPushConstantRange[] pushConstantRanges;
			
			foreach(shaderName; this.shaderNames)
			{
				//foreach(layoutName; gc.getShaderInfo(shaderName).descriptorInfo)
				//{
				//	layouts ~= gc.getDescriptorSetLayouts();
				//	//import std.stdio;
				//	//writeln(layoutName.name, " ", layouts[$ - 1] != null, " ");
				//}

				foreach(rangeName; gc.getShaderInfo(shaderName).pushConstantNames)
				{
					pushConstantRanges ~= gc.getPushConstantRanges(rangeName);
				}
			}

			VkPipelineLayoutCreateInfo pipelineInfo = {};
			pipelineInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
			pipelineInfo.setLayoutCount = cast(uint)gc.getDescriptorSetLayouts().length;
			pipelineInfo.pSetLayouts = gc.getDescriptorSetLayouts().ptr;
			pipelineInfo.pushConstantRangeCount = cast(uint)pushConstantRanges.length;
			pipelineInfo.pPushConstantRanges = pushConstantRanges.ptr;

			VkResult res = vkCreatePipelineLayout(gc.getDevice(), &pipelineInfo, null, &this.pipelineLayout);

			if(res != VK_SUCCESS)
	    		Console.get().err("Failed to create pipeline layout");

	    	return res;
		}

		abstract VkResult create(GraphicsContext gc, uint pipelineCount);

		final void free(GraphicsContext gc)
		{
			this.freePipelines(gc);
			this.freePipelineLayout(gc);
			this.freeExtra(gc);
		}

		VkResult recreate(GraphicsContext gc, uint pipelineCount)
		{
			this.createDescriptorWrites(gc);
			return VK_SUCCESS;
		}

		//final void pushConstants(ref VkCommandBuffer cmdBuffer, VkShaderStageFlags flags, uint offset, uint size, const void* values)
		//{
		//	vkCmdPushConstants(cmdBuffer, this.pipelineLayout, flags, offset, size, values);
		//}

		void createDescriptorWrites(GraphicsContext gc)
		{

		}

		abstract void bindDescriptorSets(GraphicsContext gc, ref VkCommandBuffer cmdBuffer, int index);
		
		void drawPass(GraphicsContext gc, ref VkCommandBuffer cmdBuffer, int curFrame, int imgIdx)
		{
			this.predraw(gc, cmdBuffer);
			this.bindDescriptorSets(gc, cmdBuffer, curFrame);
		}

		final void addShader(string shaderName)
		{
			this.shaderNames ~= shaderName;
		}

		void reset()
		{

		}

		final VkPipeline getPipeline(uint pipelineNum)
		{
			return this.pipelines[pipelineNum];
		}

		void createCustomData(GraphicsContext gc, VkCommandBuffer current)
		{
			this.createDescriptorWrites(gc);
		}

	protected:
		string[] shaderNames;

		// Other creation info
		VkPipelineLayout pipelineLayout;
		VkPipeline[] pipelines;

		void predraw(GraphicsContext gc, ref VkCommandBuffer cmdBuffer)
		{

		}

		abstract void draw(GraphicsContext gc, ref VkCommandBuffer cmdBuffer, int pipelineNum);

		final void freePipelines(GraphicsContext gc)
		{
			foreach(pipeline; this.pipelines)
			{
				vkDestroyPipeline(gc.getDevice(), pipeline, null);
			}

			this.pipelines.length = 0;
		}

		final void freePipelineLayout(GraphicsContext gc)
		{
			if(this.pipelineLayout != VK_NULL_HANDLE)
				vkDestroyPipelineLayout(gc.getDevice(), this.pipelineLayout, null);
		}

		void freeExtra(GraphicsContext gc)
		{

		}
}