module vivarium.graphics.commandbuffer;

import std.stdio;

import erupted;

import vivarium.core.console,
	   vivarium.graphics.graphicscontext,
	   vivarium.graphics.commandpool;

class CommandBuffer
{
	public:
		VkResult create(GraphicsContext gc, CommandPool* cmdPool, VkCommandBufferLevel level, uint amount)
		{
			this.free(gc);
			this.cmdPool = cmdPool;

			this.cmdBuffers.length = amount;
			this.cmdBuffers[0 .. $] = VK_NULL_HANDLE;

			VkCommandBufferAllocateInfo allocInfo = {};
			allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
			allocInfo.commandPool = cmdPool.getCommandPool();
			allocInfo.level = level;
			allocInfo.commandBufferCount = amount;

			VkResult res = vkAllocateCommandBuffers(gc.getDevice(),
												    &allocInfo,
													this.cmdBuffers.ptr);
			if(res != VK_SUCCESS)
			{
				this.cmdBuffers[0 .. $] = VK_NULL_HANDLE;
				Console.get().err("Failed to allocate command buffers");
			}

			return res;
		}

		void free(GraphicsContext gc)
		{
			if(cmdBuffers.length > 0)
				vkFreeCommandBuffers(gc.getDevice(),
									 this.cmdPool.getCommandPool(),
									 cast(uint)this.cmdBuffers.length,
									 this.cmdBuffers.ptr);
			cmdPool = null;
			current = null;
		}

		VkResult reset()
		{
			this.curBuffer++;
			this.curBuffer = this.curBuffer % cast(int)this.cmdBuffers.length;
			VkResult res = vkResetCommandBuffer(this.cmdBuffers[this.curBuffer], 0);
			current = null;
			if(res != VK_SUCCESS)
				Console.get().err("Failed to reset a command buffer");
			
			return res;
		}

		VkResult begin(VkCommandBufferUsageFlags flags, const VkCommandBufferInheritanceInfo* inheritanceInfo)
		{
			VkCommandBufferBeginInfo beginInfo = {};
			beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
			beginInfo.flags = flags;
			beginInfo.pInheritanceInfo = inheritanceInfo;

			VkResult res = vkBeginCommandBuffer(this.cmdBuffers[this.curBuffer], &beginInfo);
			if(res != VK_SUCCESS)
				Console.get().err("Failed to begin a command buffer");
			else
				current = &this.cmdBuffers[this.curBuffer];
			
			return res;
		}

		VkResult end()
		{
			VkResult res = vkEndCommandBuffer(this.cmdBuffers[this.curBuffer]);
			if(res != VK_SUCCESS)
				Console.get().err("Couldn't properly end command buffer");
			else
				current = null;
			
			return res;
		}

		VkCommandBuffer[] getCommandBuffers()
		{
			return this.cmdBuffers;
		}

		VkCommandBuffer getCommandBufferAt(uint loc)
		{
			return this.cmdBuffers[loc];
		}

		ref VkCommandBuffer getCurrent()
		{
			return *current;
		}

	private:
		VkCommandBuffer[] cmdBuffers;
		CommandPool* cmdPool = null;
		VkCommandBuffer* current = null;
		int curBuffer = 0;
}