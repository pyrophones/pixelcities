module vivarium.graphics.texture;

import std.stdio,
	   std.file,
	   std.path,
	   std.math,
	   std.algorithm;

import erupted,
	   dlib.math,
	   dlib.image;

import vivarium.core.console,
	   vivarium.graphics.graphicscontext,
	   vivarium.graphics.imageview,
	   vivarium.graphics.memory,
	   vivarium.graphics.vkhelpers;

class Texture
{
    public:
		VkResult create(GraphicsContext gc,
						ivec3 size,
						bool genMips,
						int layers,
						VkImageType type,
						bool is3d,
						VkFormat format,
						bool storageBit = true)
		{
			this.is3d = is3d;
			this.size = size;
			this.mipLevels = genMips ? cast(uint)floor(log2(cast(float)max(this.size.x, this.size.y, this.size.z))) + 1 : 1;
			this.layers = layers;
			this.isDepth = vkHelpers_isDepthFormat(format);

			uint bits = VK_IMAGE_USAGE_TRANSFER_SRC_BIT
						 | VK_BUFFER_USAGE_TRANSFER_DST_BIT
						 | VK_IMAGE_USAGE_SAMPLED_BIT;
			bits |= this.isDepth ? VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT : VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;

			if(storageBit)
				bits |= VK_IMAGE_USAGE_STORAGE_BIT;

			VkResult res = MemoryManager.get().createImage(gc,
														   cast(uvec3)this.size,
			                   							   this.mipLevels,
			                   							   layers,
			                   							   type,
			                   							   format,
			                   							   VK_SAMPLE_COUNT_1_BIT,
			                   							   VK_IMAGE_TILING_OPTIMAL,
														   bits,
							   							   VK_SHARING_MODE_EXCLUSIVE,
							   							   VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
														   this.image);

			//switch(type)
			//{
			//	//TODO:Cubemap support
			//	case VK_IMAGE_TYPE_1D:
			//		if(this.layers == 1)
			//			ivType = VK_IMAGE_VIEW_TYPE_1D;
			//		else
			//			ivType = VK_IMAGE_VIEW_TYPE_1D_ARRAY;
			//		break;
			//	case VK_IMAGE_TYPE_2D:
			//		if(this.layers == 1)
			//			ivType = VK_IMAGE_VIEW_TYPE_2D;
			//		else
			//			ivType = VK_IMAGE_VIEW_TYPE_2D_ARRAY;
			//		break;
			//	case VK_IMAGE_TYPE_3D:
			//		ivType = VK_IMAGE_VIEW_TYPE_3D;
			//		break;
			//	default:
			//		break;
			//}

			for(int i = 0; i < layers; i++)
			{
				this.subImages ~= this.image.alloc(cast(uvec3)this.size, i);
			}

			return res;
		}

		void free(GraphicsContext gc)
		{
			vkDestroySampler(gc.getDevice(), this.sampler, null);

			foreach(imgView; this.imgViews)
				imgView.free(gc);
			this.imgViews.length = 0;

			this.image.invalidate();
			this.subImages.length = 0;
			
			MemoryManager.get().freeImage(gc, this.image.getIndex());
		}

		ulong createImageView(GraphicsContext gc,
							  VkImageViewType type = VK_IMAGE_VIEW_TYPE_2D_ARRAY,
							  uint baseArrayLayer = 0,
							  uint layerCount = 0)
		{
			// TODO: parameterize some of these, allow multiple views into same image
			VkImageViewCreateInfo createInfo = {};
			createInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
			createInfo.image = this.image.getImage();
			createInfo.viewType = type;
			createInfo.format = this.image.getFormat();
			createInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
			createInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
			createInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
			createInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
			createInfo.subresourceRange.aspectMask = this.isDepth ? VK_IMAGE_ASPECT_DEPTH_BIT : VK_IMAGE_ASPECT_COLOR_BIT;
			createInfo.subresourceRange.baseMipLevel = 0;
			createInfo.subresourceRange.levelCount = this.mipLevels;
			createInfo.subresourceRange.baseArrayLayer = baseArrayLayer;
			createInfo.subresourceRange.layerCount = layerCount == 0 ? cast(uint)this.subImages.length : layerCount;

			this.imgViews ~= new ImageView();
			this.imgViews[$ - 1].create(gc, createInfo, 1);
			return this.imgViews.length - 1;
		}

		VkResult createSampler(GraphicsContext gc,
							   VkFilter minFilter,
						   	   VkFilter magFilter,
							   VkSamplerMipmapMode mipFilter,
						   	   VkSamplerAddressMode uMode,
						   	   VkSamplerAddressMode vMode,
						   	   VkSamplerAddressMode wMode,
						   	   VkBorderColor borderColor,
						   	   VkBool32 unnormalizedCoords)
		{
			// TODO: Make sampler a different class
			VkSamplerCreateInfo createInfo = {};
			createInfo.minFilter = minFilter;
			createInfo.magFilter = magFilter;
			createInfo.mipmapMode = mipFilter;
			createInfo.addressModeU = uMode;
			createInfo.addressModeV = vMode;
			createInfo.addressModeW = wMode;
			createInfo.anisotropyEnable = false;
			createInfo.maxAnisotropy = gc.getDeviceProperties().limits.maxSamplerAnisotropy;
			createInfo.borderColor = borderColor;
			createInfo.unnormalizedCoordinates = unnormalizedCoords;
			createInfo.compareEnable = VK_FALSE;
			createInfo.compareOp = VK_COMPARE_OP_ALWAYS;
			createInfo.mipLodBias = 0.0f;
			createInfo.minLod = 0.0f;
			createInfo.maxLod = cast(float)mipLevels;

			VkResult res = vkCreateSampler(gc.getDevice(), &createInfo, null, &this.sampler);

			if(res != VK_SUCCESS)
				Console.get().warn("Failed to create valid sampler");

			return res;
		}

		void loadTextures(GraphicsContext gc, string[] imgFiles, ref VkCommandBuffer cmdBuffer)
		{
			VkResult res;
			BufferBlock[] bufferBlocks;

			foreach(i, imgName; imgFiles)
			{
				if(i > subImages.length - 1)
				{
					Console.get().warn("Too many images supplied for this texture, skipping the rest");
					break;
				}

				SuperImage tex = loadImage(thisExePath().dirName ~ "/" ~ imgName);
				uint size = cast(uint)ubyte.sizeof * this.size.x * this.size.y * this.size.z * tex.channels();
				bufferBlocks ~= MemoryManager.get().allocTransfer(size);
				res = MemoryManager.get().copyIntoTransferBuffer(gc, tex.data().ptr, bufferBlocks[$ - 1]);

				if(res != VK_SUCCESS)
					return;
			}

			MemoryManager.get().transitionImageLayout(cmdBuffer,
													  this,
													  VK_IMAGE_LAYOUT_UNDEFINED,
													  VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
													  0,
													  VK_ACCESS_TRANSFER_WRITE_BIT,
													  VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT,
													  VK_PIPELINE_STAGE_TRANSFER_BIT);
			
			foreach(i, block; bufferBlocks)
			{
				MemoryManager.get().copyToDeviceLocalImage(cmdBuffer,
														   block,
														   this.subImages[i]);
			}

			MemoryManager.get().transitionImageLayout(cmdBuffer,
													  this,
													  VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
													  VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
													  VK_ACCESS_TRANSFER_WRITE_BIT,
													  VK_ACCESS_SHADER_READ_BIT,
													  VK_PIPELINE_STAGE_TRANSFER_BIT,
													  VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT);
		}

		void loadTextures(GraphicsContext gc, const void*[] imgData, ref VkCommandBuffer cmdBuffer)
		{
			VkResult res;
			BufferBlock[] bufferBlocks;

			VkMemoryRequirements mem_reqs;
  			vkGetImageMemoryRequirements(gc.getDevice(),
										 this.image.getImage(),
                               			 &mem_reqs);

			foreach(i, data; imgData)
			{
				bufferBlocks ~= MemoryManager.get().allocTransfer(mem_reqs.size);
				res = MemoryManager.get().copyIntoTransferBuffer(gc, data, bufferBlocks[$ - 1]);

				if(res != VK_SUCCESS)
					return;
			}

			MemoryManager.get().transitionImageLayout(cmdBuffer,
													  this,
													  VK_IMAGE_LAYOUT_UNDEFINED,
													  VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
													  0,
													  VK_ACCESS_TRANSFER_WRITE_BIT,
													  VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT,
													  VK_PIPELINE_STAGE_TRANSFER_BIT);
			
			foreach(i, block; bufferBlocks)
			{
				MemoryManager.get().copyToDeviceLocalImage(cmdBuffer,
														   block,
														   this.subImages[i]);
			}

			MemoryManager.get().transitionImageLayout(cmdBuffer,
													  this,
													  VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
													  VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
													  VK_ACCESS_TRANSFER_WRITE_BIT,
													  VK_ACCESS_SHADER_READ_BIT,
													  VK_PIPELINE_STAGE_TRANSFER_BIT,
													  VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT);
		}

		@nogc GpuSubImage*[] getSubImages()
		{
			return this.subImages;
		}

		@nogc GpuSubImage* getSubImageAtLayer(int layer)
		{
			return this.subImages[layer];
		}

		@nogc ivec3 getSize() const
		{
			return this.size;
		}

		@nogc int getLayers() const
		{
			return this.layers;
		}

		@nogc int getMipLevels() const
		{
			return this.mipLevels;
		}

		@nogc GpuImage getImage()
		{
			return this.image;
		}

		@nogc ImageView[] getImageViews()
		{
			return imgViews;
		}

		@nogc ImageView getImageView(uint idx)
		{
			return this.imgViews[idx];
		}

		@nogc VkSampler getSampler()
		{
			return this.sampler;
		}

    private:
		bool is3d = false;
		bool isDepth = false;
        ivec3 size = ivec3(0, 0, 0);
		int mipLevels = 0;
		int layers = 0;

		GpuImage image;
		GpuSubImage*[] subImages;
		ImageView[] imgViews;
		VkSampler sampler = VK_NULL_HANDLE;
		//VkImageViewType ivType;
}