#version 460

layout(location = 0) in FragIn
{
	vec2 uv;
	vec4 color;
} fragIn;

layout(set = 0, binding = 4) uniform sampler2D uiTexture;

layout(location = 0) out vec4 outColor;

void main()
{
	outColor = fragIn.color * texture(uiTexture, fragIn.uv);
}