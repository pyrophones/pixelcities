#ifndef LIGHTS_GLSL
#define LIGHTS_GLSL

struct Light
{
	vec3 color;
	float intensity;
};

struct DirectionalLight
{
	Light light;
	vec3 dir;
};

struct PointLight
{
	Light light;
	vec3 pos;
	float radius;
};

#endif
