#ifndef SCENECAMERA_GLSL
#define SCENECAMERA_GLSL

layout (set = 0, binding = 0) readonly uniform SceneCamera
{
	mat4 view;
	mat4 projection;
	mat4 invView;
	mat4 invProjection;
	mat4 invViewProjection;
	vec3 camLoc;
} sceneCamera;

#endif
