#version 460

layout(location = 0) in FragIn
{
	vec3 position;
    vec2 uv;
	vec3 norm;
	vec3 tan;
	//vec3 bitan;
} fragIn;

layout(push_constant) uniform NormalMatrix
{
	 layout(offset = 64) mat3 normal;
} normalUbo;

layout(set = 2, binding = 0) uniform sampler2DArray tileTexs;
layout(set = 2, binding = 1) uniform sampler2DArray mapTexs;

layout(location = 0) out vec4 outAlbedo;
layout(location = 1) out vec4 outNormal;
layout(location = 2) out vec4 outWorld;

// Get the offset of the sprite in the sprite sheet
vec2 getSpriteOffset(float data, vec2 uvOffset)
{
	vec2 spriteIdx = vec2(mod(data * 255.0, 16.0), floor((data * 255.0) / 16.0));

	vec2 idx = floor(spriteIdx);
	return (clamp(uvOffset, 0.0, 1.0) + idx) / vec2(16.0, 16.0);
}

void main()
{
	//mat3 test = { vec3(10, 0, 0), vec3(0, 1, 0), vec3(0, 0, 10) };
	vec3 n = normalize(normalUbo.normal * fragIn.norm);
	vec3 t = normalize(normalUbo.normal * fragIn.tan);
	t = normalize(t - dot(t, n) * n);
	vec3 b = cross(n, t);
	mat3 tbn = mat3(t, b, n);
	mat3 tbnT = transpose(tbn);

	vec2 mapSize = vec2(textureSize(mapTexs, 0));
	vec2 grid = fragIn.uv * mapSize;
	vec2 cell = floor(grid);
	vec2 uvOffset = grid - cell;

	vec3 grassColor = texture(tileTexs, vec3(getSpriteOffset(0.0, uvOffset), 0.0)).rgb;
	vec3 sandColor = texture(tileTexs, vec3(getSpriteOffset(1.0 / 255.0, uvOffset), 0.0)).rgb;
	vec3 finalColor = mix(sandColor, grassColor, smoothstep(0.25, 0.4, 1.0 /*fragIn.height*/));
		
	vec4 mapData = texture(mapTexs, vec3(cell / mapSize, 1.0));
	vec2 tileUV = getSpriteOffset(mapData.r, uvOffset);
	vec4 tile = texture(tileTexs, vec3(tileUV, 1.0));
	outAlbedo = vec4(mix(finalColor, tile.rgb, min(tile.a, mapData.a)), 0.0);
	outNormal.rgb = normalize(tbn * (normalize(vec3(0.5, 0.5, 1.0) * 2.0 - 1.0)));
	outNormal.a = 1.0;
	outWorld = vec4(fragIn.position, 1.0);
	//outColor = vec4(finalColor, 1.0);
    //outColor = texture(tex, fragIn.uv);
}