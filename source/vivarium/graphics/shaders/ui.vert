#version 460

layout(location = 0) in vec2 pos_vert;
layout(location = 1) in vec2 uv_vert;
layout(location = 2) in vec4 color_unorm_vert;

layout(location = 0) out FragIn
{
	vec2 uv;
	vec4 color;
} fragIn;

layout(push_constant) uniform ProjMat
{
	mat4 proj;
} projMat;

void main()
{
	gl_Position = projMat.proj * vec4(pos_vert, 0.0, 1.0);
	
	fragIn.uv = uv_vert;
	fragIn.color = color_unorm_vert;
}