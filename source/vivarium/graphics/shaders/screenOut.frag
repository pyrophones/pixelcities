#version 460

layout(location = 0) in FragIn
{
	vec2 uv;
} fragIn;

layout(set = 0, binding = 3) uniform sampler2D screenTexture;

layout(location = 0) out vec4 outColor;

void main()
{
	outColor = texture(screenTexture, fragIn.uv);
}

