#ifndef GBUFFER_GLSL
#define GBUFFER_GLSL

layout (set = 1, binding = 0) uniform sampler2D gAlbedoMetal;
layout (set = 1, binding = 1) uniform sampler2D gNormalAo;
layout (set = 1, binding = 2) uniform sampler2D gWorld;
//layout (set = 1, binding = 3) uniform sampler2D gRoughnessEmissive;
layout (set = 1, binding = 4) uniform sampler2D gDepth;

#endif
