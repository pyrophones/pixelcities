#ifndef TILEDRENDERING_GLSL
#define TILEDRENDERING_GLSL

#define MAX_WORK_GROUP_SIZE 32

shared uint flattenedId;

struct TileData
{
	uint activeLightCount;
	uint pointLightIndex[MAX_POINT_LIGHTS];
};

layout (set = 0, std430, binding = 2) coherent buffer TiledDeferredInfo
{
	TileData tileData[];
} tileInfo;

#endif
