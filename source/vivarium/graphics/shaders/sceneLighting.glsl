#ifndef SCENELIGHTING_GLSL
#define SCENELIGHTING_GLSL

//#define MAX_DIRECTIONAL_LIGHTS 8
#extension GL_GOOGLE_include_directive : require
#include "lights.glsl"

#define MAX_POINT_LIGHTS 1024

layout (set = 0, std430, binding = 1) readonly buffer SceneLighting
{
	PointLight pointLights[MAX_POINT_LIGHTS];
	DirectionalLight dirLight;
} sceneLighting;

#endif
