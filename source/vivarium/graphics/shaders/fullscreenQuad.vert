#version 460

layout(location = 0) in vec3 pos_vert;
layout(location = 1) in vec2 uv_vert;
layout(location = 2) in vec3 norm_vert;
layout(location = 3) in vec3 tan_vert;

layout(location = 0) out FragIn
{
	vec2 uv;
} fragIn;

void main()
{
	gl_Position = vec4(pos_vert, 1.0);
	fragIn.uv = uv_vert;
}

