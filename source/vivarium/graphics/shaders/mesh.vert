#version 460

#extension GL_GOOGLE_include_directive : require
#include "sceneLighting.glsl"
#include "sceneCamera.glsl"

layout(location = 0) in vec3 pos_vert;
layout(location = 1) in vec2 uv_vert;
layout(location = 2) in vec3 norm_vert;
layout(location = 3) in vec3 tan_vert;

//layout(binding = 0) uniform sampler2D heightmap;

layout(location = 0) out FragIn
{
	vec3 position;
	vec2 uv;
	vec3 norm;
	vec3 tan;
	//float height;
	//flat int instanceID;
} fragIn;

layout(push_constant) uniform ModelMatrix
{
	mat4 model;
} modelMat;

void main()
{
	//fragIn.pos.y = texture(heightTexture, UV).x - 0.5;
	vec4 worldPos = modelMat.model * vec4(pos_vert, 1.0);
	gl_Position = sceneCamera.projection * sceneCamera.view * worldPos;
	fragIn.position = worldPos.xyz;
	fragIn.uv = uv_vert;
	fragIn.norm = norm_vert;
	fragIn.tan = tan_vert;
	//fragIn.height = 1.0;
	//fragIn.instanceID = gl_InstanceIndex;
}