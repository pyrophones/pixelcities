#ifndef LIGHTINGFUNCTIONS_GLSL
#define LIGHTINGFUNCTIONS_GLSL

#extension GL_GOOGLE_include_directive : require
#include "lights.glsl"

// Precalculated stuff
#define PI 3.14159265359
#define ONE_OVER_PI 0.318309886184
#define MAX_REFLECTION_LOD 4.0

const vec3 F0_NON_METAL = vec3(0.04);

//Lighting equations
vec3 fresnelSchlick(float cosTheta, vec3 f0)
{
	return f0 + (1.0 - f0) * pow(1.0 - cosTheta, 5.0);
}

float distributionGGX(vec3 n, vec3 h, float roughness)
{
	float a = roughness * roughness;
	float a2 = max(a * a, 0.0000001);

	float nDotH = max(dot(n, h), 0.0);
	float nDotH2 = nDotH * nDotH;

	float denom = nDotH2 * (a2 - 1.0) + 1.0;
	denom = PI * denom * denom;

	return a2 / denom;
}

float partialGeometryGGX(float nDotV, float k)
{
	return nDotV / (nDotV * (1.0 - k) + k);
}

float geometryGGX(float nDotV, float nDotL, float roughness)
{
	float r = roughness + 1.0;
	float k = (r * r) * 0.125;

	return partialGeometryGGX(nDotV, k) * partialGeometryGGX(nDotL, k);
}

vec3 getBasicLighting(vec3 camLoc, vec3 dir, vec3 pos, vec3 albedo, vec3 normal, float roughness, float metallic)
{
	//Lighting variables
	vec3 l = normalize(dir); //Normalized light dir
	vec3 v = normalize(camLoc - pos); //Normalized angle from view pos to obj pos
	vec3 h = normalize(l + v); //Half vector
	vec3 r = reflect(-v, normal); //Reflection of -v over half vector
	float nDotV = max(dot(normal, v), 0.0);
	float nDotL = max(dot(normal, l), 0.0); // Lambert diffuse

	float ndf = distributionGGX(normal, h, roughness);
	float g = geometryGGX(nDotV, nDotL, roughness);

	vec3 f0 = mix(F0_NON_METAL, albedo, metallic);
	vec3 ks = fresnelSchlick(max(dot(v, h), 0.0), f0); //Fresnel factor for spec on edges

	vec3 num = ndf * ks * g;
	float denom = 4.0 * max(nDotV, nDotL) + 0.0001;
	vec3 spec = num / denom; //Specular

	vec3 kd = vec3(1.0) - ks;
	kd *= 1.0 - metallic;

	return (kd * nDotL * albedo + spec);
}

vec3 calcDirectionalLight(vec3 camLoc, DirectionalLight dl, vec3 pos, vec3 albedo, vec3 normal, float roughness, float metallic)
{
	//Attenuation
	float att = dl.light.intensity;
	vec3 rad = dl.light.color * att;

	//Get lighting, scale by attenuation
	vec3 color = getBasicLighting(camLoc, dl.dir, pos, albedo, normal, roughness, metallic) * rad;
	return color;
}

vec3 calcPointLight(vec3 camLoc, PointLight pl, vec3 pos, vec3 albedo, vec3 normal, float roughness, float metallic)
{
	//Lighting variables
	vec3 dir = pl.pos - pos;
	float dist = length(dir);

	//Attenuation
	float att = clamp(1.0 - dist * dist / (pl.radius * pl.radius), 0.0, 1.0);
	att *= att;
	vec3 rad = pl.light.color * att * pl.light.intensity;

	//Get lighting, scale by attenuation
	vec3 color = getBasicLighting(camLoc, dir, pos, albedo, normal, roughness, metallic) * rad;
	return color; //Return light color
}

#endif
