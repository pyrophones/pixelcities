module vivarium.core.transform;

import dlib.math;

struct Transform
{
	@nogc
	public:
	 	void _update()
		{
			if(isDirty)
			{
				this.mod = translationMatrix(this.pos) * this.rot.toMatrix4x4() * scaleMatrix(this.sca);

				this.isDirty = false;
				this.norm = matrix4x4to3x3(this.mod).inverse().transposed();
			}
		}

		vec3 getRotationAsEuler()
		{
			return this.rot.toEulerAngles();
		}

		void setRotationFromEuler(vec3 rotation)
		{
			this.rot = Quaternionf.fromEulerAngles(rotation);
			this.isDirty = true;
		}

		@property vec3 position()
		{
			return this.pos;
		}

		@property vec3 position(vec3 value)
		{
			this.isDirty = true;
			this.pos = value;
			return this.pos;
		}

		@property Quaternionf rotation()
		{
			return this.rotation;
		}

		@property Quaternionf rotation(Quaternionf value)
		{
			this.isDirty = true;
			return this.rot = value.normalized();  
		}

		@property vec3 scale()
		{
			return this.sca;
		}

		@property vec3 scale(vec3 value)
		{
			this.isDirty = true;
			this.sca = value;
			return this.sca;
		}

		@property vec3 forward()
		{
			return -this.mod.forward;
		}

		@property vec3 up()
		{
			return this.mod.up;
		}

		@property vec3 right()
		{
			return this.mod.right;
		}

		@property ref mat4 model()
		{
			return this.mod;
		}

		@property ref mat3 normal()
		{
			return this.norm;
		}


	private:
		bool isDirty = true;
		
		vec3 pos = vec3(0.0, 0.0, 0.0);
		Quaternionf rot = Quaternionf.identity();
		vec3 sca = vec3(1.0, 1.0, 1.0);
		mat4 mod = mat4.identity();
		mat3 norm = mat3.identity();
}