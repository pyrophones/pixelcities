module vivarium.core.engine;

// Standard imports
import std.stdio,
	   std.string;

// External
import bindbc.sdl,
	   dlib.math;

// Internal
import vivarium.core.input,
	   vivarium.core.timer,
	   vivarium.core.console,
	   vivarium.graphics;

class Engine
{
	public:
		~this()
		{
			
		}

		static Engine get()
    	{
    	    if (!Engine.instantiated_)
    	    {
    	        synchronized(Engine.classinfo)
    	        {
    	            if (!Engine.instance_)
    	            {
    	                Engine.instance_ = new Engine();

						Console.get();
						int ret = Engine.instance_.initializeSDL();
				
						Input.get();
						Timer.get();

						ivec2 size;
						SDL_GetWindowSize(Engine.instance_.window, &size.x, &size.y);
						Engine.instance_.gc = new GraphicsContext();
						Engine.instance_.gc.initialize(Engine.instance_.window);

						Console.get().connectSignals(Engine.instance_.gc);
    	            }

    	            Engine.instantiated_ = true;
    	        }
    	    }

    	    return Engine.instance_;
    	}

		static void release()
		{
			synchronized(Engine.classinfo)
    	    {
    	        if (Engine.instance_)
    	        {
					Console.get().saveLogToFile();
					destroy(Engine.instance_.gc);
					Console.release();
					Timer.release();
					Input.release();
					SDL_QuitSubSystem(SDL_INIT_VIDEO);
					SDL_DestroyWindow(Engine.instance_.window);
					SDL_Quit();
					destroy(Engine.instance_);
    	        }
    	    }
		}

		//override void initialize()
		//{	
		//	//TODO: Handle errors
		//	int ret = initializeSDL();
		//		
		//	if(ret != 0)
		//		return;
//
		//	Input.get().initialize();
		//	Timer.get().initialize();
		//
		//	ret = GraphicsContext.get().initialize(this.window);
	//
		//	return;
		//}
	
		int run()
		{
			Console.get().log("Success, main running loop . . .");
			int ret;
	
			while(!Input.get().receivedQuitEvent())
			{
				Input.get().processEvents();
				ret = this.gc.render();
	
				if(ret != 0)
					return ret;
			}
	
			return 0;
		}

		SDL_Window* getWindowPtr() @nogc
		{
			return this.window;
		}
	
		ivec2 getWindowSize()
		{
			ivec2 size;
			SDL_GetWindowSize(this.window, &size.x, &size.y);
			return size;
		}
	
	private:
		SDL_Window* window = null;

		GraphicsContext gc;

		// Thread local
		static bool instantiated_;

    	// Thread global
		__gshared Engine instance_= null;

		this()
		{
			
		}
	
		int initializeSDL()
		{
			import erupted.vulkan_lib_loader;
	
			loadGlobalLevelFunctions;
			SDL_SysWMinfo windowInfo;
	
			immutable SDLSupport ret = loadSDL();
	
			ivec2 size = ivec2(1920, 1080);
	
			if(ret != sdlSupport)
			{
				if(ret == SDLSupport.noLibrary)
				{
					Console.get().crit("SDL shared library failed to load!");
					return -1;
				}
	
				else if(ret == SDLSupport.badLibrary)
				{
					Console.get().crit("SDL library symbol failed to load!");
					return -1;
				}
			}
	
			if(SDL_Init(SDL_INIT_VIDEO) != 0)
			{
				Console.get().crit("SDL couldn't initialize. SDL error: " ~ SDL_GetError().fromStringz().idup);
				return -1;
			}
	
			SDL_WindowFlags flags = SDL_WINDOW_RESIZABLE | SDL_WINDOW_ALLOW_HIGHDPI | SDL_WINDOW_SHOWN | SDL_WINDOW_VULKAN;
	
			this.window = SDL_CreateWindow("Vivarium",
										   SDL_WINDOWPOS_UNDEFINED,
										   SDL_WINDOWPOS_UNDEFINED,
										   size.x,
										   size.y,
										   flags);
	
			if(this.window == null)
			{
				Console.get().crit("Could not create SDL window. SDL error: " ~ SDL_GetError().fromStringz().idup);
				return -1;
			}
	
			//SDL_SetWindowFullscreen(this.window, SDL_WINDOW_FULLSCREEN);
			SDL_GetWindowWMInfo(this.window, &windowInfo);
			SDL_VERSION(&windowInfo.version_);
	
			return 0;
		}
}