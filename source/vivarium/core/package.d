module vivarium.core;

public:
import vivarium.core.assetmanager,
	   vivarium.core.camera,
	   vivarium.core.console,
	   vivarium.core.input,
	   vivarium.core.engine,
	   vivarium.core.timer,
	   vivarium.core.transform;