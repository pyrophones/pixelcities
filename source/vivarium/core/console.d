module vivarium.core.console;

import std.string,
	   std.signals,
	   std.datetime.systime;

import bindbc.nuklear;

import vivarium.core.assetmanager,
	   vivarium.core.input,
	   vivarium.core.timer,
	   vivarium.graphics.graphicscontext;

enum LogLevel
{
	NORMAL,
	WARN,
	ERROR,
	CRITICAL
}

struct Log
{
	string text;
	nk_color color;
}

struct RecompileShaderSignal
{
	mixin Signal!();
}

class Console
{
	//@nogc:
	public:
		static Console get()
    	{
    	    if (!Console.instantiated_)
    	    {
    	        synchronized(Console.classinfo)
    	        {
    	            if (!Console.instance_)
    	            {
    	                Console.instance_ = new Console();
    	                Console.instance_.initialize();
    	            }

    	            Console.instantiated_ = true;
    	        }
    	    }

    	    return Console.instance_;
    	}

		static void release()
		{
			synchronized(Console.classinfo)
    	    {
    	        if (Console.instance_)
    	        {
					destroy(Console.instance_);
    	        }
    	    }
		}

		void initialize()
		{
			Console.logData.reserve(200);

			//debug
			//{
			//	this.commands["GC.RecompileShaders"] = true;
			//}
			this.commands["RecompileShaders"] = true;
		}

		void connectSignals(GraphicsContext gc)
		{
			recompileShaders.connect(&gc.recompileShaders);
		}

		void log(string text, LogLevel level = LogLevel.NORMAL)
		{
			string[] splitText = text.split("\n");

			foreach(snippet; splitText)
			{
				if(snippet == "")
					continue;
				logData ~= Log(this.getFormattedtime(Clock.currTime()) ~ " -- " ~ snippet, this.getLogColor(level));
			}
		}
		
		void warn(string text)
		{
			log(text, LogLevel.WARN);
		}

		void err(string text)
		{
			log(text, LogLevel.ERROR);
		}

		void crit(string text)
		{
			log(text, LogLevel.CRITICAL);
		}

		void saveLogToFile()
		{
			string logDataCompiled;
			foreach(log; this.logData)
			{
				logDataCompiled ~= log.text ~ "\n";
			}
			//AssetManager.saveToFile("./" ~ Clock.currTime().toString() ~ "-log.txt", logDataCompiled);
		}

		Log[] getLog()
		{
			return logData;
		}

		void drawWidget(nk_context* ctx)
		{
			if(nk_begin(ctx,
						"Vivarium Console",
						nk_rect(200, 200, 400, 300),
			   			NK_WINDOW_BORDER | NK_WINDOW_MOVABLE | NK_WINDOW_SCALABLE | NK_WINDOW_MINIMIZABLE | NK_WINDOW_TITLE))
			{
				nk_layout_row_dynamic(ctx, nk_window_get_height(ctx) - 90, 1);
				if(nk_group_begin(ctx, "ConsoleLog", NK_WINDOW_BORDER))
				{
					foreach(log; logData)
					{
						nk_layout_row_dynamic(ctx, 20, 1);
						//nk_text_colored(ctx, toStringz(log.text), cast(int)log.text.length, NK_TEXT_LEFT, log.color);
						nk_select_text(ctx, toStringz(log.text), cast(int)log.text.length, NK_TEXT_LEFT, false);
					}

					nk_group_end(ctx);
				}

				nk_layout_row_dynamic(ctx, 30, 1);
				nk_flags event = nk_edit_string(ctx,
												NK_EDIT_FIELD | NK_EDIT_SIG_ENTER,
												buffer.ptr,
												&this.textLen,
												256,
												&nk_filter_default);

				if(event & NK_EDIT_ACTIVATED)
				{
					Input.get().startTextInput();
				}

				if(event & NK_EDIT_DEACTIVATED)
				{
					Input.get().stopTextInput();						
				}

				if(event & NK_EDIT_ACTIVE)
				{
					if(event & NK_EDIT_COMMITED)
					{
						if(this.textLen > 0)
						{
							string toSubmit = buffer[0..this.textLen].idup();

							if(toSubmit in this.commands && this.commands[toSubmit] == true)
							{
								this.commandEnter(toSubmit);
								this.runCommand(toSubmit);
							}

							else
								this.commandEnter("Command '" ~ toSubmit ~ "' not found", LogLevel.ERROR);

							this.buffer[0..$] = '\xFF';
							this.textLen = 0;
						}
					}
				}

				uint scrollY = 0;

				//nk_window_get_scroll(ctx, null, &scrollY);
				//import std.stdio;
				//writeln(scrollY);
				//writeln(logData.length);
				//uint scrollTo = 20 * cast(uint)logData.length;
				//if(scrollY >= scrollTo)
				//	nk_window_set_scroll(ctx, 0, scrollTo);

			}
			nk_end(ctx);
		}

	private:
		Log[] logData;
		int endOfScroll = 0;
		char[256] buffer;
		int textLen;
		bool[string] commands;

		RecompileShaderSignal recompileShaders;

		// Thread local
		static bool instantiated_;

		// Thread global
		__gshared Console instance_;

		this()
		{

		}

		string getFormattedtime(SysTime time)
		{
			return "%02d:%02d:%02d".format(time.hour, time.minute, time.second);
		}

		void runCommand(string text)
		{
			string[] commandArgs= text.split(" ");

			//string[] splitCommand = text.split(".");

			switch(text)
			{
				case "RecompileShaders":
					recompileShaders.emit();
					return;
				default:
					return;
			}
		}

		void commandEnter(string text, LogLevel level = LogLevel.NORMAL)
		{
			logData ~= Log(text, this.getLogColor(level));
		}

		nk_color getLogColor(LogLevel level)
		{
			switch(level)
			{
				case LogLevel.WARN:
					return nk_color(229, 85, 2, 255);
				case LogLevel.ERROR:
					return nk_color(242, 10, 10, 255);
				case LogLevel.CRITICAL:
					return nk_color(160, 0, 0, 255);
				case LogLevel.NORMAL:
				default:
					return nk_color(255, 255, 255, 255);
			} 
		}
}