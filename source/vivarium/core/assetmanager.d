module vivarium.core.assetmanager;

import std.file,
	   std.stdio,
	   std.string;

import dlib.filesystem;

import vivarium.core.console;

class AssetManager
{
	static ubyte[] loadAsBytes(string fileName)
	{
		if(checkValid(fileName))
		{
			//InputStream
			//return readFile(toStringz(fileName));
			auto f = File(fileName, "rb");

			if(checkOpen(f))
			{
				ubyte[] contents = f.rawRead(new ubyte[f.size]);
				f.close();
				return contents;
			}
		}

		return null;
	}

	//static char[] loadAsChar(string fileName)
	//{
	//	if(checkValid(fileName))
	//	{
	//		auto f = File(fileName, "r");
//
	//		if(checkOpen(f))
	//		{
	//			char[] contents = f.rawRead(new char[f.size]);
	//			f.close();
//
	//			return contents;
	//		}
	//	}
//
	//	return null;
	//}

	static void saveToFile(string fileName, string contents)
	{
		auto f = File(fileName, "w");
		f.write(contents);
		f.close();
	}

	private:
		static bool checkValid(string fileName)
		{
			if(!exists(fileName))
			{
				Console.get().err("Could not find file with name \"" ~ fileName ~ "\"");
				return false;
			}

			if(!isFile(fileName))
			{
				Console.get().err("\"" ~ fileName ~ "\" is not a file");
				return false;
			}

			return true;
		}

		static bool checkOpen(File f)
		{
			if(!f.isOpen())
			{
				Console.get().err("Failed to open \"" ~ f.name() ~ "\"");
				return false;
			}

			return true;
		}
}