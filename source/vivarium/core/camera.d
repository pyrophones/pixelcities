module vivarium.core.camera;

import std.math;

import dlib.math;

import vivarium.core.transform;

struct SceneCameraInfo
{
	mat4 view = mat4.identity();
	mat4 projection = mat4.identity();
	mat4 invView = mat4.identity();
	mat4 invProjection = mat4.identity();
	mat4 invViewProjection = mat4.identity();
	vec3 camLoc = vec3(0.0);
}

class Camera
{
	@nogc:
	public:
		void _update()
		{
			this.trans._update();
			this.calculateView();

			this.camInfo.camLoc = this.trans.position;
		}

	 	void calculateView()
		{
			if(this.shouldUpdate)
			{
				this.camInfo.view = lookAtMatrix(this.trans.position,
												 this.trans.position + this.trans.forward,
												 this.trans.up);
				this.shouldUpdate = false;
				this.camInfo.invViewProjection = this.camInfo.projection * this.camInfo.view;
				this.camInfo.invViewProjection = this.camInfo.invViewProjection.inverse();
				this.camInfo.invView = this.camInfo.view.inverse;
			}
		}

		void move(vec3 position)
		{
			this.trans.position = this.trans.position + position;
			this.shouldUpdate = true;
		}

		void rotate(vec3 rotation)
		{
			Quaternionf pitch = rotationQuaternion(Axis.x, rotation.x);
			Quaternionf yaw = rotationQuaternion(Axis.y, rotation.y);
			Quaternionf roll = rotationQuaternion(Axis.z, rotation.z);
			this.trans.rotation = yaw * this.trans.rotation * pitch * roll;
			this.shouldUpdate = true;
		}

		void setProjection(float left, float right, float bottom, float top, float near, float far)
		{
			this.camInfo.projection = ortho(left, right, bottom, top, near, far);
			this.camInfo.invProjection = this.camInfo.projection.inverse();
			//this.camInfo.near = near;
			//this.camInfo.far = far;
		}

		void setProjection(float fov, float aspect, float near, float far)
		{
			this.camInfo.projection = perspective(fov, aspect, near, far);
			this.camInfo.invProjection = this.camInfo.projection.inverse();
			//this.camInfo.near = near;
			//this.camInfo.far = far;
		}

		ref SceneCameraInfo getSceneCameraInfo()
		{
			return this.camInfo;
		}

		@property ref Transform transform()
		{
			return this.trans;
		}

		@property ref Transform transform(Transform value)
		{
			this.trans = value;
			return this.trans;
		}

	private:
		Transform trans;
		SceneCameraInfo camInfo;

		bool shouldUpdate = true;

		// Zero to one depth
		mat4 perspective(float fovY, float aspect, float n, float f)
		{
			mat4 res = mat4.identity;

			float angle;
			float cot;

			angle = degtorad(fovY / 2.0);
			cot = cos(angle) / sin(angle);

			res.arrayof[0]  = cot / aspect;
			res.arrayof[1]  = 0.0;
			res.arrayof[2]  = 0.0;
			res.arrayof[3]  = 0.0;

			res.arrayof[4]  = 0.0;
			res.arrayof[5]  = -cot;
			res.arrayof[6]  = 0.0;
			res.arrayof[7]  = 0.0;

			res.arrayof[8]  = 0.0;
			res.arrayof[9]  = 0.0;
			res.arrayof[10] = f / (n - f);
			res.arrayof[11] = -1.0f;

			res.arrayof[12] = 0.0;
			res.arrayof[13] = 0.0;
			res.arrayof[14] = -(f * n) / (f - n);
			res.arrayof[15] = 0.0;

			return res;
		}

		// Zero to one depth
		mat4 ortho(float l, float r, float b, float t, float n, float f)
		{
			auto res = mat4.identity();

			float width  = r - l;
			float height = t - b;
			float depth  = f - n;

			res.arrayof[0]  =  2.0 / width;
			res.arrayof[1]  =  0.0;
			res.arrayof[2]  =  0.0;
			res.arrayof[3]  =  0.0;

			res.arrayof[4]  =  0.0;
			res.arrayof[5]  =  2.0 / height;
			res.arrayof[6]  =  0.0;
			res.arrayof[7]  =  0.0;

			res.arrayof[8]  =  0.0;
			res.arrayof[9]  =  0.0;
			res.arrayof[10] = -1.0 / depth;
			res.arrayof[11] =  0.0;

			res.arrayof[12] = -(r + l) / width;
			res.arrayof[13] = -(t + b) / height;
			res.arrayof[14] = -n / depth;
			res.arrayof[15] =  1.0;

			return res;
		}
}