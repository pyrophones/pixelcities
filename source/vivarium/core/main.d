module vivarium.core.main;

import vivarium.core.engine;

int main(string[] args)
{
	Engine.get();
	int res = 0;
	if(res != 0)
		return res;

	res = Engine.get().run();
	if(res != 0)
		return res;

	Engine.release();
	return 0;
}