module vivarium.core.timer;

import core.time;

class Timer
{
	//@nogc:
	public:
		static Timer get()
    	{
    	    if (!Timer.instantiated_)
    	    {
    	        synchronized(Timer.classinfo)
    	        {
    	            if (!Timer.instance_)
    	            {
    	                Timer.instance_ = new Timer();
						Timer.instance_.cur = MonoTime.currTime();
    	            }

    	            Timer.instantiated_ = true;
    	        }
    	    }

    	    return Timer.instance_;
    	}

		static void release()
		{
			synchronized(Timer.classinfo)
    	    {
    	        if (Timer.instance_)
    	        {
					destroy(Timer.instance_);
    	        }
    	    }
		}

		void update()
		{
			this.curIdx = (this.curIdx + 1) % prevTimes.length;
			this.prev = this.cur;
			this.cur = MonoTime.currTime;
			this.prevTimes[curIdx] = delta.total!"usecs" / 1_000_000.0;
			this.delta = this.cur - this.prev;
		}

		void fixedUpdate()
		{
		//	this time =
		}

		float getDeltaTime()
		{
			return this.delta.total!"usecs" / 1_000_000.0;
		}

		float getSmoothedFPS()
		{
			float avgTime = 0;
			foreach(time; this.prevTimes)
			{
				avgTime += time;
			}
			return avgTime / this.prevTimes.length;
		}

	private:
		MonoTime prev;
		MonoTime cur;
		Duration delta;
		Duration prevDelta;
		int curIdx = 0;

		float[500] prevTimes = 0;

		// Thread local
		static bool instantiated_;

    	// Thread global
		__gshared Timer instance_;

		this()
		{

		}
}