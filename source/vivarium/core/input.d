module vivarium.core.input;

import std.typecons : BitFlags;

import bindbc.sdl;

//import vivarium.core.memory.allocator;

import dlib.math;

//enum KeyState
//{
//	RELEASED = 1 << 0,
//	JUST_RELEASED = 1 << 1,
//	PRESSED = 1 << 2,
//	JUST_PRESSED = 1 << 3,
//	CHANGED = 1 << 4
//}

struct KeyState
{
	bool pressed = false;
	bool repeat = false;
	bool recievedEvent = false;
}

struct MouseInfo
{
	bool pressed = false;
	bool doubleClicked = false;
	ivec2 position;
	bool recievedEvent = false;
}

class Input
{
	//@nogc:
	public:
		~this()
		{
			
		}

		static Input get()
    	{
    	    if (!Input.instantiated_)
    	    {
    	        synchronized(Input.classinfo)
    	        {
    	            if (!Input.instance_)
    	            {
						immutable size = __traits(classInstanceSize, Input);
						Input.instance_ = new Input();
    	            }

    	            Input.instantiated_ = true;
    	        }
    	    }

    	    return Input.instance_;
    	}

		static void release()
		{
			synchronized(Input.classinfo)
    	    {
    	        if (Input.instance_)
    	        {
    	            destroy(Input.instance_);
    	        }
    	    }
		}

		void processEvents()
		{
			foreach(mouseEvent; this.mouseInfo)
			{
				//mouseEvent.pressed = false;
				mouseEvent.recievedEvent = false;
			}

			foreach(key; keys)
			{
				key.recievedEvent = false;
			}

			SDL_Event event;
			while(SDL_PollEvent(&event) != 0)
			{
				newChar = false;
				switch(event.type)
				{
					case SDL_QUIT:
						this.quitEvent = true;
						break;
					case SDL_WINDOWEVENT:
						switch(event.window.event)
						{
							case SDL_WINDOWEVENT_RESIZED:
								this.resizing = true;
								break;
							case SDL_WINDOWEVENT_MINIMIZED:
								this.minimized = true;
								break;
							case SDL_WINDOWEVENT_EXPOSED:
								this.minimized = false;
								break;
							default:
								this.resizing = false;
								break;
						}
						break;
					case SDL_MOUSEBUTTONDOWN:
					case SDL_MOUSEBUTTONUP:
						this.mouseInfo[event.button.button] = MouseInfo(event.type == SDL_MOUSEBUTTONDOWN,
																		event.button.clicks == 2,
																		ivec2(event.button.x, event.button.y),
																		true);
						break;
					case SDL_MOUSEMOTION:
						this.prevPosition = this.curPosition;
						this.curPosition = ivec2(event.motion.x, event.motion.y);
						this.relPosition = ivec2(event.motion.xrel, event.motion.yrel);
						break;
					case SDL_TEXTINPUT:
						this.charBuffer = event.text.text;
						newChar = true;
						break;
					case SDL_KEYDOWN:
						this.keys[event.key.keysym.sym] = KeyState(true, event.key.repeat ? true : false, true);
						break;
					case SDL_KEYUP:
						this.keys[event.key.keysym.sym] = KeyState(false, event.key.repeat ? true : false, true);
						break;
					default:
						break;
				}
			}
		}

		bool receivedQuitEvent()
		{ 
			return this.quitEvent;
		}
	
		bool isResizing()
		{
			return this.resizing;
		}
	
		bool isKeyPressed(SDL_Keycode key)
		{
			if(key in this.keys)
				return this.keys[key].pressed;
			else
				return false;
		}
	
		bool isKeyJustPressed(SDL_Keycode key)
		{
			if(key in this.keys)
				return this.keys[key].pressed && !this.keys[key].repeat;
			else
				return false;
		}
	
		bool isKeyReleased(SDL_Keycode key)
		{
			if(key in this.keys)
				return !this.keys[key].pressed;
			else
				return false;
		}
	
		bool isKeyJustReleased(SDL_Keycode key)
		{
			if(key in this.keys)
				return !this.keys[key].pressed && !this.keys[key].repeat;
			else
				return false;
		}

		bool didKeyRecieveEvent(SDL_Keycode key)
		{
			if(key in this.keys)
			{
				bool res = this.keys[key].recievedEvent;
				this.keys[key].recievedEvent = false;
				return res;
			}
			else
				return false;
		}

		bool didMouseRecieveEvent(ubyte button)
		{
			if(button in this.mouseInfo)
			{
				bool res = this.mouseInfo[button].recievedEvent;
				this.mouseInfo[button].recievedEvent = false;
				return res;
			}
			else
				return false;
		}

		bool isMinimized()
		{
			return this.minimized;
		}

		bool isButtonPressed(ubyte button)
		{
			if(button in this.mouseInfo)
				return this.mouseInfo[button].pressed;
			return false;
		}

		bool isButtonDoublePressed(ubyte button)
		{
			if(MouseInfo* info = button in this.mouseInfo)
				return info.doubleClicked;
			return false;
		}

		@nogc bool mouseMoved()
		{
			return curPosition != prevPosition;
		}

		@nogc void startTextInput()
		{
			SDL_StartTextInput();
		}

		@nogc void stopTextInput()
		{
			SDL_StopTextInput();
		}

		@nogc ivec2 getClickPosition(ubyte button)
		{
			if(MouseInfo* info = button in this.mouseInfo)
				return info.position;
			return ivec2(0);
		}

		@nogc ivec2 getMousePosition()
		{
			return curPosition;
		}

		@nogc ivec2 getRelativeMousePosition()
		{
			return relPosition;
		}

		@nogc char[32] getCurrentTextInput()
		{
			return this.charBuffer;
		}

		@nogc bool isNewChar()
		{
			bool old = this.newChar;
			newChar = false;
			return old;
		}

		//@nogc bool isTyping()
		//{
		//	return 
		//}

	private:
		KeyState[SDL_Keycode] keys;
		bool quitEvent = false;
		bool resizing = false;
		bool minimized = false;
		char[32] charBuffer;
		bool newChar = false;

		ivec2 curPosition = ivec2(0);
		ivec2 prevPosition = ivec2(0);
		ivec2 relPosition = ivec2(0);
		MouseInfo[ubyte] mouseInfo;
		//bool mouseMoving = true;


		// Thread local
		static bool instantiated_;

    	// Thread global
		__gshared Input instance_ = null;

		this()
		{
			
		}
}